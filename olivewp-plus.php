<?php 
/* 
Plugin Name: 	OliveWP Plus
Description: 	Enhances Olivewp theme with extra functionality.
Version:		1.3.1
Author: 		spicethemes
Author URI: 	https://spicethemes.com
Text Domain: 	olivewp-plus
*/
//Freemius SDK Snippet
if ( ! function_exists( 'op_fs' ) ) {
    // Create a helper function for easy SDK access.
    function op_fs() {
        global $op_fs;
        if ( ! isset( $op_fs ) ) {
            // Include Freemius SDK.
            if ( function_exists('olivewp_companion_activate') && defined( 'OWC_PLUGIN_DIR' ) && file_exists(OWC_PLUGIN_DIR . '/inc/freemius/start.php') ) {
                // Try to load SDK from olivewp companion folder.
                require_once OWC_PLUGIN_DIR . '/inc/freemius/start.php';
            } else if ( function_exists('olivewp_plus_activate') && defined( 'OLIVEWP_PLUGIN_DIR' ) && file_exists(OLIVEWP_PLUGIN_DIR . '/freemius/start.php') ) {
                // Try to load SDK from premium olivewp companion plugin folder.
                require_once OLIVEWP_PLUGIN_DIR . '/freemius/start.php';
            } else {
                require_once dirname(__FILE__) . '/freemius/start.php';
            }
            $op_fs = fs_dynamic_init( array(
                'id'                  => '10605',
                'slug'                => 'olivewp-plus',
                'premium_slug'        => 'olivewp-plus',
                'type'                => 'plugin',
                'public_key'          => 'pk_7c81273c35f58df9dc63736356468',
                'is_premium'          => true,
                'is_premium_only'     => true,
                'has_paid_plans'      => true,
                'is_org_compliant'    => false,
                'menu'                => array(
                    'slug'           => 'olivewp-plus-welcome',
                    'first-path'     => 'admin.php?page=olivewp-plus-welcome',
                    'support'        => false,
                ),
                // Set the SDK to work in a sandbox mode (for development & testing).
                // IMPORTANT: MAKE SURE TO REMOVE SECRET KEY BEFORE DEPLOYMENT.
                ) );
        }
        return $op_fs;
    }
  op_fs();
}
// define the constant for the URL
define( 'OLIVEWP_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
define( 'OLIVEWP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
require_once('inc/widgets/register-sidebar-plus.php');
class PageTemplater {

	/**
	 * A reference to an instance of this class.
 	*/
	private static $instance;

	/**
	 * The array of templates that this plugin tracks.
	*/
	protected $templates;

	/**
	 * Returns an instance of this class.
 	*/
	public static function get_instance() {
		if ( null == self::$instance ) {
			self::$instance = new PageTemplater();
		}
		return self::$instance;
	}

	/**
	 * Initializes the plugin by setting filters and administration functions.
 	*/
	private function __construct() {

		$this->templates = array();

		add_filter(
			'theme_page_templates', array( $this, 'add_new_template' )
		);

		// Add a filter to the template include to determine if the page has our template assigned and return it's path
		add_filter(
			'template_include',
			array( $this, 'view_project_template')
		);

		// Add your templates to this array.
		$this->templates = array(
			'template-blog-full-width.php'					=>	'Blog Full Width',
			'template-blog-left-sidebar.php' 				=> 	'Blog Left Sidebar',	
			'template-blog-right-sidebar.php' 				=> 	'Blog Right Sidebar',
			'template-blog-grid-view.php' 					=> 	'Blog Grid View',
			'template-blog-grid-view-sidebar.php'			=>	'Blog Grid View Sidebar',
			'template-blog-list-view.php'					=>	'Blog List View',
			'template-blog-list-view-sidebar.php'			=>	'Blog List View Sidebar',
			'template-blog-switcher-view.php'				=>	'Blog Switcher View',
			'template-blog-switcher-view-sidebar.php'		=>	'Blog Switcher View Sidebar',
			'template-blog-masonry-two-column.php'			=>	'Blog Masonry 2 Column',
			'template-blog-masonry-three-column.php'		=>	'Blog Masonry 3 Column',
			'template-blog-masonry-four-column.php'			=>	'Blog Masonry 4 Column'
		);

	}

	/**
	 * Adds our template to the page dropdown for v4.7+
	 *
 	*/
	public function add_new_template( $posts_templates ) {
		$posts_templates = array_merge( $posts_templates, $this->templates );
		return $posts_templates;
	}


	/**
	 * Checks if the template is assigned to the page
	 */
	public function view_project_template( $template ) {
		// Return the search template if we're searching (instead of the template for the first result)
		if ( is_search() ) {
			return $template;
		}

		// Get global post
		global $post;

		// Return template if post is empty
		if ( ! $post ) {
			return $template;
		}

		// Return default template if we don't have a custom one defined
		if ( ! isset( $this->templates[get_post_meta(
			$post->ID, '_wp_page_template', true
		)] ) ) {
			return $template;
		}

		// Allows filtering of file path
		$filepath = apply_filters( 'page_templater_plugin_dir_path', plugin_dir_path( __FILE__ ).'/inc/template/' );

		$file =  $filepath . get_post_meta(
			$post->ID, '_wp_page_template', true
		);

		// Just to be safe, we check if the file exist first
		if ( file_exists( $file ) ) {
			return $file;
		} else {
			echo $file;
		}

		// Return template
		return $template;

	}

}
add_action( 'plugins_loaded', array( 'PageTemplater', 'get_instance' ) );


//Define the function for the plugin activation
function olivewp_plus_activate() {

	// gets the current theme
	$theme = wp_get_theme();
	// checking for olivewp theme
	if ( 'OliveWP' == $theme->name || 'OliveWP Child' == $theme->name ) {
		require_once OLIVEWP_PLUGIN_DIR . '/inc/customizer/customizer-plus.php';
		require_once OLIVEWP_PLUGIN_DIR . '/inc/helpers-plus.php';
		require_once OLIVEWP_PLUGIN_DIR . '/inc/script/script-plus.php';
		require_once OLIVEWP_PLUGIN_DIR . '/inc/breadcrumbs/breadcrumbs-plus.php';
		require_once OLIVEWP_PLUGIN_DIR . '/inc/pagination/pagination.php';
		require_once OLIVEWP_PLUGIN_DIR . '/inc/functions-plus.php';
		require_once OLIVEWP_PLUGIN_DIR . '/inc/theme-color/custom-color-plus.php';
		require_once OLIVEWP_PLUGIN_DIR . '/inc/theme-color/color-background-plus.php';
		require_once OLIVEWP_PLUGIN_DIR . '/inc/typography/custom-typography-plus.php';
		require_once OLIVEWP_PLUGIN_DIR . '/inc/typography/webfonts-plus.php';
		require_once OLIVEWP_PLUGIN_DIR . '/inc/meta-box/meta-box-plus.php';
	
		// Register footer menu
		register_nav_menus( 
			array(
				'footer_menu' => esc_html__( 'Footer Menu', 'olivewp-plus' ),
			) 
		);


		/**
		* Load the localisation file.
		*/
		load_plugin_textdomain( 'olivewp-plus', false, OLIVEWP_PLUGIN_DIR . '/languages/' );


		//About Theme
	    if (is_admin()) {
	        require OLIVEWP_PLUGIN_DIR . '/inc/admin/admin-init.php';
	    } 
	}     

}
add_action('plugins_loaded', 'olivewp_plus_activate');


$theme = wp_get_theme();
if ( 'OliveWP' == $theme->name || 'OliveWP Child' == $theme->name ){
	
	register_activation_hook( __FILE__, 'olivewp_plus_install_function');
	function olivewp_plus_install_function()
	{	
		
		function olivewp_plus_activation_redirect( $plugin ) {
	        if( $plugin == plugin_basename( __FILE__ ) ) {
	            exit( wp_redirect( admin_url( 'themes.php?page=olivewp-plus-welcome' ) ) );
	        }
	    }
	    add_action( 'activated_plugin', 'olivewp_plus_activation_redirect' );

    }

    function olivewp_plus_file_replace() {

	    $plugin_dir_xml = plugin_dir_path( __FILE__ ) . 'wpml-config.xml';
	    $theme_dir_xml	= get_stylesheet_directory() . '/wpml-config.xml';

	    if (!copy($plugin_dir_xml, $theme_dir_xml)) {
	        echo "failed to copy $plugin_dir to $theme_dir_xml...\n";
	    }
	}
	add_action( 'wp_head', 'olivewp_plus_file_replace' );
}



if(get_option('olivewp_blog_post_ordering')=='')
{
$theme = wp_get_theme();
    if((get_theme_mod('olivewp_plus_enable_post_comment',true)==false ) && (get_theme_mod('olivewp_plus_enable_post_date',true)==false ) && (get_theme_mod('olivewp_plus_enable_category',true)==false ) && (get_theme_mod($theme.'_blg_comment','')!='old' ) && (get_theme_mod($theme.'_blg_date','')!='old' ) && (get_theme_mod($theme.'_blg_category','')!='old' ) ){
        set_theme_mod('olivewp_blog_meta_sort',array());
        add_option($theme.'_blg_comment', 'old');
        add_option($theme.'_blg_date', 'old');
        add_option($theme.'_blg_category', 'old');
    }
    elseif((get_theme_mod('olivewp_plus_enable_post_comment',true)==false ) && (get_theme_mod('olivewp_plus_enable_post_date',true)==false )  && (get_theme_mod($theme.'_blg_date','')!='old' ) && (get_theme_mod($theme.'_blg_comment','')!='old' ) ){
        set_theme_mod('olivewp_blog_meta_sort',array('blog_category'));
        add_option($theme.'_blg_date', 'old');
        add_option($theme.'_blg_comment', 'old');
    }
    elseif((get_theme_mod('olivewp_plus_enable_post_date',true)==false ) && (get_theme_mod('olivewp_plus_enable_category',true)==false ) && (get_theme_mod($theme.'_blg_date','')!='old' ) && (get_theme_mod($theme.'_blg_category','')!='old' ) ){
        set_theme_mod('olivewp_blog_meta_sort',array('blog_comment'));
        add_option($theme.'_blg_date', 'old');
        add_option($theme.'_blg_category', 'old');
    }
    elseif((get_theme_mod('olivewp_plus_enable_post_comment',true)==false ) && (get_theme_mod('olivewp_plus_enable_category',true)==false ) && (get_theme_mod($theme.'_blg_comment','')!='old' ) && (get_theme_mod($theme.'_blg_category','')!='old' ) ){
        set_theme_mod('olivewp_blog_meta_sort',array('blog_date'));
        add_option($theme.'_blg_comment', 'old');
        add_option($theme.'_blg_category', 'old');
    }
    elseif((get_theme_mod('olivewp_plus_enable_post_comment',true)==false ) && (get_theme_mod($theme.'_blg_comment','')!='old' ) ){
        set_theme_mod('olivewp_blog_meta_sort',array( 'blog_date', 'blog_category'));
        add_option($theme.'_blg_comment', 'old');
    }
    elseif((get_theme_mod('olivewp_plus_enable_post_date',true)==false ) && (get_theme_mod($theme.'_blg_date','')!='old' ) ){
        set_theme_mod('olivewp_blog_meta_sort',array('blog_category','blog_comment'));
        add_option($theme.'_blg_date', 'old');
    }
    elseif((get_theme_mod('olivewp_plus_enable_category',true)==false ) && (get_theme_mod($theme.'_blg_category','')!='old' ) ){
        set_theme_mod('olivewp_blog_meta_sort',array( 'blog_date','blog_comment'));
        add_option($theme.'_blg_category', 'old');
    }

add_option('olivewp_blog_post_ordering', 'old');
}
