<?php 
$olivewp_shop_button = '<ul class="nav spice-nav spice-right">%3$s';
if(get_theme_mod('after_menu_btn_new_tabl',false)==true) { 
	$olivewp_target="_blank";
}
else { 
	$olivewp_target="_self"; 
}
if((get_theme_mod('after_menu_btn_txt')!='') && (get_theme_mod('after_menu_multiple_option')=='menu_btn')):
	$olivewp_shop_button .= '<li class="menu-item header-button"><a target='.$olivewp_target.' class="theme-btn btn-style-one" href='.get_theme_mod('after_menu_btn_link','').'><span class="txt">'.get_theme_mod('after_menu_btn_txt').'</span></a></li>';
endif;
if((get_theme_mod('after_menu_html')!='') && (get_theme_mod('after_menu_multiple_option')=='html')):
	$olivewp_shop_button .= '<li class="nav-item html menu-item lite-html">'.get_theme_mod('after_menu_html').'</li>';
endif;
if(get_theme_mod('after_menu_multiple_option')=='top_menu_widget'):
	ob_start();
	$sidebar = olivewp_plus_menu_widget_area('menu-widget-area');
	$sidebar = ob_get_contents();
	$olivewp_shop_button .= '<li class="nav-item after-menu-widget">'.$sidebar.'</li>';
    ob_end_clean();
endif;

if(get_theme_mod('search_btn_enable',false)==true) { 
	$olivewp_shop_button .= '<li class="menu-item dropdown search_exists">'; 
}

//Hence This condition only work when woocommerce plugin will be activate
if(get_theme_mod('search_effect_style','toggle')=='toggle' && get_theme_mod('search_btn_enable',false)==true) {
	$olivewp_shop_button .= '<a href="#" title="'.esc_attr__('Search','olivewp-plus').'" class="search-icon dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-search"></i></a>
		<ul class="dropdown-menu pull-right search-panel"  role="menu">
			<li>
				<div class="form-spice-container">
					<form id="searchform" autocomplete="off" role="'.esc_attr('Search','olivewp-plus').'" method="get" class="search-form" action="'.esc_url( home_url( '/' )).'">
						<input type="search" class="search-field" placeholder="'.esc_attr__('Search','olivewp-plus').'" value="" name="s">
						<input type="submit" class="search-submit" value="'.esc_attr__('Search','olivewp-plus').'">
			 	</form>           
				</div>
			</li>
		</ul>
	</li>';
}
elseif(get_theme_mod('search_effect_style','popup_light')=='popup_light' && get_theme_mod('search_btn_enable',false)==true || get_theme_mod('search_effect_style','popup_dark')=='popup_dark'  && get_theme_mod('search_btn_enable',false)==true) {
	$olivewp_shop_button .=' <a href="#searchbar_fullscreen" class="search-icon" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-search"></i></a>';
}
if ( class_exists( 'WooCommerce' ) ) {
	if(get_theme_mod('cart_btn_enable',false)==true ){		   	  
		if(get_theme_mod('search_btn_enable',false)==true) { 
			$olivewp_shop_button .='<li class="menu-item cart-item"><div class="cart-header ">';
		}
		else {
			$olivewp_shop_button .='<li class="menu-item cart-item shop_exists"><div class="cart-header ">';
		}
      	global $woocommerce; 
      	$olivewp_link = function_exists( 'wc_get_cart_url' ) ? wc_get_cart_url() : $woocommerce->cart->get_cart_url();
      	$olivewp_shop_button .= '<a class="cart-icon" href="'.esc_url($olivewp_link).'" >';
	      
      	if ($woocommerce->cart->cart_contents_count == 0){
      		$olivewp_shop_button .= '<i class="fas fa-shopping-cart" aria-hidden="true"></i>';
        }
        else
        {
          	$olivewp_shop_button .= '<i class="fas fa-cart-plus" aria-hidden="true"></i>';
        }
	           
        $olivewp_shop_button .= '</a>';
	        
	    /* translators: %d: count for number of products in cart */
        $olivewp_shop_button .= '<a class="total" href="'.esc_url($olivewp_link).'" ><span class="cart-total">'.sprintf(_n('%d <span>item</span>', '%d <span>items</span> ', $woocommerce->cart->cart_contents_count, 'olivewp-plus' ), $woocommerce->cart->cart_contents_count).'</span></a>';
       $olivewp_shop_button .='</div></li>';
    }
}
$olivewp_shop_button .= '</li>';
$olivewp_shop_button .= '</ul>'; 
$olivewp_menu_class='';
wp_nav_menu( array (
	'theme_location'	=>	'primary', 
	'menu_class'    	=>	'nav spice-nav spice-right '.$olivewp_menu_class.'',
	'items_wrap'    	=>	$olivewp_shop_button,
	'fallback_cb'   	=>	'olivewp_fallback_page_menu',
	'walker'        	=>	new Olivewp_Nav_Walker()
));