<?php

/**
 * Enqueue scripts and styles.
 */
function olivewp_plus_scripts() {

    /* Enqueue the CSS scripts */
    $suffix = ( defined('SCRIPT_DEBUG') && SCRIPT_DEBUG ) ? '' : '.min';

    wp_enqueue_style('olivewp-menu-css', OLIVEWP_TEMPLATE_DIR_URI. '/assets/css/theme-menu.css');
    wp_style_add_data('olivewp-menu-css', 'rtl', 'replace');
    
    wp_enqueue_style( 'olivewp-style', get_stylesheet_uri() );
    wp_style_add_data('olivewp-style', 'rtl', 'replace');

    if (get_theme_mod('custom_color_enable') == true) {
        add_action('wp_footer','olivewp_plus_custom_color_css');
    }
    else {
        $css_name = get_theme_mod('theme_color', 'default.css');
        if($css_name == 'default.css') {
            wp_enqueue_style('olivewp-default', OLIVEWP_TEMPLATE_DIR_URI . '/assets/css/default.css');
        }
        else {
            wp_enqueue_style('olivewp-plus-default-style', OLIVEWP_PLUGIN_URL . 'inc/assets/css/' . $css_name);
        }
    }

    wp_enqueue_script('imgLoad', OLIVEWP_PLUGIN_URL.'inc/assets/js/img-loaded.js', array('jquery'), '', true);
    wp_enqueue_style('owl', OLIVEWP_PLUGIN_URL. 'inc/assets/css/owl.carousel.css');

    /* Enqueue the JS scripts */

    wp_enqueue_script('owl', OLIVEWP_PLUGIN_URL. 'inc/assets/js/owl.carousel' . $suffix . '.js', array('jquery'), '', true);

    wp_enqueue_script('waterfall', OLIVEWP_PLUGIN_URL. 'inc/assets/js/waterfall' . $suffix . '.js', array('jquery'), '', true);

   

    wp_enqueue_script('olivewp-plus-custom-js', OLIVEWP_PLUGIN_URL . 'inc/assets/js/custom-plus.js', array('jquery'), '', true);

    wp_enqueue_script('olivewp-plus-main-js', OLIVEWP_PLUGIN_URL . 'inc/assets/js/main-plus.js', array('jquery'), '', true);

}
add_action( 'wp_enqueue_scripts', 'olivewp_plus_scripts' );