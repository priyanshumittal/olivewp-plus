<?php
/**
 * theme breadcurmbs section
 *
 * @package OliveWP Plus
*/
if (!function_exists('olivewp_plus_breadcrumbs')):

	function olivewp_plus_breadcrumbs() { 
		$archive_class = get_body_class();
		$enable_disable_banner = get_theme_mod('breadcrumb_banner_enable', true); 
		$breadcrumb='';
		$olivewp_breadcrumb_type = get_theme_mod('olivewp_breadcrumb_type','yoast');
		if ($enable_disable_banner == true) { 
			if (is_404()) { ?>
                <section class="page-title-section <?php if (get_theme_mod('header_preset_layout', 'left') == 'seven') {?>standard-page-title <?php } ?>" <?php if ( !empty(get_theme_mod('error_breadcrumb_back_img') && (get_theme_mod('breadcrumb_image_enable',true)==true)) ){ ?> style="background: #17212c url('<?php echo get_theme_mod('error_breadcrumb_back_img');?>'); background-size: cover;"  <?php } ?>>   
            <?php }
            elseif (is_search()) { ?>
                <section class="page-title-section <?php if (get_theme_mod('header_preset_layout', 'left') == 'seven') {?>standard-page-title <?php } ?>" <?php if ( !empty(get_theme_mod('search_breadcrumb_back_img') && (get_theme_mod('breadcrumb_image_enable',true)==true)) ){ ?> style="background: #17212c url('<?php echo get_theme_mod('search_breadcrumb_back_img');?>'); background-size: cover;"  <?php } ?>>   
            <?php }
            elseif (in_array('woocommerce', $archive_class)) { ?>
                <section class="page-title-section <?php if (get_theme_mod('header_preset_layout', 'left') == 'seven') {?>standard-page-title <?php } ?>" <?php if ( !empty(get_theme_mod('author_shop_breadcrumb_back_img') && (get_theme_mod('breadcrumb_image_enable',true)==true)) ){ ?> style="background: #17212c url('<?php echo get_theme_mod('author_shop_breadcrumb_back_img');?>'); background-size: cover;"  <?php } ?>>   
            <?php }
            elseif (in_array('date', $archive_class)) { ?>
                <section class="page-title-section <?php if (get_theme_mod('header_preset_layout', 'left') == 'seven') {?>standard-page-title <?php } ?>" <?php if ( !empty(get_theme_mod('date_archive_breadcrumb_back_img') && (get_theme_mod('breadcrumb_image_enable',true)==true)) ){ ?> style="background: #17212c url('<?php echo get_theme_mod('date_archive_breadcrumb_back_img');?>'); background-size: cover;"  <?php } ?>>   
            <?php } 
            elseif (in_array('author', $archive_class)) { ?>
                <section class="page-title-section <?php if (get_theme_mod('header_preset_layout', 'left') == 'seven') {?>standard-page-title <?php } ?>" <?php if ( !empty(get_theme_mod('author_archive_breadcrumb_back_img') && (get_theme_mod('breadcrumb_image_enable',true)==true)) ){ ?> style="background: #17212c url('<?php echo get_theme_mod('author_archive_breadcrumb_back_img');?>'); background-size: cover;"  <?php } ?>>   
            <?php }
            else { ?>
				<section class="page-title-section <?php if (get_theme_mod('header_preset_layout', 'left') == 'seven') {?>standard-page-title <?php } ?>" <?php if( get_header_image() && (get_theme_mod('breadcrumb_image_enable',true)==true) ){ ?> style="background:#17212c url('<?php header_image(); ?>'); background-size: cover;" <?php } ?>>
				<?php 
			}
					if (get_theme_mod('breadcrumb_overlay_enable', true) == true) {
	                    $breadcrumb_overlay = get_theme_mod('breadcrumb_overlay_color', 'rgba(0,0,0,0.6)');
	                } else {
	                    $breadcrumb_overlay = 'none';
	                }?>
	                <style type="text/css">
	                    .page-title-section .breadcrumb-overlay {
	                        background-color: <?php echo esc_attr($breadcrumb_overlay); ?>;
	                    }
	                </style>
					<?php if(get_theme_mod('breadcrumb_overlay_enable',true)==true): ?>
						<div class="breadcrumb-overlay"></div>
					<?php endif; 
					$olivewp_breadcrumbs_choice=get_post_meta(get_the_ID(),'olivewp_show_breadcrumb', true );
					if(get_theme_mod('bredcrumb_position','page_header')=='page_header'  && get_theme_mod('breadcrumb_enable',true)==true || $olivewp_breadcrumbs_choice == 'olivewp_breadcrumbs_enable'):
					    $breadcrumb_col='3';
				    else:
				    	$breadcrumb_col='1';
				    endif;?>
					<div class="spice-container">
						<div class="spice-row">
						<?php include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
						$breadcrumb_enable_disable	= get_theme_mod('breadcrumb_enable',true);
						
						if($olivewp_breadcrumbs_choice == ''){
		                    if($breadcrumb_enable_disable == true ){
							if($olivewp_breadcrumb_type == 'yoast') {
								if ( function_exists('yoast_breadcrumb') ){
									$seo_bread_title = get_option('wpseo_titles');
								    if($seo_bread_title['breadcrumbs-enable'] == true ) {
								        $breadcrumbs = yoast_breadcrumb("","",false);
								        $breadcrumb='<ul class="page-breadcrumb">
								           				<li>'.wp_kses_post($breadcrumbs).'</li>
								           			</ul>';
								    }							       
								}
							  }
							}
						}
						elseif($olivewp_breadcrumbs_choice == 'olivewp_breadcrumbs_enable'){
						  if($olivewp_breadcrumb_type == 'yoast') {
							if ( function_exists('yoast_breadcrumb') ){
								$seo_bread_title = get_option('wpseo_titles');
							    if($seo_bread_title['breadcrumbs-enable'] == true ) {
							        $breadcrumbs = yoast_breadcrumb("","",false);
							        $breadcrumb='<ul class="page-breadcrumb">
							           				<li>'.wp_kses_post($breadcrumbs).'</li>
							           			</ul>';
							    }							       
							}
						  }
						}
					    if(get_theme_mod('bredcrumb_alignment','parallel')=='parallel'){ 
					    	if(get_theme_mod('bredcrumb_position','page_header')=='page_header'):
					    		echo '<div class="spice-col-3 parallel">';
					    		do_action('olivewp_breadcrumbs_page_title_hook');
					    		echo '</div>';
					    	endif;
					    	if($breadcrumb){echo '<div class="spice-col-'.$breadcrumb_col.' parallel">'.$breadcrumb.'</div>';}
                             elseif(($olivewp_breadcrumb_type == 'rankmath')&& ($breadcrumb_enable_disable == true )) {
								if( function_exists( 'rank_math_the_breadcrumbs' ) ) {
							    echo '<div class="spice-col-'.$breadcrumb_col.' parallel text-right">'; rank_math_the_breadcrumbs();	echo '</div>';
							    }}
						        elseif(($olivewp_breadcrumb_type == 'navxt')&&($breadcrumb_enable_disable == true )) { if( function_exists( 'bcn_display' ) ){
								echo '<div class="spice-col-'.$breadcrumb_col.' parallel text-right"><nav class="navxt-breadcrumb">';
                                 bcn_display();
								echo '</nav></div>';;
                                }  
                               }							
					    }
					    elseif(get_theme_mod('bredcrumb_alignment','parallel')=='parallelr'){
					    	if($breadcrumb){echo '<div class="spice-col-'.$breadcrumb_col.' text-left parallel">'.$breadcrumb.'</div>';}
							  elseif(($olivewp_breadcrumb_type == 'rankmath')&& ($breadcrumb_enable_disable == true )) {
								if( function_exists( 'rank_math_the_breadcrumbs' ) ) {
							    echo '<div class="spice-col-'.$breadcrumb_col.' text-left parallel">'; rank_math_the_breadcrumbs();	echo '</div>';
							    }}
						        elseif(($olivewp_breadcrumb_type == 'navxt')&&($breadcrumb_enable_disable == true )) { if( function_exists( 'bcn_display' ) ){
								echo '<div class="spice-col-'.$breadcrumb_col.' text-left parallel"><nav class="navxt-breadcrumb">';
                                 bcn_display();
								echo '</nav></div>';;
                                }  
                               } 
					    	if(get_theme_mod('bredcrumb_position','page_header')=='page_header'):			    		 
						    	echo '<div class="spice-col-'.$breadcrumb_col.' text-right parallel">';
						    	do_action('olivewp_breadcrumbs_page_title_hook');
						    	echo '</div>';
					    	endif;
					    }
					    elseif(get_theme_mod('bredcrumb_alignment','parallel')=='centered'){ 
					    	echo '<div class="spice-col-1 text-center">';
					    	if(get_theme_mod('bredcrumb_position','page_header')=='page_header'):
					    		do_action('olivewp_breadcrumbs_page_title_hook');
					    	endif;
					    	if($breadcrumb){echo $breadcrumb;}
							elseif(($olivewp_breadcrumb_type == 'rankmath')&& ($breadcrumb_enable_disable == true )) {
								if( function_exists( 'rank_math_the_breadcrumbs' ) ) {
							        rank_math_the_breadcrumbs();	
							    }}
						       elseif(($olivewp_breadcrumb_type == 'navxt')&&($breadcrumb_enable_disable == true )) { if( function_exists( 'bcn_display' ) ){
								echo '<nav class="navxt-breadcrumb">';
                                 bcn_display();
								echo '</nav></div>';;
                                }  
                               }	
					    	echo'</div>';
					    }
					    elseif(get_theme_mod('bredcrumb_alignment','parallel')=='left'){ 
					    	echo '<div class="spice-col-1 text-left">';
					    	if(get_theme_mod('bredcrumb_position','page_header')=='page_header'):			    		
					    		do_action('olivewp_breadcrumbs_page_title_hook');
					    	endif;
					    	if($breadcrumb){echo $breadcrumb;}
							  elseif(($olivewp_breadcrumb_type == 'rankmath')&& ($breadcrumb_enable_disable == true )) {
								if( function_exists( 'rank_math_the_breadcrumbs' ) ) {
							    rank_math_the_breadcrumbs();
							    }}
						        elseif(($olivewp_breadcrumb_type == 'navxt')&&($breadcrumb_enable_disable == true )) { if( function_exists( 'bcn_display' ) ){
								echo '<nav class=" navxt-breadcrumb">';
                                 bcn_display();
								echo '</nav>';;
                                }  
                               }
					    	echo'</div>';
					    }
					    elseif(get_theme_mod('bredcrumb_alignment','parallel')=='right'){ 
					    	echo '<div class="spice-col-1 text-right">';
					    	if(get_theme_mod('bredcrumb_position','page_header')=='page_header'):
					    		do_action('olivewp_breadcrumbs_page_title_hook');
					    	endif;
					    	if($breadcrumb){echo $breadcrumb;}
							elseif(($olivewp_breadcrumb_type == 'rankmath')&& ($breadcrumb_enable_disable == true )) {
								if( function_exists( 'rank_math_the_breadcrumbs' ) ) {
							     rank_math_the_breadcrumbs();
							    }}
						        elseif(($olivewp_breadcrumb_type == 'navxt')&&($breadcrumb_enable_disable == true )) { if( function_exists( 'bcn_display' ) ){
								echo '<nav class="navxt-breadcrumb">';
                                 bcn_display();
								echo '</nav>';;
                                }  
                               }
					    	echo'</div>';
					    }?>
					    </div>
					</div>
				</section>
		<?php
		}
	}

	add_action('olivewp_plus_breadcrumbs_hook','olivewp_plus_breadcrumbs');

endif;