<?php
/**
 * Template Name: Blog List View
 */
get_header();
do_action( 'olivewp_plus_breadcrumbs_hook' );
?>

<section class="page-section-space blog list-view bg-default" id="content">
    <div class="spice-container<?php echo esc_html(olivewp_container_width_post_layout());?>">
        <div class="spice-row">
            <?php
            if(get_theme_mod('bredcrumb_position','page_header')=='content_area'):
                echo '<div class="spice-col-1">';
                do_action('olivewp_breadcrumbs_page_title_hook');
                echo '</div>';
            endif;?>
            <div class="spice-col-1">
            <?php
                if(get_theme_mod('post_navigation_style','pagination') == 'pagination') {
                    if ( get_query_var( 'paged' ) ) { 
                        $paged = get_query_var( 'paged' ); 
                    }
                    elseif ( get_query_var( 'page' ) ) { 
                        $paged = get_query_var( 'page' ); 
                    }
                    else { 
                        $paged = 1; 
                    }
                    $args = array('post_type' => 'post', 'paged' => $paged);
                    $loop = new WP_Query($args);
                    if ($loop->have_posts()):
                        while ($loop->have_posts()): $loop->the_post();
                            //include(OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content.php');
                            include(OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content-list.php');
                        endwhile;
                        wp_reset_query();
                    else:
                        get_template_part('template-parts/content','none');
                    endif;

                    // pagination
                    $obj = new Olivewp_Plus_Pagination();
                    $obj->olivewp_plus_page($loop);
                }
                if(get_theme_mod('post_navigation_style','pagination') != 'pagination')  {
                    echo do_shortcode('[ajax_posts]');
                }
                ?>
            </div>
        </div>
    </div>
</section>  

<?php
get_footer();