<?php
/**
 * Template Name: Blog Masonry 3 Column
 */
get_header();
do_action( 'olivewp_plus_breadcrumbs_hook' );
?>
<section class="page-section-space blog blg-masonry bg-default skip-crop" id="content">
    <div class="spice-container<?php echo esc_html(olivewp_container_width_post_layout());?>">
        <?php 
        if(get_theme_mod('bredcrumb_position','page_header')=='content_area'):
                    echo '<div class="spice-col-1">';
                    do_action('olivewp_breadcrumbs_page_title_hook');
                    echo '</div>';
        endif;
        if(get_theme_mod('post_navigation_style','pagination')=='pagination') { ?><div class="waterfall spice-row">
            <?php
                    if ( get_query_var( 'paged' ) ) { 
                        $paged = get_query_var( 'paged' ); 
                    }
                    elseif ( get_query_var( 'page' ) ) { 
                        $paged = get_query_var( 'page' ); 
                    }
                    else { 
                        $paged = 1; 
                    }
                    $args = array('post_type' => 'post', 'paged' => $paged);
                    $loop = new WP_Query($args);
                    if ($loop->have_posts()):
                        while ($loop->have_posts()): $loop->the_post();
                            echo '<div class="item spice-col-4">';
                            include(OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content.php');
                            echo '</div>';
                        endwhile;
                        wp_reset_query();
                    else:
                        get_template_part('template-parts/content','none');
                    endif; ?>
                    
                    <?php 
                    echo '</div>';
                    echo '<div class="spice-clr"></div>';
                    // pagination
                    $obj = new Olivewp_Plus_Pagination();
                    $obj->olivewp_plus_page($loop);
                }
                if(get_theme_mod('post_navigation_style','pagination') != 'pagination')  {
                    echo do_shortcode('[ajax_posts]');
                }
                ?>
    </div>
</section>  

<?php
get_footer();