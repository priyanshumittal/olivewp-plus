<?php

class Olivewp_Plus_Pagination {

    function olivewp_plus_page() {

        global $post;
        global $wp_query, $loop, $template;
        if (get_query_var('paged')) {
            $paged = get_query_var('paged');
        } 
        elseif (get_query_var('page')) {
            $paged = get_query_var('page');
        } 
        else {
            $paged = 1;
        }
        if(basename($template)!='search.php') {                    
            if ($wp_query->max_num_pages == 0) {
                $wp_query = $loop;
            }
        }
        the_posts_pagination(array(
            'prev_text' =>  __( 'Previous', 'olivewp-plus' ),
            'next_text' =>  __( 'Next', 'olivewp-plus' )
        ));
    }

}