<?php
/**
 * Footer Widget Area
 *
 * @package OliveWP Plus
 */
?>
<div class="spice-row footer-sidebar">
	<?php
        $olivewp_plus_layout = get_theme_mod('footer_widget_layout', 4);
        switch ( $olivewp_plus_layout )
        {  
            case 1:
                olivewp_plus_footer_layout('1');
                break; 
            case 2:
                olivewp_plus_footer_layout('2');
                break;
            case 3:
                olivewp_plus_footer_layout('3');
                break;
            case 4:
                olivewp_plus_footer_layout('4');
                break;
            case 5:
                olivewp_plus_footer_layout('5');
                break;
            case 6:
                olivewp_plus_footer_layout('6');
                break;
            case 7:
                olivewp_plus_footer_layout('7');
                break;
            case 8:
                olivewp_plus_footer_layout('8');
                break;
            case 9:
                olivewp_plus_footer_layout('9');
                break;
        }
    ?>
</div>