<?php
/**
 * Register widget area.
 *
*/
function olivewp_plus_widgets_init() {

    /**
    * Footer Bar 1 widget area
    */
    register_sidebar(
        array(
            'name'          => esc_html__('Footer Bar 1', 'olivewp-plus' ),
            'id'            => 'footer-bar-1',
            'description'   => esc_html__('Add widgets in footer bar widget area 1', 'olivewp-plus' ),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

    /**
    * Footer Bar 2 widget area
    */
    register_sidebar(
        array(
            'name'          => esc_html__('Footer Bar 2', 'olivewp-plus' ),
            'id'            => 'footer-bar-2',
            'description'   => esc_html__('Add widgets in footer bar widget area 2', 'olivewp-plus' ),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

    /**
    * Top Header Sidebar Left area
    */
    register_sidebar(
        array(
            'name'          => esc_html__('Top Header Sidebar Left area', 'olivewp-plus' ),
            'id'            => 'top-header-sidebar-left',
            'description'   => esc_html__('Add widgets in top header left widget area', 'olivewp-plus' ),
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

    /**
    * Top Header Sidebar Right area
    */
    register_sidebar(
        array(
            'name'          => esc_html__('Top Header Sidebar Right area', 'olivewp-plus' ),
            'id'            => 'top-header-sidebar-right',
            'description'   => esc_html__('Add widgets in top header right widget area', 'olivewp-plus' ),
            'before_widget' => '<aside id="%1$s" class="widget spice-right %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

    /**
    * After Menu Widget area
    */
    register_sidebar(
        array(
            'name'          => esc_html__('After Menu widget area', 'olivewp-plus' ),
            'id'            => 'menu-widget-area',
            'description'   => esc_html__('Widgets in this area are used in the after menu region.', 'olivewp-plus' ),
            'before_widget' => '<aside id="%1$s" class="widget spice-right %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<h2 class="widget-title">',
            'after_title'   => '</h2>',
        )
    );

}

add_action('widgets_init', 'olivewp_plus_widgets_init');