<header class="header-sidebar">
	<?php do_action('olivewp_plus_top_bar_header_feature'); ?>
	<nav class="spice spice-custom <?php if(get_theme_mod('sticky_header_enable',false)===true):?>header-sticky<?php endif; if(get_theme_mod('ssh_animation_effect','')=='shrink'): echo ' shrink'; endif; ?> trsprnt-menu" role="navigation">
		<div class="spice-container">
			<div class="spice-header">
			    <?php if(has_custom_logo()) { the_custom_logo(); } do_action('ssh_plus_feature'); ?>
			    <div class="custom-logo-link-url">  
					<h2 class="site-title">
						<a class="site-title-name" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" style="outline: none;"><?php bloginfo( 'name' ); ?></a>
					</h2>
					<?php
					$olivewp_plus_description = get_bloginfo( 'description', 'display' );
					if ( $olivewp_plus_description || is_customize_preview() ) : ?>
						<p class="site-description"><?php echo $olivewp_plus_description; ?></p>
					<?php endif;?>
				</div>
			    <button id="spice-toggle" class="spice-toggle" data-toggle="collapse" type="button" aria-controls="menu" aria-expanded="false">
			    	<i class="fas fa-bars"></i>
			    </button>
			</div>


			<div class="collapse spice-collapse" id="custom-collapse">
				<div class="ml-auto">
					<?php include_once(	OLIVEWP_PLUGIN_DIR.'/inc/menu-search.php');?>
				</div>
			</div>
		</div>
	</nav>
</header>