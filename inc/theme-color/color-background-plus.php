<?php
/**
 * Use Color Background settings in the theme package
 *
 * @package OliveWP Plus
*/
function olivewp_plus_color_back_custom_css() {
    // Get values from the customizer settings
    $olivewp_plus_enable_topbar_color               =   get_theme_mod('enable_topbar_color', false);
    $olivewp_plus_enable_header_back_color          =   get_theme_mod('enable_header_back_color', false);
    $olivewp_plus_enable_header_color               =   get_theme_mod('enable_header_color', false);
    $olivewp_plus_enable_menu_color                 =   get_theme_mod('enable_menu_color', false); 
    $olivewp_plus_enable_after_menu                 =   get_theme_mod('enable_after_menu', false); 
    $olivewp_plus_enable_banner_color               =   get_theme_mod('enable_banner_color', false);
    $olivewp_plus_enable_content_color              =   get_theme_mod('enable_content_color', false);
    $olivewp_plus_enable_blog_page_archive          =   get_theme_mod('enable_blog_page_archive', false);
    $olivewp_plus_enable_single_post                =   get_theme_mod('enable_single_post', false);
    $olivewp_plus_enable_sidebar_color              =   get_theme_mod('enable_sidebar_color', false);
    $olivewp_plus_enable_footer_color               =   get_theme_mod('enable_footer_color', false);
    $olivewp_plus_enable_footer_bar                 =   get_theme_mod('enable_footer_bar', false);
?>
    <style type="text/css">
        body .post .post-thumbnail a {
         margin: unset;
        }
        .spice-row article.post
        {
            background-color:<?php echo esc_attr( get_theme_mod('olivewp_plus_blog_bgcolor', '#ffffff') ); ?>;
        }
        .blog .spice-row article.post.single
        {
            border-width: <?php echo intval(get_theme_mod('olivewp_plus_single_blog_border_top', 0)) ; ?>px <?php echo intval(get_theme_mod('olivewp_plus_single_blog_border_right', 0)) ; ?>px <?php echo  intval(get_theme_mod('olivewp_plus_single_blog_border_bottom', 0)) ; ?>px <?php echo intval(get_theme_mod('olivewp_plus_single_blog_border_left', 0)) ; ?>px; 
            border-style: solid;
             border-color: <?php echo esc_attr( get_theme_mod('olivewp_plus_single_blog_border_color', '#ffffff') ); ?>;
             <?php if(get_theme_mod('olivewp_plus_enable_margin_single_post',false)==true):?>
             margin: <?php echo intval( get_theme_mod('olivewp_plus_single_blog_margin_top', 0) ); ?>px <?php echo intval( get_theme_mod('olivewp_plus_single_blog_margin_right', 0) ); ?>px <?php echo intval( get_theme_mod('olivewp_plus_single_blog_margin_bottom', 30) ); ?>px <?php echo intval( get_theme_mod('olivewp_plus_single_blog_margin_left', 0) ); ?>px;
             <?php endif;?>
             <?php if(get_theme_mod('olivewp_plus_enable_padding_single_post',false)==true):?>
            padding: <?php echo intval( get_theme_mod('olivewp_plus_single_blog_padding_top', 0) ); ?>px <?php echo intval( get_theme_mod('olivewp_plus_single_blog_padding_right', 0) ); ?>px <?php echo intval( get_theme_mod('olivewp_plus_single_blog_padding_bottom', 8) ); ?>px <?php echo intval( get_theme_mod('olivewp_plus_single_blog_padding_left', 0) ); ?>px;
        <?php endif;?>
        }
        .blog .spice-row article.post .entry-meta:not(.footer-meta)
        {   
            <?php if(get_theme_mod('olivewp_enable_meta_border',false)==true):?>
            border-width: <?php echo intval(get_theme_mod('olivewp_plus_meta_border_top', 0)) ; ?>px <?php echo intval(get_theme_mod('olivewp_plus_meta_border_right', 0)) ; ?>px <?php echo  intval(get_theme_mod('olivewp_plus_meta_border_bottom', 0)) ; ?>px <?php echo intval(get_theme_mod('olivewp_plus_meta_border_left', 0)) ; ?>px; 
             border-color: <?php echo esc_attr( get_theme_mod('olivewp_plus_meta_border_color', '#ffffff') ); ?>;
             border-style: solid;
             <?php endif; if(get_theme_mod('olivewp_enable_meta_margin',false)==true):?>
             margin: <?php echo intval( get_theme_mod('olivewp_plus_meta_margin_top', 0) ); ?>px <?php echo intval( get_theme_mod('olivewp_plus_meta_margin_right', 0) ); ?>px <?php echo intval( get_theme_mod('olivewp_plus_meta_margin_bottom', 0) ); ?>px <?php echo intval( get_theme_mod('olivewp_plus_meta_margin_left', 0) ); ?>px;
             <?php endif; if(get_theme_mod('olivewp_enable_meta_padding',false)==true):?>
            padding: <?php echo intval( get_theme_mod('olivewp_plus_meta_padding_top', 8) ); ?>px <?php echo intval( get_theme_mod('olivewp_plus_meta_padding_right', 30) ); ?>px <?php echo intval( get_theme_mod('olivewp_plus_meta_padding_bottom', 8) ); ?>px <?php echo intval( get_theme_mod('olivewp_plus_meta_padding_left', 30) ); ?>px;
        <?php endif;?>
        }
        .blog .spice-row article.post:not(.single)
        {
            border-width: <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_border_top', 0) ); ?>px <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_border_right', 0) ); ?>px <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_border_bottom', 0) ); ?>px <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_border_left', 0) ); ?>px;
            border-color: <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_border_color', '#ffffff') ); ?>;
            border-style: solid;
            <?php 
            if(get_theme_mod('olivewp_plus_enable_margin_padding',false)==true): ?>
            margin: <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_margin_top', 0) ); ?>px <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_margin_right', 0) ); ?>px <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_margin_bottom', 30) ); ?>px <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_margin_left', 0) ); ?>px;
            padding: <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_padding_top', 0) ); ?>px <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_padding_right', 0) ); ?>px <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_padding_bottom', 8) ); ?>px <?php echo esc_attr( get_theme_mod('olivewp_plus_blog_padding_left', 0) ); ?>px;
        <?php endif;?>
          
        }
    </style>

  <?php
    /* ====================
        * Topbar Widgets
    ==================== */
    if($olivewp_plus_enable_topbar_color == true) { ?>
        <style>
            body .spice-topbar .wp-block-search .wp-block-search__label, body .spice-topbar .widget.widget_block h1, body .spice-topbar .widget.widget_block h2, body .spice-topbar .widget.widget_block h3, body .spice-topbar .widget.widget_block h4, body .spice-topbar .widget.widget_block h5, body .spice-topbar .widget.widget_block h6, body .widget .widget-title, body .wc-block-product-search__label {
                color: <?php echo esc_attr( get_theme_mod('topbar_widget_title_color', '#ffffff') ); ?>;
            }
            body .spice-topbar p, body .spice-topbarr .wp-block-calendar table tbody, body .spice-topbar .widget, body .spice-topbar ul li, body .spice-topbar ol li {
                color: <?php echo esc_attr( get_theme_mod('topbar_widget_text_color', '#ffffff') ); ?>;
            }
            body .spice-topbar a:not(body .custom-social-icons li a), body .spice-topbar .widget .wp-block-tag-cloud a {
                color: <?php echo esc_attr( get_theme_mod('topbar_widget_link_color', '#ffffff') ); ?>;
            }
            body .spice-topbar a:hover:not(body .custom-social-icons li a:hover), body .spice-topbar .widget .wp-block-tag-cloud a:hover {
                color: <?php echo esc_attr( get_theme_mod('topbar_widget_link_hover_color', '#ff6f61') ); ?>;
            }
        </style>
    <?php }


    /* ====================
        * Header section (Site title, Tagline) 
    ==================== */

    if($olivewp_plus_enable_header_back_color == true) { ?>
        <style>
            body .spice-custom.trsprnt-menu, .layout1.spice-custom, .layout2.spice-custom, .layout1.spice-custom.shrink, .layout2.spice-custom.shrink {
                background-color: <?php echo esc_attr( get_theme_mod('header_back_color', 'rgba(0, 0, 0, 0)') ); ?>;
            }
        </style>
    <?php }
    if($olivewp_plus_enable_header_color == true) { ?>
        <style>
            body .custom-logo-link-url .site-title a, body .layout1 .custom-logo-link-url .site-title a, body .layout2 .custom-logo-link-url .site-title a {
                color: <?php echo esc_attr( get_theme_mod('site_title_link_color', '#fff') ); ?>;
            }
            body .custom-logo-link-url .site-title a:hover, body .layout1 .custom-logo-link-url .site-title a:hover, body .layout2 .custom-logo-link-url .site-title a:hover {
                color: <?php echo esc_attr( get_theme_mod('site_title_link_hover_color', '#fff') ); ?>;
            }
            body .custom-logo-link-url .site-description, body .layout1 .custom-logo-link-url .site-description, body .layout2 .custom-logo-link-url .site-description {
                color: <?php echo esc_attr( get_theme_mod('tagline_text_color', '#c5c5c5') ); ?>;
            }
        </style>
    <?php }


    /* ====================
        * Primary Menu
    ==================== */
    if($olivewp_plus_enable_menu_color == true) { ?>
        <style>
            body .spice-nav > li.parent-menu a, body .layout1 .spice-nav > li.parent-menu a, body .layout2 .spice-nav > li.parent-menu a, body .spice-custom .spice-nav .dropdown.open > a, body .cart-header > a.cart-icon, body .spice-custom .spice-nav li > a, body .layout1 .cart-header > a.cart-icon, body .layout1.spice-custom .spice-nav li > a, body .layout2 .cart-header > a.cart-icon, body .layout2.spice-custom .spice-nav li > a, body .cart-header > a.total span.cart-total span {
                color: <?php echo esc_attr( get_theme_mod('menu_link_color', '#ffffff') ); ?>;
            }
            body .spice-nav > li.parent-menu a:hover, body .layout1 .spice-nav > li.parent-menu a:hover, body .layout2 .spice-nav > li.parent-menu a:hover, body .spice-custom .spice-nav .open > a:hover, body .spice-custom .spice-nav > .active > a:hover, body .spice-custom .spice-nav .open.active > a:hover, body .layout1.spice-custom .spice-nav .open.active > a:hover, body .layout2.spice-custom .spice-nav .open.active > a:hover, body .layout1.spice-custom .spice-nav li.active > a:hover, body .layout2.spice-custom .spice-nav li.active > a:hover,    body .cart-header > a.cart-icon:hover, body .spice-custom .spice-nav li > a:hover, body .layout1 .cart-header > a.cart-icon:hover, body .layout1.spice-custom .spice-nav li > a:hover, body .layout2 .cart-header > a.cart-icon:hover, body .layout2.spice-custom .spice-nav li > a:hover {
                color: <?php echo esc_attr( get_theme_mod('menu_link_hover_color', '#ff6f61') ); ?>;
            }
            body .spice-custom .spice-nav > .active > a, body .layout1.spice-custom .spice-nav > .active > a, body .layout2.spice-custom .spice-nav > .active > a, body .spice-custom .spice-nav .open .dropdown-menu > .active > a, body .layout1.spice-custom .spice-nav .open .dropdown-menu > .active > a, body .layout2.spice-custom .spice-nav .open .dropdown-menu > .active > a, .spice-custom .spice-nav .open .dropdown-menu > .active > a:hover, .spice-custom .spice-nav .open .dropdown-menu > .active > a:focus, .spice-custom .spice-nav > .active > a, .spice-custom .spice-nav > .active > a:hover, body .spice-custom .spice-nav > .active.open > a, .layout1.spice-custom .spice-nav li.active > a, .layout1.spice-custom .spice-nav li.active > a:hover, .layout2.spice-custom .spice-nav li.active > a, .layout2.spice-custom .spice-nav li.active > a:hover {
                color: <?php echo esc_attr( get_theme_mod('menu_active_link_color', '#ff6f61') ); ?>;
            }
            body .spice-custom .dropdown-menu, body .layout1.spice-custom .dropdown-menu, body .layout2.spice-custom .dropdown-menu, body .spice-custom .open .dropdown-menu, body .layout1.spice-custom .open .dropdown-menu, body .layout2.spice-custom .open .dropdown-menu {
                background-color: <?php echo esc_attr( get_theme_mod('submenu_bg_color', '#21202e') ); ?>;
            }
            body .spice-custom .dropdown-menu > li > a, body .layout1.spice-custom .dropdown-menu > li > a, body .layout2.spice-custom .dropdown-menu > li > a, body .spice-custom .spice-nav .open > a, body .spice-custom .spice-nav .dropdown-menu .open > a {
                color: <?php echo esc_attr( get_theme_mod('submenu_link_color', '#d5d5d5') ); ?>;
            }
            body .spice-custom .spice-nav .dropdown-menu > li > a:hover, body .layout1.spice-custom .spice-nav .dropdown-menu > li > a:hover, body .layout2.spice-custom .spice-nav .dropdown-menu > li > a:hover, body .spice-custom .spice-nav .open .dropdown-menu > .active > a:hover {
                color: <?php echo esc_attr( get_theme_mod('submenu_link_hover_color', '#ffffff') ); ?>;
            }
        </style>
    <?php }


    /* ====================
        * After Menu Button
    ==================== */
    if($olivewp_plus_enable_after_menu == true) { ?>
        <style>
            body .spice.spice-custom .header-button a {
                background-color: <?php echo esc_attr( get_theme_mod('after_menu_back_color', '#ff6f61') ); ?>;
                border: 2px solid <?php echo esc_attr( get_theme_mod('after_menu_back_color', '#ff6f61') ); ?>;
            }
            body .spice.spice-custom .header-button a, body .layout1.spice.spice-custom .header-button a, body .layout2.spice.spice-custom .header-button a, body .spice.spice-custom .header-button a:hover, body .stickymenu.spice-custom .spice-nav li.header-button a, body .stickymenu.spice-custom .spice-nav li.header-button a:hover, body .spice-custom.layout1 .spice-nav li.header-button a:hover, body .spice-custom.layout2 .spice-nav li.header-button a:hover {
                color: <?php echo esc_attr( get_theme_mod('after_menu_text_color', '#fff') ); ?>;
            }
            body .spice.spice-custom .header-button a:hover {
                background-color: <?php echo esc_attr( get_theme_mod('after_menu_button_hover_color', '#ff6f61') ); ?>;
                border: 2px solid <?php echo esc_attr( get_theme_mod('after_menu_button_hover_color', '#ff6f61') ); ?>;
            }
        </style>
    <?php }



    /* ====================
        * Banner
    ==================== */
    if($olivewp_plus_enable_banner_color == true) { ?>
        <style>
            body .page-title-section  .page-title h1,
            body .page-title.content-area-title :is(span,h1,h2,h3,h4,h5,h6,p,div),
            body .page-title:not(.page-section-space.blog .page-title) :is(span,h1,h2,h3,h4,h5,h6,p,div) {
                color: <?php echo esc_attr( get_theme_mod('breadcrumb_banner_title_color', '#ffffff') ); ?>;
            }
            body .page-breadcrumb > li a, body .page-breadcrumb > li , body .rank-math-breadcrumb a, .navxt-breadcrumb, .rank-math-breadcrumb span , body .navxt-breadcrumb span a  {
                color: <?php echo esc_attr( get_theme_mod('breadcrumb_breadcrumb_link_color', '#ffffff') ); ?>;
            }
            body .page-breadcrumb > li a:hover,  body .rank-math-breadcrumb a:hover, body .navxt-breadcrumb span a:hover  {
                color: <?php echo esc_attr( get_theme_mod('breadcrumb_breadcrumb_link_hover_color', '#ff6f61') ); ?>;
            }
        </style>
    <?php }
  


    /* ====================
        * Content (H1---H6, paragraph) 
    ==================== */
    if($olivewp_plus_enable_content_color == true) { ?>
        <style>
            body .entry-content h1, body.woocommerce .product .product_title, body .page-section-full h1 {
                color: <?php echo esc_attr( get_theme_mod('h1_color', '#000000') ); ?>;
            }
            body .entry-content h2, body h2.woocommerce-loop-product__title, body .page-section-full h2 {
                color: <?php echo esc_attr( get_theme_mod('h2_color', '#000000') ); ?>;
            }
            body .entry-content h3, body .comment-form h3, body .comment-title h3, body .page-section-full h3 {
                color: <?php echo esc_attr( get_theme_mod('h3_color', '#000000') ); ?>;
            }
            body .entry-content h4, body .blog-author-info h4, body .page-section-full h4 {
                color: <?php echo esc_attr( get_theme_mod('h4_color', '#000000') ); ?>;
            }
            body .entry-content h5, body .blog-author h5, body .comment-body h5, body .page-section-full h5 {
                color: <?php echo esc_attr( get_theme_mod('h5_color', '#000000') ); ?>;
            }
            body .entry-content h6, body .page-section-full h6 {
                color: <?php echo esc_attr( get_theme_mod('h6_color', '#000000') ); ?>;
            }
            body .entry-content p, body .woocommerce-page .product p, body .woocommerce-product-details__short-description p, body .blog-author-info p, body .blog .sticky.post .entry-content p, body .page-section-full p, body .entry-content table, body .entry-content table a, body .entry-content dl, body .entry-content ul, body .entry-content ol, body .entry-content address, body .entry-content cite, body .entry-content pre, body .page-section-full table, body .page-section-full table a, body .page-section-full dl, body .page-section-full ul, body .page-section-full ol, body .page-section-full address, body .page-section-full cite, body .page-section-full pre {
                color: <?php echo esc_attr( get_theme_mod('p_color', '#858585') ); ?>;
            }
            body form.search-form input.search-submit, body input[type="submit"], body .sidebar .wp-block-search .wp-block-search__button, body .post .entry-content .more-link, body .woocommerce button.button, body .woocommerce .return-to-shop a.button, body .woocommerce a.button.alt, body .woocommerce button.button.alt, body .woocommerce a.button, body.woocommerce-page ul.products li.product .button, body.woocommerce div.product form.cart .button, body.woocommerce #respond input#submit, body.woocommerce a.added_to_cart, body .wp-block-button__link, body .blog .sticky.post .entry-content .more-link  {
                color: <?php echo esc_attr( get_theme_mod('button_color', '#ffffff') ); ?>;
            }
            body form.search-form input.search-submit, body input[type="submit"], body .sidebar .wp-block-search .wp-block-search__button, body .post .entry-content .more-link, body .woocommerce button.button, body .woocommerce .return-to-shop a.button, body .woocommerce a.button.alt, body .woocommerce button.button.alt, body .woocommerce a.button, body.woocommerce-page ul.products li.product .button, body.woocommerce div.product form.cart .button, body.woocommerce #respond input#submit, body.woocommerce a.added_to_cart, body .wp-block-button__link, body .blog .sticky.post .entry-content .more-link {
                background-color: <?php echo esc_attr( get_theme_mod('button_back_color', '#ff6f61') ); ?>;
                border-color: <?php echo esc_attr( get_theme_mod('button_back_color', '#ff6f61') ); ?>;
            }
            body form.search-form input.search-submit:hover, body input[type="submit"]:hover, body .sidebar .wp-block-search .wp-block-search__button:hover, body .post .entry-content .more-link:hover, body .woocommerce button.button:hover, body .woocommerce .return-to-shop a.button:hover, body .woocommerce button.button:disabled[disabled]:hover, body .woocommerce a.button.alt:hover, body .woocommerce button.button.alt:hover, body .woocommerce a.button:hover, body.woocommerce-page ul.products li.product .button:hover, body.woocommerce div.product form.cart .button:hover, body.woocommerce #respond input#submit:hover, body.woocommerce a.added_to_cart:hover, body .wp-block-button__link:hover, body .blog .sticky.post .entry-content .more-link:hover {
                color: <?php echo esc_attr( get_theme_mod('button_hover_color', '#ffffff') ); ?>;
            }
            body form.search-form input.search-submit:hover, body input[type="submit"]:hover, body .sidebar .wp-block-search .wp-block-search__button:hover, body .post .entry-content .more-link:hover, body .woocommerce button.button:hover, body .woocommerce .return-to-shop a.button:hover, body .woocommerce button.button:disabled[disabled]:hover, body .woocommerce a.button.alt:hover, body .woocommerce button.button.alt:hover, body .woocommerce a.button:hover, body.woocommerce-page ul.products li.product .button:hover, body.woocommerce div.product form.cart .button:hover, body.woocommerce #respond input#submit:hover, body.woocommerce a.added_to_cart:hover, body .wp-block-button__link:hover, body .blog .sticky.post .entry-content .more-link:hover {
                background-color: <?php echo esc_attr( get_theme_mod('button_back_hover_color', '#ff6f61') ); ?>;
                border-color: <?php echo esc_attr( get_theme_mod('button_back_hover_color', '#ff6f61') ); ?>;
            }
        </style>
    <?php }


    /* ====================
    * Blog Page/Archive
    ==================== */
    if($olivewp_plus_enable_blog_page_archive == true) { ?>
        <style>
            body .entry-header h3.entry-title a:not(.single .entry-header h3.entry-title a) {
                color: <?php echo esc_attr( get_theme_mod('blog_page_title_color', '#000000') ); ?>;
            }
            body .entry-header h3.entry-title a:hover:not(.single .entry-header h3.entry-title a)  {
                color: <?php echo esc_attr( get_theme_mod('blog_page_title_hover_color', '#ff6f61') ); ?>;
            }
            body .entry-meta a:not(.single .entry-meta a), body .entry-meta span:not(.related-posts .entry-meta span,.single .entry-meta span), body .blog .sticky.post .entry-meta a, body .blog .sticky.post .entry-meta span {
                color: <?php echo esc_attr( get_theme_mod('blog_page_meta_link_color', '#858585') ); ?>;
            }
            body .entry-meta a:hover:not(.single .entry-meta a), body .entry-meta span:hover:not(.related-posts .entry-meta span,.single .entry-meta span), body .blog .sticky.post .entry-meta a:hover, body .blog .sticky.post .entry-meta span:hover {
                color: <?php echo esc_attr( get_theme_mod('blog_page_meta_link_hover_color', '#ff6f61') ); ?>;
            }
        </style>
    <?php }


    /* ====================
    * Single Post
    ==================== */
    if($olivewp_plus_enable_single_post == true) { ?>
        <style>
            body.single .entry-header h3.entry-title, body.single .related-posts .entry-header h3.entry-title a {
                color: <?php echo esc_attr( get_theme_mod('single_post_title_color', '#000000') ); ?>;
            }
            body.single .entry-meta a, body.single .entry-meta span {
                color: <?php echo esc_attr( get_theme_mod('single_post_meta_link_color', '#858585') ); ?>;
            }
            body.single .entry-meta a:hover, body.single .entry-meta span:hover {
                color: <?php echo esc_attr( get_theme_mod('single_post_meta_link_hover_color', '#ff6f61') ); ?>;
            }
        </style>
    <?php }


    /* ====================
        * Sidebar 
    ==================== */
    if($olivewp_plus_enable_sidebar_color == true) { ?>
        <style>
            body .sidebar .wp-block-search .wp-block-search__label, body .sidebar .widget.widget_block h1, body .sidebar .widget.widget_block h2, body .sidebar .widget.widget_block h3, body .sidebar .widget.widget_block h4, body .sidebar .widget.widget_block h5, body .sidebar .widget.widget_block h6, body .widget .widget-title, body .wc-block-product-search__label {
                color: <?php echo esc_attr( get_theme_mod('sidebar_title_color', '#000000') ); ?>;
            }
            body .sidebar p, body .sidebar .wp-block-calendar table tbody {
                color: <?php echo esc_attr( get_theme_mod('sidebar_text_color', '#858585') ); ?>;
            }
            body .sidebar ul li a, body .sidebar ol li a, body .sidebar .widget p a, body .woocommerce ul.cart_list li a, body .woocommerce ul.product_list_widget li a, body .widget .tagcloud a, body .sidebar .widget .wp-block-tag-cloud a {
                color: <?php echo esc_attr( get_theme_mod('sidebar_link_color', '#858585') ); ?>;
            }
            body .sidebar ul li a:hover, body .sidebar ol li a:hover, body .sidebar .widget p a:hover {
                color: <?php echo esc_attr( get_theme_mod('sidebar_link_hover_color', '#ff6f61') ); ?>;
            }
        </style>
    <?php }


    /* ====================
        * Footer 
    ==================== */
    if($olivewp_plus_enable_footer_color == true) { ?>
        <style>
            body .site-footer {
                background-color: <?php echo esc_attr( get_theme_mod('footer_bg_color', '#000000') ); ?>;
            }
            body .footer-sidebar .wp-block-search .wp-block-search__label, body .footer-sidebar .widget.widget_block h1, body .footer-sidebar .widget.widget_block h2, body .footer-sidebar .widget.widget_block h3, body .footer-sidebar .widget.widget_block h4, body .footer-sidebar .widget.widget_block h5, body .footer-sidebar .widget.widget_block h6, body .footer-sidebar .widget .widget-title {
                color: <?php echo esc_attr( get_theme_mod('footer_title_color', '#ffffff') ); ?>;
            }
            body .footer-sidebar p, body .footer-sidebar .wp-block-calendar table tbody, body .footer-sidebar .widget, body .footer-sidebar address {
                color: <?php echo esc_attr( get_theme_mod('footer_text_color', '#858585') ); ?>;
            }
            body .footer-sidebar a, body .footer-sidebar .widget .wp-block-tag-cloud a {
                color: <?php echo esc_attr( get_theme_mod('footer_link_color', '#ffffff') ); ?>;
            }
            body .footer-sidebar a:hover, body .footer-sidebar .widget .wp-block-tag-cloud a:hover {
                color: <?php echo esc_attr( get_theme_mod('footer_link_hover_color', '#ff6f61') ); ?>;
            }
        </style>
    <?php }

    /* ====================
        * Footer Bar
    ==================== */
    if($olivewp_plus_enable_footer_bar == true) { ?>
        <style>
            body .site-info {
                background-color: <?php echo esc_attr( get_theme_mod('footer_bar_back_color', '#000') ); ?>;
            }
            body .site-info .wp-block-search .wp-block-search__label, body .site-info .widget.widget_block h1, body .site-info .widget.widget_block h2, body .site-info .widget.widget_block h3, body .site-info .widget.widget_block h4, body .site-info .widget.widget_block h5, body .site-info .widget.widget_block h6 {
                color: <?php echo esc_attr( get_theme_mod('footer_bar_title_color', '#ffffff') ); ?>;
            }
            body .site-info p, body .site-info {
                color: <?php echo esc_attr( get_theme_mod('footer_bar_text_color', '#ffffff') ); ?>;
            }
            body .site-info a {
                color: <?php echo esc_attr( get_theme_mod('footer_bar_link_color', '#ffffff') ); ?>;
            }
            body .site-info a:hover, body .site-footer .site-info .footer-nav li a:hover {
                color: <?php echo esc_attr( get_theme_mod('footer_bar_link_hover_color', '#ff6f61') ); ?>;
            }
        </style>
    <?php }

}
add_action('wp_head', 'olivewp_plus_color_back_custom_css');