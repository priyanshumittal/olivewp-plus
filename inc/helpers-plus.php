<?php
/**
 * This file includes helper functions used throughout the theme.
 *
 * @package OliveWP Plus
 */

/*
-------------------------------------------------------------------------------
 Table of contents 
-------------------------------------------------------------------------------*/

	# Display Button on Archive/Blog Page
	# Related Posts
	# Preloader
	# Header Preset
	# Scroll to Top
	# Enqueue file for customizer preview
	# Container Width
	# Container Width for Page Layout
	# Container Width for Post Layout
	# Container Width for Single Post Layout
	# Custom Post Navigation
	# Load More Post Navigation
	# Enqueue Js Script for Load More Post Navigation
	# Footer Widget Layout
	# Footer Widget Layout Section
	# Footer Section
	# Footer Bar Layout 1
	# Footer Bar Layout 2
	# Footer Bar Menu
	# Footer Background Image CSS
	# Top Bar Header
	# Menu Widget Area
	# Predefined Default Background
	# Apply Additional Styling

/*
-------------------------------------------------------------------------------
 Display Button on Archive/Blog Page
-------------------------------------------------------------------------------*/

if ( ! function_exists( 'olivewp_plus_readmore_button' ) ) {

	function olivewp_plus_readmore_button() {
        if (get_theme_mod('olivewp_plus_enable_post_read_more', true) == true):
            $blog_button = get_theme_mod('blog_button_title', 'Read More');
            if (empty($blog_button)) {
                return;
            }
            echo '<a href="' . esc_url(get_the_permalink()) . '" class="more-link">' . esc_html($blog_button) . '<i class="fas fa-chevron-right"></i></a>';
        endif;
    }
	
}



/*
-------------------------------------------------------------------------------
 Related Posts
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_related_posts')) {

	function olivewp_plus_related_posts() {
		wp_reset_postdata();
        global $post;

        // Define shared post arguments
        $args = array(
			'no_found_rows'				=> true,
			'update_post_meta_cache' 	=> false,
			'update_post_term_cache' 	=> false,
			'ignore_sticky_posts' 		=> 1,
			'orderby' 					=> 'rand',
			'post__not_in' 				=> array($post->ID),
			'posts_per_page' 			=> 10
        );
        // Related posts by categories
        if (get_theme_mod('olivewp_plus_related_post_option') == 'categories') {
            $cats = get_post_meta($post->ID, 'related-cat', true);
            if (!$cats) {
                $cats = wp_get_post_categories($post->ID, array('fields' => 'ids'));
                $args['category__in'] = $cats;
            } else {
                $args['cat'] = $cats;
            }
        }
        // Related posts by tags
        if (get_theme_mod('olivewp_plus_related_post_option') == 'tags') {
            $tags = get_post_meta($post->ID, 'related-tag', true);
            if (!$tags) {
                $tags = wp_get_post_tags($post->ID, array('fields' => 'ids'));
                $args['tag__in'] = $tags;
            } else {
                $args['tag_slug__in'] = explode(',', $tags);
            }
            if (!$tags) {
                $break = true;
            }
        }
        $query = !isset($break) ? new WP_Query($args) : new WP_Query;
        return $query;
    }

}



/*
-------------------------------------------------------------------------------
 Preloader
-------------------------------------------------------------------------------*/
if ( ! function_exists( 'olivewp_plus_preloader_feature' ) ) {

	function olivewp_plus_preloader_feature() {
		if( get_theme_mod('preloader_enable',false) == true ):
			$preload_layout = get_theme_mod('preloader_style',1);
			if( $preload_layout == 1 ) { ?>
				<div id="preloader1" class="olivewp-loader">
			        <div class="olivewp-preloader-cube">
				        <div class="olivewp-cube1 olivewp-cube"></div>
				        <div class="olivewp-cube2 olivewp-cube"></div>
				        <div class="olivewp-cube4 olivewp-cube"></div>
				        <div class="olivewp-cube3 olivewp-cube"></div>
			    	</div> 
			    </div>
	  		<?php }
	  		if( $preload_layout == 2 ) { ?>
	  			<div id="preloader2" class="olivewp-loader">
				    <div class="loader-2">
					    <div class="square"></div>
					    <div class="path">
					        <div></div><div></div><div></div><div></div><div></div><div></div><div></div>
					    </div>
				  </div>
				</div>
	  		<?php }
	  		if( $preload_layout == 3 ) { ?>
	  			<div id="preloader3" class="olivewp-loader">
					<div class="loader">
					  	<div><span></span><span></span></div><div><span></span><span></span></div><div><span></span><span></span></div>
						<div><span></span><span></span></div>
					</div>
    			</div>
	  		<?php }
	  		if( $preload_layout == 4 ) { ?>
	  			<div id="preloader4" class="olivewp-loader">
					<div class="loader-4">
						<span class="loader-inner-1"></span><span class="loader-inner-2"></span><span class="loader-inner-3"></span><span class="loader-inner-4"></span>
					</div>
				</div>
	  		<?php }
	  		if( $preload_layout == 5 ) { ?>
	  			<div id="preloader5" class="olivewp-loader">
					<div class="loader-5"><span></span><span></span><span></span><span></span></div>
				</div>
	  		<?php }
	  		if( $preload_layout == 6 ) { ?>
	  			<div id="preloader6" class="olivewp-loader">
					<div class="loader-6">
						<div class="inner_loader"></div><div class="inner_loader"></div><div class="inner_loader"></div><div class="inner_loader"></div><div class="inner_loader"></div>
					</div>
				</div>
	  		<?php }
	  	endif;
	}
	add_action('olivewp_plus_preloader','olivewp_plus_preloader_feature');

}



/*
-------------------------------------------------------------------------------
 Header Preset
-------------------------------------------------------------------------------*/
if ( ! function_exists( 'olivewp_plus_header_preset' ) ) {

	function olivewp_plus_header_preset() {
	    if (get_theme_mod('header_preset_layout', 'left')):
	        $header_logo_path= OLIVEWP_PLUGIN_DIR.'/inc/header-preset/menu-with-'.get_theme_mod('header_preset_layout','left').'-logo.php';
	        include_once($header_logo_path);
	    endif;
	    if (get_theme_mod('search_effect_style', 'toggle') != 'toggle'): ?>
	        <div id="searchbar_fullscreen" <?php if (get_theme_mod('search_effect_style', 'popup_light') == 'popup_light'): ?> class="bg-light" <?php endif; ?>>
	            <button type="button" class="close">×</button>
	            <form method="get" id="searchform" autocomplete="off" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
	            	<label>
	            		<input autofocus type="search" class="search-field" placeholder="<?php echo esc_attr_x('Search', 'placeholder', 'olivewp-plus' ); ?>" value="" name="s" id="s" autofocus>
	            	</label>
	            	<input type="submit" class="search-submit btn" value="<?php echo esc_html__('Search', 'olivewp-plus'); ?>">
	            </form>
	        </div>
	    <?php
	    endif;
	}
	add_action('olivewp_plus_header','olivewp_plus_header_preset');

}




/*
-------------------------------------------------------------------------------
 Scroll to Top
-------------------------------------------------------------------------------*/
if ( ! function_exists( 'olivewp_plus_scroll_to_top' ) ) {

	function olivewp_plus_scroll_to_top() {
		$scrolltotop_icon_enable = get_theme_mod('scrolltotop_setting_enable', true);
    	if ($scrolltotop_icon_enable == true) { ?>
        	<div class="scroll-up custom <?php echo get_theme_mod('scroll_to_top_position','right'); ?>"><a href="#totop"><i class="<?php echo get_theme_mod('scroll_to_top_icon_class', 'fa fa-arrow-up'); ?>"></i></a></div>
    	<?php } ?>
    	<style type="text/css">
    		.scroll-up {
    			<?php echo get_theme_mod('scroll_to_top_position','right'); ?>: 3.75rem;
    		}
    		.scroll-up.left { right: auto; }
    		.scroll-up.custom a {
		        border-radius: <?php echo get_theme_mod('scroll_to_top_button_radious', 3); ?>px;
		    }
    	</style>
    	<?php if(get_theme_mod('scroll_to_top_color_enable',false)==true) { ?>
    		<style type="text/css">
    			.scroll-up.custom a {
				    background: <?php echo get_theme_mod('scroll_to_top_back_color','#ff6f61');?>;
				    color: <?php echo get_theme_mod('scroll_to_top_icon_color','#fff');?>;
    			}
    			.scroll-up.custom a:hover {
				    background: <?php echo get_theme_mod('scroll_to_top_back_hover_color','#ff6f61');?>;
				    color: <?php echo get_theme_mod('scroll_to_top_icon_hover_color','#fff');?>;
    			}
    		</style>
    	<?php }
	}
	add_action('olivewp_plus_scrolltotop','olivewp_plus_scroll_to_top');

}



/*
-------------------------------------------------------------------------------
 Enqueue file for customizer preview
-------------------------------------------------------------------------------*/
if ( ! function_exists( 'olivewp_plus_customizer_preview' ) ) {

	function olivewp_plus_customizer_preview() {
		wp_enqueue_script( 'olivewp-plus-customizer-preview-js', OLIVEWP_PLUGIN_URL .'/inc/customizer/assets/js/customizer-preview-plus.js', array( 'customize-preview', 'jquery' ) );
	}
	add_action('customize_preview_init','olivewp_plus_customizer_preview', 11);

}



/*
-------------------------------------------------------------------------------
 Container Width
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_container_width')) :

	function olivewp_plus_container_width() {  ?>
		<style>
			.page-section-space .spice-container, .section-space .spice-container:not(.page-section-space.stretched .spice-container, .section-space.stretched .spice-container) {
				width: <?php echo intval( get_theme_mod('container_width','1200') );?>px;
				max-width: 100%;
			}
			body .page-section-space .spice-row .spice-col-2{
				    width: <?php echo get_theme_mod('content_width','66.6') ;?>%;
			}
			body .page-section-space .spice-row .spice-col-4{
					width: <?php echo  get_theme_mod('sidebar_width','33.3');?>%;
			}
			@media(max-width: 691px){
				body .page-section-space .spice-row .spice-col-2{
				    width: 100%;
				}
				body .page-section-space .spice-row .spice-col-4{
						width: 100%;
				}
			}
		</style>
	<?php }
	add_action('wp_head','olivewp_plus_container_width');

endif;



/*
-------------------------------------------------------------------------------
 Container Width for Page Layout
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_container_width_page_layout')) :

	function olivewp_container_width_page_layout() {  
		if(get_theme_mod('page_layout_width','default')=='full_width_fluid') {
			$container_width = "-fluid";
		}
		elseif(get_theme_mod('page_layout_width','default')=='full_width_streatched') {
			$container_width = "-streatched";
		}
		else {
  			$container_width = "";
		}
		return $container_width;
	}

endif;



/*
-------------------------------------------------------------------------------
 Container Width for Post Layout
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_container_width_post_layout')) :

	function olivewp_container_width_post_layout() {  
		if(get_theme_mod('blog_page_layout_width','default')=='full_width_fluid') {
			$container_width = "-fluid";
		}
		elseif(get_theme_mod('blog_page_layout_width','default')=='full_width_streatched') {
			$container_width = "-streatched";
		}
		else {
  			$container_width = "";
		}
		return $container_width;
	}

endif;



/*
-------------------------------------------------------------------------------
 Container Width for Single Post Layout
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_container_width_single_post_layout')) :

	function olivewp_container_width_single_post_layout() {  
		if(get_theme_mod('single_post_layout_width','default')=='full_width_fluid') {
			$container_width = "-fluid";
		}
		elseif(get_theme_mod('single_post_layout_width','default')=='full_width_streatched') {
			$container_width = "-streatched";
		}
		else {
  			$container_width = "";
		}
		return $container_width;
	}

endif;

/*
-------------------------------------------------------------------------------
 Custom Post Navigation
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_custom_navigation')) :

    function olivewp_plus_custom_navigation() {
    	global $loop;
        if(get_theme_mod('post_navigation_style','pagination')=='pagination') {
    		$obj = new Olivewp_Plus_Pagination();
            $obj->olivewp_plus_page($loop);
        }
        elseif(get_theme_mod('post_navigation_style','pagination') != 'pagination')  {
            echo do_shortcode('[ajax_posts]');
        }
    }
endif;
add_action('olivewp_plus_page_navigation', 'olivewp_plus_custom_navigation');



/*
-------------------------------------------------------------------------------
 Load More Post Navigation
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_ajax_script_load_more')) :

	function olivewp_plus_ajax_script_load_more($args) {

		if((get_theme_mod('olivewp_plus_blog_col_feature',2)==2))
		{
			$olivewp_ms_col=3;
		}
		else if((get_theme_mod('olivewp_plus_blog_col_feature',2)==3))
		{
			$olivewp_ms_col=4;
		}
		else
		{
			$olivewp_ms_col=5;
		}
    	global $template;
        //init ajax
        $ajax = false;
        //check ajax call or not
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $ajax = true;
        }
        //number of posts per page default
        $num = get_option('posts_per_page');
        //page number

        $paged = empty($_POST['page']) ? 0 : $_POST['page'] + 1;

        //args
        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => $num,
            'paged' => $paged
        );
        $i=0;


        $page_template = empty($_POST['ajaxPage_template']) ? '' : $_POST['ajaxPage_template'];

        //query
        $query = new WP_Query($args);
        //check
        if ($query->have_posts()):
            //loop articales
            while ($query->have_posts()): $query->the_post();
                //include articles template

                if (($page_template == 'template-blog-full-width.php') || (basename($template) == 'template-blog-full-width.php')) {
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content-blog.php');
                } 
                elseif (($page_template == 'template-blog-grid-view-sidebar.php') || (basename($template) == 'template-blog-grid-view-sidebar.php')) {
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content.php');
                } 
                elseif (($page_template == 'template-blog-grid-view.php') || (basename($template) == 'template-blog-grid-view.php')) {
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content.php');
                } 
                 elseif (($page_template == 'template-blog-masonry-two-column.php') || (basename($template) == 'template-blog-masonry-two-column.php')) {
                 	echo '<div class="item spice-col-3">';
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content.php');
                    echo '</div>';
                }
                elseif (($page_template == 'template-blog-masonry-three-column.php') || (basename($template) == 'template-blog-masonry-three-column.php')) {
                 	echo '<div class="item spice-col-4">';
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content.php');
                    echo '</div>';
                }
                elseif (($page_template == 'template-blog-masonry-four-column.php') || (basename($template) == 'template-blog-masonry-four-column.php')) {
                 	echo '<div class="item spice-col-5">';
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content.php');
                    echo '</div>';
                }
                elseif (($page_template == 'template-blog-left-sidebar.php') || (basename($template) == 'template-blog-left-sidebar.php')) {
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content-blog.php');
                } 
                elseif (($page_template == 'template-blog-list-view-sidebar.php') || (basename($template) == 'template-blog-list-view-sidebar.php')) {
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content-list.php');
                } 
                elseif (($page_template == 'template-blog-list-view.php') || (basename($template) == 'template-blog-list-view.php')) {
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content-list.php');
                }
                elseif (($page_template == 'template-blog-switcher-view-sidebar.php') || (basename($template) == 'template-blog-switcher-view-sidebar.php')) {
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content-switcher.php');
                } 
                elseif (($page_template == 'template-blog-switcher-view.php') || (basename($template) == 'template-blog-switcher-view.php')) {
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content-switcher.php');
                } 
                elseif (($page_template == 'template-blog-right-sidebar.php') || (basename($template) == 'template-blog-right-sidebar.php')) {
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content-blog.php');
                }
                elseif (($page_template == 'index.php') && (get_theme_mod('olivewp_plus_grid_style_feature','default')=='masonry') && (get_theme_mod('olivewp_plus_blog_layout_feature','default')=='grid') ) {
                	echo '<div class="item spice-col-'.$olivewp_ms_col.'">';
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content.php');
                    echo '</div>';
                }
                elseif (($page_template == 'index.php') && (get_theme_mod('olivewp_plus_grid_style_feature','default')=='default') && (get_theme_mod('olivewp_plus_blog_layout_feature','default')=='grid') ) {
                	echo '<div class="olive-grid-column">';
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content.php');
                    echo '</div>';
                }
                elseif (($page_template == 'index.php') && (get_theme_mod('olivewp_plus_blog_layout_feature','default')=='list') ) {
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content-list.php');
                }
                elseif (($page_template == 'index.php')  ) {
                    include (OLIVEWP_PLUGIN_DIR.'/inc/template-parts/content.php');
                }
                $i++;
            endwhile;
        else:
        	echo 0;
        endif;
        //reset post data
        wp_reset_postdata();
        //check ajax call
        if ($ajax)
        	die();
    }
    /*
     * load more script ajax hooks
     */
    add_action('wp_ajax_nopriv_olivewp_plus_ajax_script_load_more', 'olivewp_plus_ajax_script_load_more');
    add_action('wp_ajax_olivewp_plus_ajax_script_load_more', 'olivewp_plus_ajax_script_load_more');

endif;


/*
-------------------------------------------------------------------------------
 Enqueue Js Script for Load More Post Navigation
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_ajax_enqueue_script')) :

	function olivewp_plus_ajax_enqueue_script() {
	    global $template;
	    if ((basename($template) == 'template-blog-full-width.php') || (basename($template) == 'template-blog-grid-view-sidebar.php') || (basename($template) == 'template-blog-grid-view.php') || (basename($template) == 'template-blog-masonry-two-column.php') || (basename($template) == 'template-blog-masonry-three-column.php') || (basename($template) == 'template-blog-masonry-four-column.php') || (basename($template) == 'template-blog-left-sidebar.php') || (basename($template) == 'template-blog-list-view-sidebar.php') || (basename($template) == 'template-blog-list-view.php') || (basename($template) == 'template-blog-switcher-view-sidebar.php') || (basename($template) == 'template-blog-switcher-view.php') || (basename($template) == 'template-blog-right-sidebar.php') || (basename($template) == 'index.php') ) {
	            wp_enqueue_script('script_ajax', OLIVEWP_PLUGIN_URL.'/inc/assets/js/load_more_ajax.js', array('jquery'), '1.0', true);
	    }
	}
	add_action('wp_enqueue_scripts', 'olivewp_plus_ajax_enqueue_script');

endif;



/*
-------------------------------------------------------------------------------
 Footer Widget Layout
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_footer_layout')) :

	function olivewp_plus_footer_layout($layout) {
		$class = '';
		if( $layout == 1 ) {
			$class = 'spice-col-1';
		}
		elseif( $layout == 2 ) {
			$class = 'spice-col-3';
		}
		elseif( $layout == 3 ) {
			$class = 'spice-col-4';
		}
		elseif( $layout == 4 ) {
			$class = 'spice-col-5';
		}
		elseif( $layout == 5 ) {
			echo '<div class="spice-col-5">';
			olivewp_plus_footer_widget_area('footer-sidebar-1');
			echo '</div>';
			echo '<div class="spice-col-5">';
			olivewp_plus_footer_widget_area('footer-sidebar-2');
			echo '</div>';
			echo '<div class="spice-col-3">';
			olivewp_plus_footer_widget_area('footer-sidebar-3');
			echo '</div>';
		}
		elseif( $layout == 6 ) {
			echo '<div class="spice-col-5">';
			olivewp_plus_footer_widget_area('footer-sidebar-1');
			echo '</div>';
			echo '<div class="spice-col-3">';
			olivewp_plus_footer_widget_area('footer-sidebar-2');
			echo '</div>';
			echo '<div class="spice-col-5">';
			olivewp_plus_footer_widget_area('footer-sidebar-3');
			echo '</div>';
		}
		elseif( $layout == 7 ) {
			echo '<div class="spice-col-3">';
			olivewp_plus_footer_widget_area('footer-sidebar-1');
			echo '</div>';
			echo '<div class="spice-col-5">';
			olivewp_plus_footer_widget_area('footer-sidebar-2');
			echo '</div>';
			echo '<div class="spice-col-5">';
			olivewp_plus_footer_widget_area('footer-sidebar-3');
			echo '</div>';
		}
		elseif( $layout == 8 ) {
			echo '<div class="spice-col-2">';
			olivewp_plus_footer_widget_area('footer-sidebar-1');
			echo '</div>';
			echo '<div class="spice-col-4">';
			olivewp_plus_footer_widget_area('footer-sidebar-2');
			echo '</div>';
		}
		elseif( $layout == 9 ) {
			echo '<div class="spice-col-4">';
			olivewp_plus_footer_widget_area('footer-sidebar-1');
			echo '</div>';
			echo '<div class="spice-col-2">';
			olivewp_plus_footer_widget_area('footer-sidebar-2');
			echo '</div>';
		}
		if($layout == 1 || $layout == 2 || $layout == 3 || $layout == 4):
			for($i=1; $i<=$layout; $i++)
			{
				echo '<div class="' . $class . '">';
				olivewp_plus_footer_widget_area('footer-sidebar-'.$i);
				echo '</div>';
			}
		endif;

	}

endif;



/*
-------------------------------------------------------------------------------
 Footer Widget Layout Section
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_footer_widget_layout_section')) {

    function olivewp_plus_footer_widget_layout_section() {
    	// Get Footer widgets
        if (!function_exists('olivewp_plus_footer_widget_area')) {

            function olivewp_plus_footer_widget_area($sidebar_id) {

                if (is_active_sidebar($sidebar_id)) {
                    dynamic_sidebar($sidebar_id);
                } 
                elseif (current_user_can('edit_theme_options')) {

                    global $wp_registered_sidebars;
                    $sidebar_name = '';
                    if (isset($wp_registered_sidebars[$sidebar_id])) {
                        $sidebar_name = $wp_registered_sidebars[$sidebar_id]['name'];
                    }
                    ?>
                    <div class="widget ast-no-widget-row">
                        <h2 class='widget-title'><?php echo esc_html($sidebar_name); ?></h2>

                        <p class='no-widget-text'>
                            <a href='<?php echo esc_url(admin_url('widgets.php')); ?>'>
                                <?php esc_html_e('Click here to assign a widget for this area.', 'olivewp-plus'); ?>
                            </a>
                        </p>
                    </div>
                    <?php
                }
            }

        }
        /* Function for widget sectons */
        include (OLIVEWP_PLUGIN_DIR.'/inc/footer/footer-sidebar-plus.php');        
    }

}



/*
-------------------------------------------------------------------------------
 Footer Section
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_footer_widget_section')) :

	function olivewp_plus_footer_widget_section() {
	    ?>
	    <footer class="site-footer bg-default bg-footer-lite" <?php if (!empty(get_theme_mod('footer_widget_back_image'))): ?> style="background-image: url(<?php echo get_theme_mod('footer_widget_back_image'); ?>);" <?php endif; ?>>
		    	<?php do_action('spice_sticky_footer');?>
		    	<?php $footer_widget_overlay_color = get_theme_mod('footer_widget_image_overlay_color', 'rgba(0, 0, 0, 0.7)'); ?>	
	    	<div class="footer-overlay" <?php if (!empty(get_theme_mod('footer_widget_back_image'))) { if (get_theme_mod('footer_widget_image_overlay_enable', true) == true) { ?> style="background-color:<?php echo $footer_widget_overlay_color; ?>" <?php } } ?> >
	    		<?php
	    		if (get_theme_mod('footer_widget_enable', true) == true) {  
		    		if((current_user_can('edit_theme_options')) || (is_active_sidebar('footer-sidebar-1') || is_active_sidebar('footer-sidebar-2') || is_active_sidebar('footer-sidebar-3') || is_active_sidebar('footer-sidebar-4'))): ?>
						<div class="spice-container">	
							 <?php olivewp_plus_footer_widget_layout_section(); ?>
						</div>
					<?php
					endif;
				} 
				if (get_theme_mod('footer_bar_enable', true) == true): ?>
					<div class="site-info">
						<div class="spice-container">
							<?php 
							$footer_bar_layout = get_theme_mod('footer_bar_layout', 1);
            				switch ($footer_bar_layout) { 
            					case 1: ?>
									<div class="spice-row">
										<div class="spice-col-1 text-center">
											<?php do_action('olivewp_plus_footer_layout_design1'); ?>
										</div>
									</div>
									<?php break;
								case 2:
									do_action('olivewp_plus_footer_layout_design2');
									break;
							}?>
						</div>
					</div>
				<?php endif; do_action('spice_cookies'); ?>
			</div>
		</footer>
	<?php
	}	
	add_action('olivewp_plus_footer_widgets', 'olivewp_plus_footer_widget_section');

endif;



/*
-------------------------------------------------------------------------------
 Footer Bar Layout 1
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_footer_bar_layout1')) :

	function olivewp_plus_footer_bar_layout1() { 
		$foot_section_1 = get_theme_mod('footer_bar_section1','custom_text');
      	switch ( $foot_section_1 )
    	{
            case 'none':
            break;

            case 'footer_menu':
            echo olivewp_plus_footer_bar_menu();
            break;

            case 'custom_text':
            echo get_theme_mod('footer_bar_section1_copyright','<p>'.__( 'Proudly powered by <a href="https://wordpress.org"> WordPress</a> | Theme: OliveWP by <a href="https://olivewp.org" rel="nofollow">olivewp.org</a>', 'olivewp-plus' ).'</p>');
            break;

            case 'widget':
            olivewp_plus_footer_widget_area('footer-bar-1');
            break;
        }   
   	}	
	add_action('olivewp_plus_footer_layout_design1', 'olivewp_plus_footer_bar_layout1');

endif;



/*
-------------------------------------------------------------------------------
 Footer Bar Layout 2
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_footer_bar_layout2')) :

	function olivewp_plus_footer_bar_layout2() { ?> 
		<div class="spice-row">
			<div class="spice-col-3 spice-left">
				<?php do_action('olivewp_plus_footer_layout_design1'); ?>
			</div> 
			<div class="spice-col-3 spice-right">
				<?php
				$foot_section_2 = get_theme_mod('footer_bar_section2','none');
				switch ( $foot_section_2 )
				{
					case 'none':
					break;

					case 'footer_menu':
					echo olivewp_plus_footer_bar_menu();
					break;

					case 'custom_text':
					echo get_theme_mod('footer_bar_section2_copyright','<p>'.__( 'Proudly powered by <a href="https://wordpress.org"> WordPress</a> | Theme: OliveWP by <a href="https://olivewp.org" rel="nofollow">olivewp.org</a>', 'olivewp-plus' ).'</p>');
					break;

					case 'widget':
					olivewp_plus_footer_widget_area('footer-bar-2');
					break;
				}?>
			</div>  
		</div>
	<?php }	
	add_action('olivewp_plus_footer_layout_design2', 'olivewp_plus_footer_bar_layout2');

endif;


/*
-------------------------------------------------------------------------------
 Footer Bar Menu
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_footer_bar_menu')) {

    function olivewp_plus_footer_bar_menu() {
        ob_start();
        if (has_nav_menu('footer_menu')) {
            wp_nav_menu(
                array(
                    'theme_location' 	=> 'footer_menu',
                    'menu_class'		=> 'footer-nav',
                    'items_wrap' 		=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth' 			=> 1
                )
            );
        } 
        else {
            if (is_user_logged_in() && current_user_can('edit_theme_options')) { ?>
                <a href="<?php echo esc_url(admin_url('/nav-menus.php?action=locations')); ?>"><?php esc_html_e('Assign Footer Menu', 'olivewp-plus'); ?></a>
            <?php }
        }
        return ob_get_clean();
    }
}



/*
-------------------------------------------------------------------------------
 Footer Background Image CSS
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_footer_custom_css')) :

	function olivewp_plus_footer_custom_css() {  
		if (get_theme_mod('footer_widget_enable', true) === true) { ?>
			<style type="text/css">
	            .site-footer {
		            background-repeat:<?php echo get_theme_mod('footer_widget_back_image_repeat', 'no-repeat'); ?>;
		            background-position:<?php echo get_theme_mod('footer_widget_back_image_position', 'left top'); ?>;
		            background-size:<?php echo get_theme_mod('footer_widget_back_size', 'cover'); ?>;
		            background-attachment:<?php echo get_theme_mod('footer_widget_back_attachment', 'scroll'); ?>;
	        	}
	        	<?php if (get_theme_mod('footer_bar_border_enable', true) === true) {  ?>
		        	.site-info {
	        			border-top: <?php echo get_theme_mod('footer_bar_border',1);?>px <?php echo get_theme_mod('footer_bar_border_style','solid');?> <?php echo get_theme_mod('footer_bar_border_color','#FF6F61'); ?>
	        		}
	        	<?php } ?>
			</style>
		<?php }
	}
	add_action('wp_head','olivewp_plus_footer_custom_css');

endif;



/*
-------------------------------------------------------------------------------
 Top Bar Header
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_top_bar_header')) :

	function olivewp_plus_top_bar_header() {  
		if( is_active_sidebar('top-header-sidebar-left') || is_active_sidebar('top-header-sidebar-right')) { ?>
			<div class="spice-topbar <?php if((get_theme_mod('header_preset_layout','left')=='six') || (get_theme_mod('header_preset_layout','left')=='seven')):?>layout-2<?php endif;?>">
		   		<div class="spice-container">
					<?php 
					if( is_active_sidebar('top-header-sidebar-left') )  {
						dynamic_sidebar( 'top-header-sidebar-left' ); 
					}
					if( is_active_sidebar('top-header-sidebar-right') ) {
						dynamic_sidebar( 'top-header-sidebar-right' );
					} 
					?>
				</div>
			</div>
		<?php }
	}
	add_action('olivewp_plus_top_bar_header_feature','olivewp_plus_top_bar_header');

endif;



/*
-------------------------------------------------------------------------------
 Menu Widget Area
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_menu_widget_area')) {

    function olivewp_plus_menu_widget_area($sidebar_id) {

        if (is_active_sidebar($sidebar_id)) {
            dynamic_sidebar($sidebar_id);
        } 
        elseif (current_user_can('edit_theme_options')) {

            global $wp_registered_sidebars;
            $sidebar_name = '';
            if (isset($wp_registered_sidebars[$sidebar_id])) {
                $sidebar_name = $wp_registered_sidebars[$sidebar_id]['name'];
            }
            ?>
            <div class="widget ast-no-widget-row">
                <h2 class='widget-title'><?php echo esc_html($sidebar_name); ?></h2>

                <p class='no-widget-text'>
                    <a href='<?php echo esc_url(admin_url('widgets.php')); ?>'>
                        <?php esc_html_e('Click here to assign a widget for this area.', 'olivewp-plus'); ?>
                    </a>
                </p>
            </div>
            <?php
        }
    }

}


/*
-------------------------------------------------------------------------------
 Predefined Default Background
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_predefined_background')) :

	function olivewp_plus_predefined_background() {  
		$predefined_bg_image_url = get_theme_mod('predefined_back_image', 'bg-img1.png');
	    if ($predefined_bg_image_url != '') {
	        echo '<style> body.boxed{ background-image:url("'.OLIVEWP_PLUGIN_URL.'inc/customizer/assets/images/bg-pattern/' . $predefined_bg_image_url . '");} </style>';
	    }
	}
	add_action('wp_head','olivewp_plus_predefined_background', 10, 0);

endif;



/*
-------------------------------------------------------------------------------
 Apply Additional Styling
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_additional_css')) :

	function olivewp_plus_additional_css() {  
		if (get_theme_mod('breadcrumb_banner_enable', true) == false): ?>
        	<style type="text/css">
        		.spice-topbar {
        			background-color: #ff6f61;
        		}
        	</style>
    	<?php endif;
    	if (get_theme_mod('breadcrumb_banner_enable', true) == true): ?>
        	<style type="text/css">
        		.page-title-section, .standard-page-title.page-title-section {
        			padding-top:<?php echo intval(get_theme_mod('breadcrumb_top_padding',190))?>px;
      				padding-right:<?php echo intval(get_theme_mod('breadcrumb_right_padding',0))?>px;
  					padding-bottom:<?php echo intval(get_theme_mod('breadcrumb_bottom_padding',30))?>px;
      				padding-left:<?php echo intval(get_theme_mod('breadcrumb_left_padding',0))?>px;
        		}
        	</style>
    	<?php endif;
	}
	add_action('wp_head','olivewp_plus_additional_css');

endif;

/*
-------------------------------------------------------------------------------
 Apply Breadcrumb Styling
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_bradcrumb_css')) :

	function olivewp_plus_bradcrumb_css() {?>
        	<style type="text/css">
        		<?php if (get_theme_mod('bredcrumb_visibility', 'show') == 'hide_tablet'): ?>
	        		 @media (min-width:692px) and (max-width:1100px){
		        		body .page-title-section  {
		        			display: none;
		        		}
		        		.header-sidebar {background-color: rgba(0,0,0,0.6);}
		        	}
		        <?php endif;?>
		        <?php if (get_theme_mod('bredcrumb_visibility', 'show') == 'hide_mobile'): ?>	        		 
		        	@media (max-width:691px){
		        		body .page-title-section  {
		        			display: none;
		        		}
		        		.header-sidebar {background-color: rgba(0,0,0,0.6);}
		        	}
		        <?php endif;?>
		        <?php if (get_theme_mod('bredcrumb_visibility', 'show') == 'hide_tab_mob'): ?>
		        	@media (max-width:1100px){
		        		body .page-title-section  {
		        			display: none;
		        		}
		        		.header-sidebar {background-color: rgba(0,0,0,0.6);}
		        	}
		        <?php endif;?>
		        <?php if (get_theme_mod('bredcrumb_visibility', 'show') == 'hide_all'): ?>
		        	body .page-title-section  {
		        			display: none;
		        	}
		        	.header-sidebar {background-color: rgba(0,0,0,0.6);}
	        	<?php endif;?>

        		body .page-title-section  {
        			height: <?php echo get_theme_mod('banner_height_desktop', 300);?>px;
        		}
        		@media (min-width:692px) and (max-width:1100px){
        			body .page-title-section  {
        				height: <?php echo get_theme_mod('banner_height_ipad', 350);?>px;
        			}
        		}
        		@media (max-width:691px){
	        		body .page-title-section  {
        				height: <?php echo get_theme_mod('banner_height_mobile', 370);?>px;
        			}
	        	}
        		<?php if(get_theme_mod('bredcrumb_style','text')=='icon' && get_theme_mod('breadcrumb_enable',true)==true ):?>
        		.page-breadcrumb span span.breadcrumb_last:not(.page-breadcrumb span span span.breadcrumb_last),
        		.page-breadcrumb span span a:not(.page-breadcrumb span span span a){
				    font-size: 0;
				}
				.page-breadcrumb span span.breadcrumb_last:not(.page-breadcrumb span span span.breadcrumb_last)::before,
				.page-breadcrumb span span a:not(.page-breadcrumb span span span a)::before{
				  content:"\f015";
				  font-family: 'Font Awesome 5 Free';
				  font-style: normal;
				  font-weight: 600;
				  font-display: block;				  
				  font-size: 1rem;
				}
			<?php endif;?>
			body section.page-title-section {
			    background-attachment: <?php echo get_theme_mod('breadcrumb_back_attachment', 'scroll');?>!important;
			    background-position: <?php echo get_theme_mod('breadcrumb_back_image_position', 'top center');?>!important;
			    background-repeat: <?php echo get_theme_mod('breadcrumb_back_image_repeat', 'no-repeat');?>!important;;
			    background-size: <?php echo get_theme_mod('breadcrumb_back_size', 'cover');?>!important;
			}
			<?php  if(get_theme_mod('breadcrumb_image_enable',true)==false):?>
				body section.page-title-section{
					background-image: none;
				}
			<?php endif;?>
        	</style>
        	<?php
	}
	add_action('wp_head','olivewp_plus_bradcrumb_css');

endif;

function olivewp_plus_meta_breadcrumb_style(){
	if(get_post_meta(get_the_ID(),'olivewp_show_breadcrumb', true )!='olivewp_breadcrumbs_disable'):?>
		<style>
			body .page-breadcrumb > li a, body .page-breadcrumb > li {
                color: <?php echo esc_attr( get_post_meta(get_the_ID(),'link_color',true) ); ?>;
            }
            body .page-breadcrumb > li a:hover {
                color: <?php echo esc_attr( get_post_meta(get_the_ID(),'link_hover_color',true) ); ?>;
            }
	    </style>
			<?php endif;
}
add_action('wp_head','olivewp_plus_meta_breadcrumb_style',11);