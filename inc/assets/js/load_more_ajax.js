jQuery.noConflict($);
/* Ajax functions */
jQuery(document).ready(function($) {
    if(!$('#loadMore').attr('class')) return;
    var btn_class=$('#loadMore').attr('class').split("=");
    if(btn_class[0]=='infinite')
    {
        $('#loadMore').hide();
        //find scroll position
        $(window).scroll(function() {
            //init
            var that = $('#loadMore');
            var page = $('#loadMore').data('page');
            var newPage = page + 1;
            var ajaxurl = $('#loadMore').data('url');
            var ajaxPage_template = btn_class[1];
            //check
            if ($(window).scrollTop() == $(document).height() - $(window).height()) {
                //ajax call
                $.ajax({
                    url: ajaxurl,
                    type: 'post',
                    data: {
                        ajaxPage_template: ajaxPage_template,
                        page: page,
                        action: 'olivewp_plus_ajax_script_load_more'
                    },
                    error: function(response) {
                        console.log(response);
                    },
                    success: function(response) {
                        //check
                        if (response == 0) {
                            //check
                            if ($("#no-more").length == 0) {
                                $('#ajax-content2').append('<div id="no-more" class="spice-col-1 blog-not-found text-center"><h3>You reached the end of the line!</h3><p>No more posts to load.</p></div>');
                            }
                            $('#loadMore').hide();
                        } 
                        else {
                            $('#loadMore').data('page', newPage);
                            $('#ajax-content').append(response); 
                            if(((btn_class[2]=='grid') && (btn_class[3]=='masonry')) || (btn_class[1]=='template-blog-masonry-two-column.php') || (btn_class[1]=='template-blog-masonry-three-column.php') || (btn_class[1]=='template-blog-masonry-four-column.php'))
                                {
                                $('#ajax-content').imagesLoaded().progress( function() {
                                    var grid = document.querySelector('.waterfall');
                                    waterfall(grid);
                                }); 
                                }
                        }
                    }
                });
            }
        });
    }
    else {  
    //onclick
    $("#loadMore").on('click', function(e) {
        //init
        event.preventDefault();
        $("#loadMore").hide();
        var that = $(this);
        var page = $(this).data('page');
        var newPage = page + 1;
        var ajaxurl = that.data('url');
        var ajaxPage_template = btn_class[1];
        //ajax call
        $.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                ajaxPage_template: ajaxPage_template,
                page: page,
                action: 'olivewp_plus_ajax_script_load_more'
            },
            error: function(response) {
                console.log(response);
            },
            success: function(response) {
                //check
                if (response == 0) {
                    $('#ajax-content2').append('<div class="spice-col-1 blog-not-found text-center"><h3>You reached the end of the line!</h3><p>No more posts to load.</p></div>');
                    $('#loadMore').hide();
                } 
                else {
                    that.data('page', newPage);
                    $('#ajax-content').append(response);  
                    if(((btn_class[2]=='grid') && (btn_class[3]=='masonry')) || (btn_class[1]=='template-blog-masonry-two-column.php') || (btn_class[1]=='template-blog-masonry-three-column.php') || (btn_class[1]=='template-blog-masonry-four-column.php'))
                    {
                    $('#ajax-content').imagesLoaded().progress( function() {
                        var grid = document.querySelector('.waterfall');
                        waterfall(grid);
                    }); 
                    }
                    $("#loadMore").show();
                }
            }
        });
    });
}
});