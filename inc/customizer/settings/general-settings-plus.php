<?php
/**
 * General Settings Customizer
 *
 * @package OliveWP Plus
*/
function olivewp_plus_general_settings_customizer ( $wp_customize ) {

    /* ====================
    * Preloader image layouts 
    ==================== */
    if ( class_exists( 'Olivewp_Plus_Customize_Control_Radio_Image' ) ) {
        $wp_customize->add_setting('preloader_style', array(
                'default'   =>  1
            )
        );

        $wp_customize->add_control(new Olivewp_Plus_Customize_Control_Radio_Image($wp_customize, 'preloader_style', 
            array(
                'label'             => esc_html__('Preloader Style', 'olivewp-plus' ),
                'description'       => esc_html__('You can change the color skin of first, second and fifth Preloader styles', 'olivewp-plus' ),
                'active_callback'   => 'olivewp_plus_preloader_callback',
                'priority'          => 2,
                'section'           => 'preloader_section',
                'choices'           => array(
                    1 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/preloader/preloader1.png',
                    ),
                    2 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/preloader/preloader2.png',
                    ),
                    3 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/preloader/preloader3.png',

                    ),
                    4 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/preloader/preloader4.png',
                        
                    ),
                    5 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/preloader/preloader5.png',
                        
                    ),
                    6 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/preloader/preloader6.png'
                        
                    )
                )
            )
        ));
    }



    /* ====================
    * After Menu
    ==================== */
    // Add multiple options with widgets option
    $wp_customize->add_setting('after_menu_multiple_option',
        array(
            'default'           =>  'none',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('after_menu_multiple_option', 
        array(
            'label'     => esc_html__('After Menu','olivewp-plus' ),
            'section'   => 'after_menu_setting_section',
            'setting'   => 'after_menu_multiple_option',
            'type'      => 'select',
            'choices'   =>  
            array(
                'none'              =>  esc_html__('None', 'olivewp-plus' ),
                'menu_btn'          =>  esc_html__('Button', 'olivewp-plus' ),
                'html'              =>  esc_html__('HTML', 'olivewp-plus' ),
                'top_menu_widget'   =>  esc_html__('Widget', 'olivewp-plus')
            )
        )
    );

    // For the Widget option 
    class Olivewp_Plus_After_Menu_Widget_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php _e('To add widgets, Go to Widgets >> After Menu Widget Area','olivewp-plus');?></h3>
        <?php
        }
    }
    $wp_customize->add_setting('after_menu_widget_area_section',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )   
    );
    $wp_customize->add_control( new Olivewp_Plus_After_Menu_Widget_Customize_Control($wp_customize, 'after_menu_widget_area_section', array( 
            'section' => 'after_menu_setting_section',
            'setting' => 'after_menu_widget_area_section'
        )
    ));




    /* ====================
    * Header Preset
    ==================== */
    $wp_customize->add_section('olivewp_plus_header_preset_section',
        array(
            'title'     =>  esc_html__('Header Presets','olivewp-plus' ),
            'panel'     =>  'olivewp_general_settings',
            'priority'  =>  3
        )
    );
    if ( class_exists( 'Olivewp_Plus_Customize_Control_Radio_Image' ) ) {
        $wp_customize->add_setting('header_preset_layout', array(
                'default'   =>  'left'
            )
        );

        $wp_customize->add_control(new Olivewp_Plus_Customize_Control_Radio_Image($wp_customize, 'header_preset_layout', 
            array(
                'label'             => esc_html__('Header layout with logo placing', 'olivewp-plus' ),
                'section'           => 'olivewp_plus_header_preset_section',
                'choices'           => array(
                    'left'  => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/header-preset/container-right.png',
                    ),
                    'right' => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/header-preset/container-left.png',
                    ),
                    'center' => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/header-preset/center.png',

                    ),
                    'full' => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/header-preset/full-left.png',
                        
                    ),
                    'five' => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/header-preset/5.png',
                        
                    ),
                    'six' => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/header-preset/6.png',
                        
                    ),
                    'seven' => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/header-preset/7.png',
                        
                    )
                )
            )
        ));
    }


     /* ====================
    * Search 
    ==================== */
    $wp_customize->add_section('olivewp_plus_search_effects',
        array(
            'title'     =>  esc_html__('Search Effects','olivewp-plus' ),
            'panel'     =>  'olivewp_general_settings',
            'priority'  =>  5
        )
    );

    // Enable/Disable search effects
    $wp_customize->add_setting('search_btn_enable',
        array(
            'default'           => false,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'search_btn_enable',
        array(
            'label'             =>  esc_html__( 'Enable/Disable Search Icon', 'olivewp-plus'),
            'section'           =>  'olivewp_plus_search_effects',
            'type'              =>  'toggle',
            'priority'          =>  1
        )
    ));

    // Search effects
    $wp_customize->add_setting('search_effect_style',
        array(
            'default'           =>  'toggle',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('search_effect_style', 
        array(
            'label'             =>  esc_html__('Choose Search Effect','olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_search_effects_callback',
            'section'           =>  'olivewp_plus_search_effects',
            'setting'           =>  'search_effect_style',
            'type'              =>  'radio',
            'priority'          =>  2,
            'choices'           =>  array(
                'toggle'        =>  esc_html__('Toggle', 'olivewp-plus' ),
                'popup_light'   =>  esc_html__('Pop up light', 'olivewp-plus' ),
                'popup_dark'    =>  esc_html__('Pop up dark', 'olivewp-plus')
            )
        )
    );


    /* ====================
    * Breadcrumbs 
    ==================== */
    $wp_customize->add_section('olivewp_plus_breadcrumb',
        array(
            'title'     =>  esc_html__('Breadcrumb','olivewp-plus' ),
            'panel'     =>  'olivewp_general_settings',
            'priority'  =>  6
        )
    );

    // Enable/Disable breadcrumbs section
    $wp_customize->add_setting('breadcrumb_banner_enable',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'breadcrumb_banner_enable',
        array(
            'label'             =>  esc_html__( 'Enable/Disable Banner', 'olivewp-plus'),
            'section'           =>  'olivewp_plus_breadcrumb',
            'type'              =>  'toggle',
            'priority'          =>  1
        )
    ));

    /* Visibility */

    $wp_customize->add_setting('bredcrumb_visibility',
        array(
            'default'           =>  'show',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );

    $wp_customize->add_control('bredcrumb_visibility', 
        array(
            'label'     => esc_html__('Visibility','olivewp-plus' ),
            'section'   => 'olivewp_plus_breadcrumb',
            'setting'   => 'bredcrumb_visibility',
            'active_callback'   => 'olivewp_plus_breadcrumb_section_callback',
            'priority'  => 1,
            'type'      => 'select',
            'choices'   =>  
            array(
                'show'          =>  esc_html__('Show On All Devices', 'olivewp-plus' ),
                'hide_tablet'   =>  esc_html__('Hide On Tablet', 'olivewp-plus' ),
                'hide_mobile'   =>  esc_html__('Hide On Mobile', 'olivewp-plus' ),
                'hide_tab_mob'  =>  esc_html__('Hide On Tablet & Mobile', 'olivewp-plus' ),
                'hide_all'      =>  esc_html__('Hide On All Devices', 'olivewp-plus' ),
            )
        )
    );

    $wp_customize->add_setting('banner_height_option',
        array(
            'default'           =>  'desktop',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('banner_height_option', 
        array(
            'label'     => esc_html__('Banner Height','olivewp-plus' ),
            'section'   => 'olivewp_plus_breadcrumb',
            'setting'   => 'banner_height_option',
            'active_callback'   => 'olivewp_plus_breadcrumb_section_callback',
            'priority'  => 1,
            'type'      => 'select',
            'choices'   =>  
            array(
                'desktop'      =>  esc_html__('Desktop', 'olivewp-plus' ),
                'ipad'  =>  esc_html__('Ipad', 'olivewp-plus' ),
                'mobile'      =>  esc_html__('Mobile', 'olivewp-plus' )
            )
        )
    );

    $wp_customize->add_setting( 'banner_height_desktop',
        array(
            'default'           => 300,
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'banner_height_desktop',
        array(
            'section'           =>  'olivewp_plus_breadcrumb',
            'setting'           =>  'banner_height_desktop',
            'active_callback'   =>  'olivewp_plus_breadcrumb_section_callback',
            'priority'          =>  1,
            'input_attrs'   => 
                array(
                    'min'   =>  0,
                    'max'   =>  800,
                    'step'  =>  1
                )
        )
    ));

    $wp_customize->add_setting( 'banner_height_ipad',
        array(
            'default'           => 350,
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'banner_height_ipad',
        array(
            'section'           =>  'olivewp_plus_breadcrumb',
            'setting'           =>  'banner_height_ipad',
            'active_callback'   =>  'olivewp_plus_breadcrumb_section_callback',
            'priority'          =>  1,
            'input_attrs'   => 
                array(
                    'min'   =>  0,
                    'max'   =>  800,
                    'step'  =>  1
                )
        )
    ));

    $wp_customize->add_setting( 'banner_height_mobile',
        array(
            'default'           => 370,
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'banner_height_mobile',
        array(
            'section'           =>  'olivewp_plus_breadcrumb',
            'setting'           =>  'banner_height_mobile',
            'active_callback'   =>  'olivewp_plus_breadcrumb_section_callback',
            'priority'          =>  1,
            'input_attrs'   => 
                array(
                    'min'   =>  0,
                    'max'   =>  800,
                    'step'  =>  1
                )
        )
    ));

    // Breadcrumb padding 
    class Olivewp_Plus_Breadcusrmb_Padding_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php _e('Padding','olivewp-plus');?></h3>
        <?php
        }
    }
    $wp_customize->add_setting('breadcrumb_section_padding',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )   
    );
    $wp_customize->add_control( new Olivewp_Plus_Breadcusrmb_Padding_Customize_Control($wp_customize, 'breadcrumb_section_padding', array( 
            'section'   => 'olivewp_plus_breadcrumb',
            'setting'   => 'breadcrumb_section_padding',
            'active_callback'   =>  'olivewp_plus_breadcrumb_section_callback',
            'priority'  =>  2
        )
    ));
    /* Breadcrumb top padding */
    $wp_customize->add_setting( 'breadcrumb_top_padding',
        array(
            'default'           => 190,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'breadcrumb_top_padding',
        array(
            'label'             => esc_html__( 'Top', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_breadcrumb_section_callback',
            'section'           => 'olivewp_plus_breadcrumb',
            'type'              => 'number',
            'priority'          => 2,
            'input_attrs'       => 
                array( 
                    'min' => 0, 
                    'max' => 500, 
                    'step' => 1
                )
        )
    );
    /* Breadcrumb right padding */
    $wp_customize->add_setting( 'breadcrumb_right_padding',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'breadcrumb_right_padding',
        array(
            'label'             => esc_html__( 'Right', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_breadcrumb_section_callback',
            'section'           => 'olivewp_plus_breadcrumb',
            'type'              => 'number',
            'priority'          => 2,
            'input_attrs'       => 
                array( 
                    'min' => 0, 
                    'max' => 500, 
                    'step' => 1
                )
        )
    );
    /* Breadcrumb bottom padding */
    $wp_customize->add_setting( 'breadcrumb_bottom_padding',
        array(
            'default'           => 30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'breadcrumb_bottom_padding',
        array(
            'label'             => esc_html__( 'Bottom', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_breadcrumb_section_callback', 
            'section'           => 'olivewp_plus_breadcrumb',
            'type'              => 'number',
            'priority'          => 2,
            'input_attrs'       => 
                array( 
                    'min' => 0, 
                    'max' => 500, 
                    'step' => 1
                )
        )
    );
    /* Breadcrumb left padding */
    $wp_customize->add_setting( 'breadcrumb_left_padding',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'breadcrumb_left_padding',
        array(
            'label'             => esc_html__( 'Left', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_breadcrumb_section_callback',
            'section'           => 'olivewp_plus_breadcrumb',
            'type'              => 'number',
            'priority'          => 2,
            'input_attrs'       => 
                array( 
                    'min' => 0, 
                    'max' => 500, 
                    'step' => 1
                )
        )
    );

    /* == Heading for the Page title == */
    class Olivewp_breadcrumbs_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3 style="background:#e5e5e5;padding:10px 12px;margin:0"><?php esc_html_e('Breadcrumbs', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('bredcrumb_setting',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_breadcrumbs_Customize_Control($wp_customize, 'bredcrumb_setting', 
        array(
                'section'           =>  'olivewp_plus_breadcrumb',
                'setting'           =>  'bredcrumb_setting',
                'active_callback'   => 'olivewp_plus_breadcrumb_section_callback',
                'priority'  => 2,
            )
    ));


    // Enable/Disable breadcrumbs section
    $wp_customize->add_setting('breadcrumb_enable',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'breadcrumb_enable',
        array(
            'label'             =>  esc_html__( 'Enable/Disable Breadcrumbs', 'olivewp-plus'),
            'active_callback'   => 'olivewp_plus_breadcrumb_section_callback',
            'section'           =>  'olivewp_plus_breadcrumb',
            'type'              =>  'toggle',
            'priority'          =>  2
        )
    ));

     /* Breadcrumb Style */

    $wp_customize->add_setting('bredcrumb_style',
        array(
            'default'           =>  'text',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );

    $wp_customize->add_control('bredcrumb_style', 
        array(
            'label'     => esc_html__('Home item','olivewp-plus' ),
            'section'   => 'olivewp_plus_breadcrumb',
            'setting'   => 'bredcrumb_style',
            'active_callback'   => 'olivewp_plus_breadcrumb_section_callback',
            'priority'  => 2,
            'type'      => 'select',
            'choices'   =>  
            array(
                'text'      =>  esc_html__('Text', 'olivewp-plus' ),
                'icon'      =>  esc_html__('Icon', 'olivewp-plus' ),
            )
        )
    );


     // Enable/Disable breadcrumbs section
    $wp_customize->add_setting('breadcrumb_image_enable',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'breadcrumb_image_enable',
        array(
            'label'             =>  esc_html__( 'Enable/Disable Background Image', 'olivewp-plus'),
            'section'           =>  'olivewp_plus_breadcrumb',
            'active_callback'   => 'olivewp_plus_breadcrumb_section_callback',
            'type'              =>  'toggle',
            'priority'          =>  2
        )
    ));

    // breadcrumb background image repeat
    $wp_customize->add_setting('breadcrumb_back_image_repeat',
        array(
            'default'           =>  'no-repeat',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('breadcrumb_back_image_repeat', 
        array(
            'label'             =>  esc_html__('Background Image Repeat','olivewp-plus' ),
            'section'           =>  'olivewp_plus_breadcrumb',
            'setting'           =>  'breadcrumb_back_image_repeat',
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_breadcrumb_section_callback($control) &&
                                            olivewp_plus_breadcrumb_callback($control)
                                        );
                                    },
            'priority'          =>  2,
            'type'              =>  'select',
            'choices'           =>  array(
                'no-repeat'     =>  esc_html__('No-Repeat', 'olivewp-plus' ),
                'repeat'        =>  esc_html__('Repeat All', 'olivewp-plus'  ),
                'repeat-x'      =>  esc_html__('Repeat Horizontally', 'olivewp-plus' ),
                'repeat-y'      =>  esc_html__('Repeat Vertically', 'olivewp-plus' )
            )
        )
    );

    // breadcrumb background image position
    $wp_customize->add_setting('breadcrumb_back_image_position',
        array(
            'default'           =>  'top center',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('breadcrumb_back_image_position', 
        array(
            'label'             =>  esc_html__('Background Image Position','olivewp-plus' ),
            'section'           =>  'olivewp_plus_breadcrumb',
            'setting'           =>  'breadcrumb_back_image_position',
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_breadcrumb_section_callback($control) &&
                                            olivewp_plus_breadcrumb_callback($control)
                                        );
                                    },
            'priority'          =>  2,
            'type'              =>  'select',
            'choices'           =>  array(
                'left top'          =>  esc_html__('Left Top', 'olivewp-plus' ),
                'left center'       =>  esc_html__('Left Center', 'olivewp-plus' ),
                'left bottom'       =>  esc_html__('Left Bottom', 'olivewp-plus' ),
                'right top'         =>  esc_html__('Right Top', 'olivewp-plus' ),
                'right center'      =>  esc_html__('Right Center', 'olivewp-plus' ),
                'right bottom'      =>  esc_html__('Right Bottom', 'olivewp-plus' ),
                'top center'        =>  esc_html__('Center Top', 'olivewp-plus' ),
                'center center'     =>  esc_html__('Center Center', 'olivewp-plus' ),
                'center bottom'     =>  esc_html__('Center Bottom', 'olivewp-plus' ),
            )
        )
    );

    // breadcrumb background size
    $wp_customize->add_setting('breadcrumb_back_size',
        array(
            'default'           =>  'cover',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('breadcrumb_back_size', 
        array(
            'label'             =>  esc_html__('Background Size','olivewp-plus' ),
            'section'           =>  'olivewp_plus_breadcrumb',
            'setting'           =>  'breadcrumb_back_size',
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_breadcrumb_section_callback($control) &&
                                            olivewp_plus_breadcrumb_callback($control)
                                        );
                                    },
            'priority'          =>  2,
            'type'              =>  'select',
            'choices'           =>  array(
                'cover'         =>  esc_html__('Cover', 'olivewp-plus' ),
                'contain'       =>  esc_html__('Contain', 'olivewp-plus' ),
                'auto'          =>  esc_html__('Auto', 'olivewp-plus' )
            )
        )
    );

    // breadcrumb background attachment
    $wp_customize->add_setting('breadcrumb_back_attachment',
        array(
            'default'           =>  'scroll',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('breadcrumb_back_attachment', 
        array(
            'label'             =>  esc_html__('Background Attachment','olivewp-plus' ),
            'description'       =>  esc_html__('Note: Background Image Repeat and Background Image Position will not work with Background Attachment Fixed property', 'olivewp-plus'),
            'section'           =>  'olivewp_plus_breadcrumb',
            'setting'           =>  'breadcrumb_back_attachment',
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_breadcrumb_section_callback($control) &&
                                            olivewp_plus_breadcrumb_callback($control)
                                        );
                                    },
            'priority'          =>  2,
            'type'              =>  'select',
            'choices'           =>  array(
                'scroll'        =>  esc_html__('Scroll', 'olivewp-plus' ),
                'fixed'         =>  esc_html__('Fixed', 'olivewp-plus' )            )
        )
    );


    // Enable/Disable breadcrumbs overlay
    $wp_customize->add_setting('breadcrumb_overlay_enable',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'breadcrumb_overlay_enable',
        array(
            'label'             =>  esc_html__( 'Enable/Disable Banner Image Overlay', 'olivewp-plus'),
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_breadcrumb_section_callback($control) &&
                                            olivewp_plus_breadcrumb_callback($control)
                                        );
                                    },
            'section'           =>  'olivewp_plus_breadcrumb',
            'type'              =>  'toggle',
            'priority'          =>  3
        )
    ));

    // Breadcrumb overlay color
    $wp_customize->add_setting( 'breadcrumb_overlay_color', 
        array(
            'sanitize_callback' => 'sanitize_text_field',
            'default'           => 'rgba(0,0,0,0.6)'
        ) 
    );      
    $wp_customize->add_control(new Olivewp_Plus_Customize_Alpha_Color_Control( $wp_customize,'breadcrumb_overlay_color', 
        array(
            'label'             =>  esc_html__('Image Overlay Color','olivewp-plus' ),
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_breadcrumb_section_callback($control) &&
                                            olivewp_plus_breadcrumb_overlay_callback($control) &&
                                            olivewp_plus_breadcrumb_callback($control)
                                        );
                                    },
            'palette'           =>  true,
            'section'           =>  'olivewp_plus_breadcrumb',
            'priority'          =>  4
        )
    ));

    // 404 page breadcrumb background image
    $wp_customize->add_setting( 'error_breadcrumb_back_img', 
        array(
            'sanitize_callback' => 'esc_url_raw'
        ) 
    );      
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'error_breadcrumb_back_img', 
        array(
            'label'             =>  esc_html__( '404 Page', 'olivewp-plus' ),
            'section'           =>  'olivewp_plus_breadcrumb',
            'setting'           =>  'error_breadcrumb_back_img',
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_breadcrumb_section_callback($control) &&
                                            olivewp_plus_breadcrumb_callback($control)
                                        );
                                    },
            'priority'          =>  10
        ) 
    ));

    // Search page breadcrumb background image
    $wp_customize->add_setting( 'search_breadcrumb_back_img', 
        array(
            'sanitize_callback' => 'esc_url_raw'
        ) 
    );      
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'search_breadcrumb_back_img', 
        array(
            'label'             =>  esc_html__( 'Search Page', 'olivewp-plus' ),
            'section'           =>  'olivewp_plus_breadcrumb',
            'setting'           =>  'search_breadcrumb_back_img',
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_breadcrumb_section_callback($control) &&
                                            olivewp_plus_breadcrumb_callback($control)
                                        );
                                    },
            'priority'          =>  11
        ) 
    ));

    // Date Archive breadcrumb background image
    $wp_customize->add_setting( 'date_archive_breadcrumb_back_img', 
        array(
            'sanitize_callback' => 'esc_url_raw'
        ) 
    );      
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'date_archive_breadcrumb_back_img', 
        array(
            'label'             =>  esc_html__( 'Date Archive', 'olivewp-plus' ),
            'section'           =>  'olivewp_plus_breadcrumb',
            'setting'           =>  'date_archive_breadcrumb_back_img',
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_breadcrumb_section_callback($control) &&
                                            olivewp_plus_breadcrumb_callback($control)
                                        );
                                    },
            'priority'          =>  12
        ) 
    ));

    // Author Archive breadcrumb background image
    $wp_customize->add_setting( 'author_archive_breadcrumb_back_img', 
        array(
            'sanitize_callback' => 'esc_url_raw'
        ) 
    );      
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'author_archive_breadcrumb_back_img', 
        array(
            'label'             =>  esc_html__( 'Author Archive', 'olivewp-plus' ),
            'section'           =>  'olivewp_plus_breadcrumb',
            'setting'           =>  'author_archive_breadcrumb_back_img',
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_breadcrumb_section_callback($control) &&
                                            olivewp_plus_breadcrumb_callback($control)
                                        );
                                    },
            'priority'          =>  13
        ) 
    ));

    // Shop breadcrumb background image
    $wp_customize->add_setting( 'author_shop_breadcrumb_back_img', 
        array(
            'sanitize_callback' => 'esc_url_raw'
        ) 
    );      
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'author_shop_breadcrumb_back_img', 
        array(
            'label'             =>  esc_html__( 'Shop Page', 'olivewp-plus' ),
            'section'           =>  'olivewp_plus_breadcrumb',
            'setting'           =>  'author_shop_breadcrumb_back_img',
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_breadcrumb_section_callback($control) &&
                                            olivewp_plus_breadcrumb_callback($control)
                                        );
                                    },
            'priority'          =>  14
        ) 
    ));




    /* ====================
    * Container Width 
    ==================== */
    // container width for the page
    $wp_customize->add_setting('page_layout_width',
        array(
            'default'           =>  'default',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('page_layout_width', 
        array(
            'label'             =>  esc_html__('Page Layout','olivewp-plus' ),
            'section'           =>  'container_width_section',
            'setting'           =>  'page_layout_width',
            'type'              =>  'select',
            'priority'          =>  4,
            'choices'           =>  array(
                'default'               =>  esc_html__('Default', 'olivewp-plus' ),
                'full_width_fluid'      =>  esc_html__('Full Width / Container Fluid', 'olivewp-plus' ),
                'full_width_streatched'   =>  esc_html__('Full Width / Streatched', 'olivewp-plus')
            )
        )
    );

    // container width for the blog pages
    $wp_customize->add_setting('blog_page_layout_width',
        array(
            'default'           =>  'default',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('blog_page_layout_width', 
        array(
            'label'             =>  esc_html__('Blog Layout','olivewp-plus' ),
            'section'           =>  'container_width_section',
            'setting'           =>  'blog_page_layout_width',
            'type'              =>  'select',
            'priority'          =>  5,
            'choices'           =>  array(
                'default'               =>  esc_html__('Default', 'olivewp-plus' ),
                'full_width_fluid'      =>  esc_html__('Full Width / Container Fluid', 'olivewp-plus' ),
                'full_width_streatched'   =>  esc_html__('Full Width / Streatched', 'olivewp-plus')
            )
        )
    );

    // container width for the single post page
    $wp_customize->add_setting('single_post_layout_width',
        array(
            'default'           =>  'default',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('single_post_layout_width', 
        array(
            'label'             =>  esc_html__('Single Post Layout','olivewp-plus' ),
            'section'           =>  'container_width_section',
            'setting'           =>  'single_post_layout_width',
            'type'              =>  'select',
            'priority'          =>  6,
            'choices'           =>  array(
                'default'                   =>  esc_html__('Default', 'olivewp-plus' ),
                'full_width_fluid'          =>  esc_html__('Full Width / Container Fluid', 'olivewp-plus' ),
                'full_width_streatched'     =>  esc_html__('Full Width / Streatched', 'olivewp-plus')
            )
        )
    );




    /* ====================
    * Post Navigation 
    ==================== */
    $wp_customize->add_section('olivewp_plus_post_navigation',
        array(
            'title'     =>  esc_html__('Post Navigation','olivewp-plus' ),
            'panel'     =>  'olivewp_general_settings',
            'priority'  =>  8
        )
    );
    // Post navigation style
    $wp_customize->add_setting('post_navigation_style',
        array(
            'default'           =>  'pagination',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('post_navigation_style', 
        array(
            'label'             =>  esc_html__('Choose Style','olivewp-plus' ),
            'section'           =>  'olivewp_plus_post_navigation',
            'setting'           =>  'post_navigation_style',
            'type'              =>  'radio',
            'choices'           =>  array(
                'pagination'    =>  esc_html__('Pagination', 'olivewp-plus' ),
                'load_more'     =>  esc_html__('Load More', 'olivewp-plus' ),
                'infinite'      =>  esc_html__('Infinite Scroll', 'olivewp-plus')
            )
        )
    );




    /* ====================
    * Scroll to Top
    ==================== */
    // Scroll to top icon position
    $wp_customize->add_setting('scroll_to_top_position',
        array(
            'default'           =>  'right',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('scroll_to_top_position', 
        array(
            'label'             =>  esc_html__('Choose Position','olivewp-plus' ),
            'section'           =>  'scrolltotop_setting_section',
            'setting'           =>  'scroll_to_top_position',
            'active_callback'   =>  'olivewp_plus_scroll_to_top_callback',
            'priority'          =>  2,
            'type'              =>  'radio',
            'choices'           =>  array(
                'left'      =>  esc_html__('Left', 'olivewp-plus' ),
                'right'     =>  esc_html__('Right', 'olivewp-plus' )
            )
        )
    );

    // scroll to top icon font
    $wp_customize->add_setting('scroll_to_top_icon_class',
        array(
            'default'           => esc_html__('fa fa-arrow-up', 'olivewp-plus'),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('scroll_to_top_icon_class',
        array(
            'label'             => esc_html__('Icon Class Name', 'olivewp-plus'),
            'section'           => 'scrolltotop_setting_section',
            'setting'           => 'scroll_to_top_icon_class',
            'active_callback'   => 'olivewp_plus_scroll_to_top_callback',
            'priority'          => 3,
            'type'              => 'text'
        )
    );

    // scroll to top button radious
    $wp_customize->add_setting( 'scroll_to_top_button_radious',
        array(
            'default'           => 3,
            'transport'         => 'postMessage',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'scroll_to_top_button_radious',
        array(
            'label'             =>  esc_html__('Border Radius', 'olivewp-plus'),
            'section'           =>  'scrolltotop_setting_section',
            'setting'           =>  'scroll_to_top_button_radious',
            'active_callback'   =>  'olivewp_plus_scroll_to_top_callback',
            'priority'          =>  4,
            'input_attrs'   => 
                array(
                    'min'   =>  0,
                    'max'   =>  30,
                    'step'  =>  1
                )
        )
    ));

    // Color setting apply on scroll to top
    $wp_customize->add_setting( 'scroll_to_top_color_enable',
        array(
            'default'           => false,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'scroll_to_top_color_enable',
        array(
            'label'             =>  esc_html__( 'Enable to apply the color settings', 'olivewp-plus'  ),
            'section'           =>  'scrolltotop_setting_section',
            'setting'           =>  'scroll_to_top_color_enable',
            'active_callback'   =>  'olivewp_plus_scroll_to_top_callback',
            'type'              =>  'toggle',
            'priority'          =>  5
        )
    ));

    // Background color for the scroll to top
    $wp_customize->add_setting('scroll_to_top_back_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'scroll_to_top_back_color', 
        array(
            'label'             =>  esc_html__('Background Color', 'olivewp-plus' ),
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_scroll_to_top_callback($control) &&
                                            olivewp_plus_scroll_to_top_color_callback($control)
                                        );
            },
            'section'           =>  'scrolltotop_setting_section',
            'setting'           =>  'scroll_to_top_back_color',
            'priority'          =>  6
        )
    ));

    // Icon color for the scroll to top
    $wp_customize->add_setting('scroll_to_top_icon_color', 
        array(
            'default'           => '#fff',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'scroll_to_top_icon_color', 
        array(
            'label'             =>  esc_html__('Icon Color', 'olivewp-plus' ),
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_scroll_to_top_callback($control) &&
                                            olivewp_plus_scroll_to_top_color_callback($control)
                                        );
            },
            'section'           =>  'scrolltotop_setting_section',
            'setting'          =>  'scroll_to_top_icon_color',
            'priority'          =>  7
        )
    ));

    // Background hover color for the scroll to top
    $wp_customize->add_setting('scroll_to_top_back_hover_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'scroll_to_top_back_hover_color', 
        array(
            'label'             =>  esc_html__('Background Hover Color', 'olivewp-plus' ),
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_scroll_to_top_callback($control) &&
                                            olivewp_plus_scroll_to_top_color_callback($control)
                                        );
            },
            'section'           =>  'scrolltotop_setting_section',
            'setting'          =>  'scroll_to_top_back_hover_color',
            'priority'          =>  8
        )
    ));

    // Icon hover color for the scroll to top
    $wp_customize->add_setting('scroll_to_top_icon_hover_color', 
        array(
            'default'           => '#fff',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'scroll_to_top_icon_hover_color', 
        array(
            'label'             =>  esc_html__('Icon Hover Color', 'olivewp-plus' ),
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_scroll_to_top_callback($control) &&
                                            olivewp_plus_scroll_to_top_color_callback($control)
                                        );
            },
            'section'           =>  'scrolltotop_setting_section',
            'setting'           =>  'scroll_to_top_icon_hover_color',
            'priority'          =>  9
        )
    ));




    /* ====================
    * Footer Widgets
    ==================== */
    $wp_customize->add_section('olivewp_plus_footer_widget_section',
        array(
            'title'     =>  esc_html__('Footer Widgets','olivewp-plus' ),
            'panel'     =>  'olivewp_general_settings',
            'priority'  =>  10
        )
    );

    // Enable / Disable footer widget option
    $wp_customize->add_setting( 'footer_widget_enable',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'footer_widget_enable',
        array(
            'label'     =>  esc_html__( 'Enable/Disable Footer Widgets', 'olivewp-plus'  ),
            'setting'   =>  'footer_widget_enable',
            'section'   =>  'olivewp_plus_footer_widget_section',
            'type'      =>  'toggle',
            'priority'  =>  1
        )
    ));

    // Footer widgets layouts
    if ( class_exists( 'Olivewp_Plus_Customize_Control_Radio_Image' ) ) {
        $wp_customize->add_setting('footer_widget_layout', array(
                'default'   =>  4
            )
        );

        $wp_customize->add_control(new Olivewp_Plus_Customize_Control_Radio_Image($wp_customize, 'footer_widget_layout', 
            array(
                'label'             =>  esc_html__('Widget Layout', 'olivewp-plus' ),
                'active_callback'   =>  'olivewp_plus_footer_widget_callback',
                'setting'           =>  'footer_widget_layout',
                'section'           =>  'olivewp_plus_footer_widget_section',
                'priority'          =>  2,
                'choices'           =>  array(
                    1 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-widgets/1.png',
                    ),
                    2 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-widgets/2.png',
                    ),
                    3 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-widgets/3.png',

                    ),
                    4 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-widgets/4.png',
                        
                    ),
                    5 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-widgets/3-3-6.png',

                    ),
                    6 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-widgets/3-6-3.png',

                    ),
                    7 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-widgets/6-3-3.png',

                    ),
                    8 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-widgets/8-4.png',

                    ),
                    9 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-widgets/4-8.png',

                    ),
                )
            )
        ));
    }

    // Footer widget background image
    $wp_customize->add_setting( 'footer_widget_back_image', 
        array(
            'sanitize_callback' => 'esc_url_raw'
        ) 
    );      
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_widget_back_image', 
        array(
            'label'             =>  esc_html__( 'Widgets Background Image', 'olivewp-plus' ),
            'section'           =>  'olivewp_plus_footer_widget_section',
            'setting'           =>  'footer_widget_back_image',
            'active_callback'   =>  'olivewp_plus_footer_widget_callback',
            'priority'          =>  3
        ) 
    ));

    // Footer widget background image repeat
    $wp_customize->add_setting('footer_widget_back_image_repeat',
        array(
            'default'           =>  'no-repeat',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('footer_widget_back_image_repeat', 
        array(
            'label'             =>  esc_html__('Background Image Repeat','olivewp-plus' ),
            'section'           =>  'olivewp_plus_footer_widget_section',
            'setting'           =>  'footer_widget_back_image_repeat',
            'active_callback'   =>  'olivewp_plus_footer_widget_callback',
            'priority'          =>  4,
            'type'              =>  'select',
            'choices'           =>  array(
                'no-repeat'     =>  esc_html__('No-Repeat', 'olivewp-plus' ),
                'repeat'        =>  esc_html__('Repeat All', 'olivewp-plus' ),
                'repeat-x'      =>  esc_html__('Repeat Horizontally', 'olivewp-plus' ),
                'repeat-y'      =>  esc_html__('Repeat Vertically', 'olivewp-plus' )
            )
        )
    );

    // Footer widget background image position
    $wp_customize->add_setting('footer_widget_back_image_position',
        array(
            'default'           =>  'left top',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('footer_widget_back_image_position', 
        array(
            'label'             =>  esc_html__('Background Image Position','olivewp-plus' ),
            'section'           =>  'olivewp_plus_footer_widget_section',
            'setting'           =>  'footer_widget_back_image_position',
            'active_callback'   =>  'olivewp_plus_footer_widget_callback',
            'priority'          =>  5,
            'type'              =>  'select',
            'choices'           =>  array(
                'left top'          =>  esc_html__('Left Top', 'olivewp-plus' ),
                'left center'       =>  esc_html__('Left Center', 'olivewp-plus' ),
                'left bottom'       =>  esc_html__('Left Bottom', 'olivewp-plus' ),
                'right top'         =>  esc_html__('Right Top', 'olivewp-plus' ),
                'right center'      =>  esc_html__('Right Center', 'olivewp-plus' ),
                'right bottom'      =>  esc_html__('Right Bottom', 'olivewp-plus' ),
                'center top'        =>  esc_html__('Center Top', 'olivewp-plus' ),
                'center center'     =>  esc_html__('Center Center', 'olivewp-plus' ),
                'center bottom'     =>  esc_html__('Center Bottom', 'olivewp-plus' ),
            )
        )
    );

    // Footer widget background size
    $wp_customize->add_setting('footer_widget_back_size',
        array(
            'default'           =>  'cover',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('footer_widget_back_size', 
        array(
            'label'             =>  esc_html__('Background Size','olivewp-plus' ),
            'section'           =>  'olivewp_plus_footer_widget_section',
            'setting'           =>  'footer_widget_back_size',
            'active_callback'   =>  'olivewp_plus_footer_widget_callback',
            'priority'          =>  6,
            'type'              =>  'select',
            'choices'           =>  array(
                'cover'         =>  esc_html__('Cover', 'olivewp-plus' ),
                'contain'       =>  esc_html__('Contain', 'olivewp-plus' ),
                'auto'          =>  esc_html__('Auto', 'olivewp-plus' )
            )
        )
    );

    // Footer widget background attachment
    $wp_customize->add_setting('footer_widget_back_attachment',
        array(
            'default'           =>  'scroll',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('footer_widget_back_attachment', 
        array(
            'label'             =>  esc_html__('Background Attachment','olivewp-plus' ),
            'description'       =>  esc_html__('Note: Background Image Repeat and Background Image Position will not work with Background Attachment Fixed property', 'olivewp-plus'),
            'section'           =>  'olivewp_plus_footer_widget_section',
            'setting'           =>  'footer_widget_back_attachment',
            'active_callback'   =>  'olivewp_plus_footer_widget_callback',
            'priority'          =>  7,
            'type'              =>  'select',
            'choices'           =>  array(
                'scroll'        =>  esc_html__('Scroll', 'olivewp-plus' ),
                'fixed'         =>  esc_html__('Fixed', 'olivewp-plus' )            )
        )
    );

    // Enable / Disable footer widget image overlay color
    $wp_customize->add_setting( 'footer_widget_image_overlay_enable',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'footer_widget_image_overlay_enable',
        array(
            'label'             =>  esc_html__( 'Enable/Disable Widgets Image Overlay', 'olivewp-plus'  ),
            'setting'           =>  'footer_widget_image_overlay_enable',
            'section'           =>  'olivewp_plus_footer_widget_section',
            'active_callback'   =>  'olivewp_plus_footer_widget_callback',
            'type'              =>  'toggle',
            'priority'          =>  8
        )
    ));

    // Footer widget image overlay color
    $wp_customize->add_setting( 'footer_widget_image_overlay_color', 
        array(
            'sanitize_callback' => 'sanitize_text_field',
            'default'           => 'rgba(0, 0, 0, 0.7)'
        ) 
    );      
    $wp_customize->add_control(new Olivewp_Plus_Customize_Alpha_Color_Control( $wp_customize,'footer_widget_image_overlay_color', 
        array(
            'label'             =>  esc_html__('Widgets Image Overlay Color','olivewp-plus' ),
            'active_callback'   =>  function($control) {
                                        return (
                                            olivewp_plus_footer_widget_callback($control) &&
                                            olivewp_plus_footer_widget_overlay_color_callback($control)
                                        );
                                    },
            'palette'           =>  true,
            'setting'           =>  'footer_widget_image_overlay_color',
            'section'           =>  'olivewp_plus_footer_widget_section',
            'priority'          =>  9
        )
    ));




    /* ====================
    * Footer Bar
    ==================== */
    $wp_customize->add_section('olivewp_plus_footer_bar_section',
        array(
            'title'     =>  esc_html__('Footer Bar','olivewp-plus' ),
            'panel'     =>  'olivewp_general_settings',
            'priority'  =>  11
        )
    );

    // Enable / Disable footer bar
    $wp_customize->add_setting( 'footer_bar_enable',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'footer_bar_enable',
        array(
            'label'     =>  esc_html__( 'Enable/Disable Footer Bar', 'olivewp-plus'  ),
            'setting'   =>  'footer_bar_enable',
            'section'   =>  'olivewp_plus_footer_bar_section',
            'type'      =>  'toggle',
            'priority'  =>  1
        )
    ));

    // Footer bar layouts
    if ( class_exists( 'Olivewp_Plus_Customize_Control_Radio_Image' ) ) {
        $wp_customize->add_setting('footer_bar_layout', array(
                'default'   =>  1
            )
        );

        $wp_customize->add_control(new Olivewp_Plus_Customize_Control_Radio_Image($wp_customize, 'footer_bar_layout', 
            array(
                'label'             =>  esc_html__('Footer Bar layout', 'olivewp-plus' ),
                'active_callback'   =>  'olivewp_plus_footer_bar_callback',
                'setting'           =>  'footer_bar_layout',
                'section'           =>  'olivewp_plus_footer_bar_section',
                'priority'          =>  2,
                'choices'           =>  array(
                    1 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-bar/footer-layout-1.png',
                    ),
                    2 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-bar/footer-layout-2.png',
                    )
                )
            )
        ));
    }

    // Footer bar section 1
    $wp_customize->add_setting('footer_bar_section1',
        array(
            'default'           =>  'custom_text',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('footer_bar_section1', 
        array(
            'label'             =>  esc_html__('Section 1','olivewp-plus' ),
            'section'           =>  'olivewp_plus_footer_bar_section',
            'setting'           =>  'footer_bar_section1',
            'active_callback'   =>  'olivewp_plus_footer_bar_callback',
            'priority'          =>  3,
            'type'              =>  'select',
            'choices'           =>  array(
                'none'          =>  esc_html__('None', 'olivewp-plus' ),
                'footer_menu'   =>  esc_html__('Footer Menu', 'olivewp-plus' ),
                'custom_text'   =>  esc_html__('Copyright Text', 'olivewp-plus' ),
                'widget'        =>  esc_html__('Widget', 'olivewp-plus' )          
            )
        )
    );

    // Footer bar section 1 copyright textbox
    $wp_customize->add_setting('footer_bar_section1_copyright', 
        array(
            'default'           => '<p>'.__( 'Proudly powered by <a href="https://wordpress.org"> WordPress</a> | Theme: OliveWP by <a href="https://olivewp.org" rel="nofollow">olivewp.org</a>', 'olivewp-plus' ).'</p>',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('footer_bar_section1_copyright', 
        array(
            'label'             =>  esc_html__('Copyright Section 1','olivewp-plus' ),
            'section'           =>  'olivewp_plus_footer_bar_section',
            'setting'           =>  'footer_bar_section1_copyright',
            'type'              =>  'textarea',
            'active_callback'   =>  'olivewp_plus_footer_bar_callback',
            'priority'          =>  4
        )
    );

    // Footer bar section 2
    $wp_customize->add_setting('footer_bar_section2',
        array(
            'default'           =>  'none',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('footer_bar_section2', 
        array(
            'label'             =>  esc_html__('Section 2','olivewp-plus' ),
            'section'           =>  'olivewp_plus_footer_bar_section',
            'setting'           =>  'footer_bar_section2',
            'active_callback'   =>  function($control) {
                                        return (
                                                olivewp_plus_footer_bar_callback($control) &&
                                                olivewp_plus_footer_bar_section_callback($control)
                                                );
            },
            'priority'          =>  5,
            'type'              =>  'select',
            'choices'           =>  array(
                'none'          =>  esc_html__('None', 'olivewp-plus' ),
                'footer_menu'   =>  esc_html__('Footer Menu', 'olivewp-plus' ),
                'custom_text'   =>  esc_html__('Copyright Text', 'olivewp-plus' ),
                'widget'        =>  esc_html__('Widget', 'olivewp-plus' )          
            )
        )
    );

    // Footer bar section 2 copyright textbox
    $wp_customize->add_setting('footer_bar_section2_copyright', 
        array(
            'default'           => '<p>'.__( 'Proudly powered by <a href="https://wordpress.org"> WordPress</a> | Theme: OliveWP by <a href="https://olivewp.org" rel="nofollow">olivewp.org</a>', 'olivewp-plus' ).'</p>',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('footer_bar_section2_copyright', 
        array(
            'label'             =>  esc_html__('Copyright Section 2','olivewp-plus' ),
            'section'           =>  'olivewp_plus_footer_bar_section',
            'setting'           =>  'footer_bar_section2_copyright',
            'type'              =>  'textarea',
            'active_callback'   =>  'olivewp_plus_footer_bar_callback',
            'priority'          =>  6
        )
    );


    // Enable Disable Footer bar border color */
    $wp_customize->add_setting( 'footer_bar_border_enable',
        array(
            'default'           => false,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'footer_bar_border_enable',
        array(
            'label'             => esc_html__( 'Enable to apply the settings', 'olivewp-plus'  ),
            'section'           => 'olivewp_plus_footer_bar_section',
            'active_callback'   => 'olivewp_plus_footer_bar_callback',
            'type'              => 'toggle',
            'priority'          => 7
        )
    ));
    // Footer bar border color
    $wp_customize->add_setting('footer_bar_border_color', 
        array(
            'default'           => '#FF6F61',
            'sanitize_callback' => 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bar_border_color', 
        array(
            'label'             =>  esc_html__('Border Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_plus_footer_bar_section',
            'setting'          =>  'footer_bar_border_color',
            'priority'          =>  8,
            'active_callback'   =>  function($control) {
                                        return (
                                                olivewp_plus_footer_bar_callback($control) &&
                                                olivewp_plus_footer_bar_border_callback($control)
                                                );
            }
        )
    ));

    // Footer Bar Border
    $wp_customize->add_setting('footer_bar_border',
        array(
            'default'       =>  1,
            'capability'    =>  'edit_theme_options'
        )
    );

    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'footer_bar_border',
        array(
            'label'             =>  esc_html__( 'Border Radius', 'olivewp-plus' ),
            'active_callback'   =>  function($control) {
                                        return (
                                                olivewp_plus_footer_bar_callback($control) &&
                                                olivewp_plus_footer_bar_border_callback($control)
                                                );
            },
            'section'           =>  'olivewp_plus_footer_bar_section',
            'setting'           =>  'footer_bar_border',
            'priority'          =>  9,
            'input_attrs'   => array(
                'min'   => 1,
                'max'   => 50,
                'step'  => 1,
            ),
        )
    ));


    // Footer bar border style
    $wp_customize->add_setting('footer_bar_border_style',
        array(
            'default'           =>  'solid',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('footer_bar_border_style', 
        array(
            'label'             =>  esc_html__('Border Style','olivewp-plus' ),
            'active_callback'   =>  function($control) {
                                        return (
                                                olivewp_plus_footer_bar_callback($control) &&
                                                olivewp_plus_footer_bar_border_callback($control)
                                                );
            },
            'section'           =>  'olivewp_plus_footer_bar_section',
            'setting'           =>  'footer_bar_border_style',
            'type'              =>  'select',
            'priority'          =>  10,
            'choices'           =>  array(
                'solid'         =>  esc_html__('Solid', 'olivewp-plus'),
                'dotted'        =>  esc_html__('Dotted', 'olivewp-plus'),
                'dashed'        =>  esc_html__('Dashed', 'olivewp-plus'),
                'double'        =>  esc_html__('Double', 'olivewp-plus'),
                'groove'        =>  esc_html__('Groove', 'olivewp-plus'),
                'ridge'         =>  esc_html__('Ridge', 'olivewp-plus'),
                'inset'         =>  esc_html__('Inset', 'olivewp-plus'),
                'outset'        =>  esc_html__('Outset', 'olivewp-plus')
            )
        )
    );


}

add_action( 'customize_register', 'olivewp_plus_general_settings_customizer' );