<?php
/**
 * Blog Options Customizer
 *
 * @package OliveWP Plus
*/

function olivewp_plus_single_blog_customizer ( $wp_customize ) {

    /* ====================
    * Related post  
    ==================== */
    // Enable/Disable related post
    $wp_customize->add_setting('olivewp_plus_enable_related_post',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control($wp_customize, 'olivewp_plus_enable_related_post',
        array(
            'label'     => esc_html__('Enable/Disable Related Posts', 'olivewp-plus' ),
            'type'      => 'toggle',
            'section'   => 'olivewp_single_blog_section',
            'setting'   => 'olivewp_plus_enable_related_post',
            'priority'  => 1
        )
    ));

    // Related post title 
    $wp_customize->add_setting('olivewp_plus_related_post_title', 
        array(
            'default'           => esc_html__('Related Posts','olivewp-plus' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_related_post_title',
        array(
            'label'             => esc_html__('Related Posts Title','olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_related_post_callback',
            'section'           => 'olivewp_single_blog_section',
            'setting'           => 'olivewp_plus_related_post_title',
            'type'              => 'text',
            'priority'          => 2
        )
    );

    // Related post option
    $wp_customize->add_setting('olivewp_plus_related_post_option',
        array(
            'default'           =>  esc_html__('categories','olivewp-plus'),
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('olivewp_plus_related_post_option', 
        array(
            'label'             => esc_html__('Related Posts Option','olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_related_post_callback',
            'section'           => 'olivewp_single_blog_section',
            'setting'           => 'olivewp_plus_related_post_option',
            'type'              => 'select',
            'priority'          => 3,
            'choices'           =>  
            array(
                'categories'    =>  esc_html__('All', 'olivewp-plus' ),
                'tags'          =>  esc_html__('Related by tags', 'olivewp-plus' )
            )
        )
    );



    // Padding
    // Enable/Disable Padding single post
    $wp_customize->add_setting('olivewp_plus_enable_padding_single_post',
        array(
            'default'           => false,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control($wp_customize, 'olivewp_plus_enable_padding_single_post',
        array(
            'label'     => esc_html__('Enable/Disable Padding', 'olivewp-plus' ),
            'type'      => 'toggle',
            'section'   => 'olivewp_single_blog_section',
            'setting'   => 'olivewp_plus_enable_padding_single_post',
            'priority'  => 9
        )
    ));
    class Olivewp_Plus_Single_Post_Padding_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Padding', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('single_blog_padding_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Single_Post_Padding_Customize_Control($wp_customize, 'single_blog_padding_title', 
        array(
            'section'           =>  'olivewp_single_blog_section',
            'active_callback' => 'olivewp_plus_padding_single_post_callback',
            'setting'           =>  'single_blog_padding_title',
             'priority'  => 9
        )
    ));
    // Padding Top
    $wp_customize->add_setting( 'olivewp_plus_single_blog_padding_top',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_single_blog_padding_top',
        array(
            'label'         => esc_html__( 'Top', 'olivewp-plus'  ),
            'section'       => 'olivewp_single_blog_section',
            'active_callback' => 'olivewp_plus_padding_single_post_callback',
            'type'          => 'number',
            'priority'      => 10,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

     // Padding Right
    $wp_customize->add_setting( 'olivewp_plus_single_blog_padding_right',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_single_blog_padding_right',
        array(
            'label'         => esc_html__( 'Right', 'olivewp-plus'  ),
            'section'       => 'olivewp_single_blog_section',
            'active_callback' => 'olivewp_plus_padding_single_post_callback',
            'type'          => 'number',
            'priority'      => 11,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    // Padding Bottom
    $wp_customize->add_setting( 'olivewp_plus_single_blog_padding_bottom',
        array(
            'default'           => 8,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_single_blog_padding_bottom',
        array(
            'label'         => esc_html__( 'Bottom', 'olivewp-plus'  ),
            'section'       => 'olivewp_single_blog_section',
            'active_callback' => 'olivewp_plus_padding_single_post_callback',
            'type'          => 'number',
            'priority'      => 12,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

   // Padding Left
    $wp_customize->add_setting( 'olivewp_plus_single_blog_padding_left',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_single_blog_padding_left',
        array(
            'label'         => esc_html__( 'Left', 'olivewp-plus'  ),
            'section'       => 'olivewp_single_blog_section',
            'active_callback' => 'olivewp_plus_padding_single_post_callback',
            'type'          => 'number',
            'priority'      => 13,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    // Margin
    // Enable/Disable Margin single post
    $wp_customize->add_setting('olivewp_plus_enable_margin_single_post',
        array(
            'default'           => false,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control($wp_customize, 'olivewp_plus_enable_margin_single_post',
        array(
            'label'     => esc_html__('Enable/Disable Margin', 'olivewp-plus' ),
            'type'      => 'toggle',
            'section'   => 'olivewp_single_blog_section',
            'setting'   => 'olivewp_plus_enable_margin_single_post',
            'priority'  => 14
        )
    ));
    class Olivewp_Plus_Single_Blog_Margin_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Margin', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('single_blog_margin_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Single_Blog_Margin_Customize_Control($wp_customize, 'single_blog_margin_title', 
        array(
            'section'           =>  'olivewp_single_blog_section',
            'active_callback' => 'olivewp_plus_margin_single_post_callback',
            'setting'           =>  'single_blog_margin_title',
             'priority'  => 14
        )
    ));
    // Margin Top
    $wp_customize->add_setting( 'olivewp_plus_single_blog_margin_top',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_single_blog_margin_top',
        array(
            'label'         => esc_html__( 'Top', 'olivewp-plus'  ),
            'section'       => 'olivewp_single_blog_section',
           'active_callback' => 'olivewp_plus_margin_single_post_callback',
            'type'          => 'number',
            'priority'      => 15,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

     // Margin Right
    $wp_customize->add_setting( 'olivewp_plus_single_blog_margin_right',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_single_blog_margin_right',
        array(
            'label'         => esc_html__( 'Right', 'olivewp-plus'  ),
            'section'       => 'olivewp_single_blog_section',
            'active_callback' => 'olivewp_plus_margin_single_post_callback',
            'type'          => 'number',
            'priority'      => 16,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    // Margin Bottom
    $wp_customize->add_setting( 'olivewp_plus_single_blog_margin_bottom',
        array(
            'default'           => 30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_single_blog_margin_bottom',
        array(
            'label'         => esc_html__( 'Bottom', 'olivewp-plus'  ),
            'section'       => 'olivewp_single_blog_section',
            'active_callback' => 'olivewp_plus_margin_single_post_callback',
            'type'          => 'number',
            'priority'      => 17,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

   // Margin Left
    $wp_customize->add_setting( 'olivewp_plus_single_blog_margin_left',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_single_blog_margin_left',
        array(
            'label'         => esc_html__( 'Left', 'olivewp-plus'  ),
            'section'       => 'olivewp_single_blog_section',
           'active_callback' => 'olivewp_plus_margin_single_post_callback',
            'type'          => 'number',
            'priority'      => 18,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );



    // Border
    class Olivewp_Plus_Single_Blog_Border_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Border Width', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('single_blog_border_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Single_Blog_Border_Customize_Control($wp_customize, 'single_blog_border_title', 
        array(
            'section'           =>  'olivewp_single_blog_section',
            //'active_callback'   =>  'olivewp_plus_topbar_widget_typography_callback',
            'setting'           =>  'single_blog_border_title',
             'priority'  => 19
        )
    ));
    // Border Top
    $wp_customize->add_setting( 'olivewp_plus_single_blog_border_top',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_single_blog_border_top',
        array(
            'label'         => esc_html__( 'Top', 'olivewp-plus'  ),
            'section'       => 'olivewp_single_blog_section',
            'type'          => 'number',
            'priority'      => 20,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

     // Border Right
    $wp_customize->add_setting( 'olivewp_plus_single_blog_border_right',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_single_blog_border_right',
        array(
            'label'         => esc_html__( 'Right', 'olivewp-plus'  ),
            'section'       => 'olivewp_single_blog_section',
            'type'          => 'number',
            'priority'      => 21,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    // Border Bottom
    $wp_customize->add_setting( 'olivewp_plus_single_blog_border_bottom',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_single_blog_border_bottom',
        array(
            'label'         => esc_html__( 'Bottom', 'olivewp-plus'  ),
            'section'       => 'olivewp_single_blog_section',
            'type'          => 'number',
            'priority'      => 22,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

   // Border Left
    $wp_customize->add_setting( 'olivewp_plus_single_blog_border_left',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_single_blog_border_left',
        array(
            'label'         => esc_html__( 'Left', 'olivewp-plus'  ),
            'section'       => 'olivewp_single_blog_section',
            'type'          => 'number',
            'priority'      => 23,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

    // Single Blog Border Color
     $wp_customize->add_setting('olivewp_plus_single_blog_border_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'olivewp_plus_single_blog_border_color', 
        array(
            'label'             =>  esc_html__('Border Color', 'olivewp-plus' ),
            //'active_callback'   =>  'olivewp_plus_after_menu_button_callback',
            'section'           =>  'olivewp_single_blog_section',
            'setting'           =>  'olivewp_plus_single_blog_border_color',
            'priority'  => 24
        )
    ));

}
add_action( 'customize_register', 'olivewp_plus_single_blog_customizer' );