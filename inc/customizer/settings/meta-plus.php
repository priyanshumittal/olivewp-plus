<?php
/**
 * Blog Options Customizer
 *
 * @package OliveWP Plus
*/

function olivewp_plus_meta_customizer ( $wp_customize ) {


    $wp_customize->add_setting('olivewp_enable_meta_padding',
        array(
            'default'           => false,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control($wp_customize, 'olivewp_enable_meta_padding',
        array(
            'label'     => esc_html__('Enable/Disable Padding', 'olivewp' ),
            'type'      => 'toggle',
            'section'   => 'olivewp_meta_section',
            'priority'  => 2
        )
    ));
    // Padding
    class Olivewp_Plus_Meta_Padding_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Padding', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('meta_padding_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Meta_Padding_Customize_Control($wp_customize, 'meta_padding_title', 
        array(
            'section'           =>  'olivewp_meta_section',
            'active_callback' => 'olivewp_plus_meta_padding_callback',
            'setting'           =>  'meta_padding_title',
             'priority'  => 2
        )
    ));
    // Padding Top
    $wp_customize->add_setting( 'olivewp_plus_meta_padding_top',
        array(
            'default'           => 8,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_meta_padding_top',
        array(
            'label'         => esc_html__( 'Top', 'olivewp-plus'  ),
            'section'       => 'olivewp_meta_section',
            'active_callback' => 'olivewp_plus_meta_padding_callback',
            'type'          => 'number',
            'priority'      => 3,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

     // Padding Right
    $wp_customize->add_setting( 'olivewp_plus_meta_padding_right',
        array(
            'default'           => 30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_meta_padding_right',
        array(
            'label'         => esc_html__( 'Right', 'olivewp-plus'  ),
            'section'       => 'olivewp_meta_section',
            'active_callback' => 'olivewp_plus_meta_padding_callback',
            'type'          => 'number',
            'priority'      => 4,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    // Padding Bottom
    $wp_customize->add_setting( 'olivewp_plus_meta_padding_bottom',
        array(
            'default'           => 8,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_meta_padding_bottom',
        array(
            'label'         => esc_html__( 'Bottom', 'olivewp-plus'  ),
            'section'       => 'olivewp_meta_section',
            'active_callback' => 'olivewp_plus_meta_padding_callback',
            'type'          => 'number',
            'priority'      => 5,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

   // Padding Left
    $wp_customize->add_setting( 'olivewp_plus_meta_padding_left',
        array(
            'default'           => 30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_meta_padding_left',
        array(
            'label'         => esc_html__( 'Left', 'olivewp-plus'  ),
            'section'       => 'olivewp_meta_section',
            'active_callback' => 'olivewp_plus_meta_padding_callback',
            'type'          => 'number',
            'priority'      => 6,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    $wp_customize->add_setting('olivewp_enable_meta_margin',
        array(
            'default'           => false,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control($wp_customize, 'olivewp_enable_meta_margin',
        array(
            'label'     => esc_html__('Enable/Disable Margin', 'olivewp' ),
            'type'      => 'toggle',
            'section'   => 'olivewp_meta_section',
            'priority'  => 7
        )
    ));
    // Margin
    class Olivewp_Plus_Meta_Margin_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Margin', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('meta_margin_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Meta_Margin_Customize_Control($wp_customize, 'meta_margin_title', 
        array(
            'section'           =>  'olivewp_meta_section',
            'active_callback' => 'olivewp_plus_meta_margin_callback',
            'setting'           =>  'meta_margin_title',
             'priority'  => 7
        )
    ));
    // Margin Top
    $wp_customize->add_setting( 'olivewp_plus_meta_margin_top',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_meta_margin_top',
        array(
            'label'         => esc_html__( 'Top', 'olivewp-plus'  ),
            'section'       => 'olivewp_meta_section',
            'active_callback' => 'olivewp_plus_meta_margin_callback',
            'type'          => 'number',
            'priority'      => 8,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

     // Border Right
    $wp_customize->add_setting( 'olivewp_plus_meta_margin_right',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_meta_margin_right',
        array(
            'label'         => esc_html__( 'Right', 'olivewp-plus'  ),
            'section'       => 'olivewp_meta_section',
            'active_callback' => 'olivewp_plus_meta_margin_callback',
            'type'          => 'number',
            'priority'      => 9,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    // Border Bottom
    $wp_customize->add_setting( 'olivewp_plus_meta_margin_bottom',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_meta_margin_bottom',
        array(
            'label'         => esc_html__( 'Bottom', 'olivewp-plus'  ),
            'section'       => 'olivewp_meta_section',
            'active_callback' => 'olivewp_plus_meta_margin_callback',
            'type'          => 'number',
            'priority'      => 10,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

   // Border Left
    $wp_customize->add_setting( 'olivewp_plus_meta_margin_left',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_meta_margin_left',
        array(
            'label'         => esc_html__( 'Left', 'olivewp-plus'  ),
            'section'       => 'olivewp_meta_section',
            'active_callback' => 'olivewp_plus_meta_margin_callback',
            'type'          => 'number',
            'priority'      => 11,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    $wp_customize->add_setting('olivewp_enable_meta_border',
        array(
            'default'           => false,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control($wp_customize, 'olivewp_enable_meta_border',
        array(
            'label'     => esc_html__('Enable/Disable Border', 'olivewp' ),
            'type'      => 'toggle',
            'section'   => 'olivewp_meta_section',
            'priority'  => 12
        )
    ));

    // Border
    class Olivewp_Plus_Meta_Border_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Border Width', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('meta_border_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Meta_Border_Customize_Control($wp_customize, 'meta_border_title', 
        array(
            'section'           =>  'olivewp_meta_section',
            'active_callback'   =>  'olivewp_plus_meta_border_callback',
            'setting'           =>  'meta_border_title',
             'priority'  => 12
        )
    ));
    // Border Top
    $wp_customize->add_setting( 'olivewp_plus_meta_border_top',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_meta_border_top',
        array(
            'label'         => esc_html__( 'Top', 'olivewp-plus'  ),
            'section'       => 'olivewp_meta_section',
            'active_callback'   =>  'olivewp_plus_meta_border_callback',
            'type'          => 'number',
            'priority'      => 13,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

     // Border Right
    $wp_customize->add_setting( 'olivewp_plus_meta_border_right',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_meta_border_right',
        array(
            'label'         => esc_html__( 'Right', 'olivewp-plus'  ),
            'section'       => 'olivewp_meta_section',
            'active_callback'   =>  'olivewp_plus_meta_border_callback',
            'type'          => 'number',
            'priority'      => 14,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    // Border Bottom
    $wp_customize->add_setting( 'olivewp_plus_meta_border_bottom',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_meta_border_bottom',
        array(
            'label'         => esc_html__( 'Bottom', 'olivewp-plus'  ),
            'section'       => 'olivewp_meta_section',
            'active_callback'   =>  'olivewp_plus_meta_border_callback',
            'type'          => 'number',
            'priority'      => 15,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

   // Border Left
    $wp_customize->add_setting( 'olivewp_plus_meta_border_left',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_meta_border_left',
        array(
            'label'         => esc_html__( 'Left', 'olivewp-plus'  ),
            'section'       => 'olivewp_meta_section',
            'active_callback'   =>  'olivewp_plus_meta_border_callback',
            'type'          => 'number',
            'priority'      => 16,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

    // Single Blog Border Color
     $wp_customize->add_setting('olivewp_plus_meta_border_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'olivewp_plus_meta_border_color', 
        array(
            'label'             =>  esc_html__('Border Color', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_meta_border_callback',
            'section'           =>  'olivewp_meta_section',
            'setting'           =>  'olivewp_plus_meta_border_color',
            'priority'  => 17
        )
    ));

}
add_action( 'customize_register', 'olivewp_plus_meta_customizer' );