<?php
/**
 * Color & Background Customizer
 *
 * @package OliveWP Plus
*/

function olivewp_plus_color_back_customizer($wp_customize) {

    $selective_refresh = isset($wp_customize->selective_refresh) ? 'postMessage' : 'refresh';


    /* ====================
    * Topbar Widget 
    ==================== */
    $wp_customize->add_section('olivewp_topbar_widget', 
        array(
            'title'     =>  esc_html__('Topbar Widgets', 'olivewp-plus' ),
            'panel'     =>  'colors_back_settings',
            'priority'  =>  3
        )
    );
    // Enable/Disable the topbar widget color setting
    $wp_customize->add_setting('enable_topbar_color',
        array(
            'default'           =>  false,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_topbar_color',
        array(
            'label'             =>  esc_html__( 'Enable to apply the settings', 'olivewp-plus'  ),
            'section'           =>  'olivewp_topbar_widget',
            'setting'           =>  'enable_topbar_color',
            'priority'          =>  1,
            'type'              =>  'toggle'
        )
    ));
    // setting for the topbar widget title color
    $wp_customize->add_setting('topbar_widget_title_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'topbar_widget_title_color', 
        array(
            'label'             =>  esc_html__('Title Color', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_topbar_widget_color_callback',
            'section'           =>  'olivewp_topbar_widget',
            'setting'           =>  'topbar_widget_title_color'
        )
    ));
    // setting for the topbar widget text color
    $wp_customize->add_setting('topbar_widget_text_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'topbar_widget_text_color', 
        array(
            'label'             =>  esc_html__('Text Color', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_topbar_widget_color_callback',
            'section'           =>  'olivewp_topbar_widget',
            'setting'           =>  'topbar_widget_text_color'
        )
    ));
    // setting for the topbar widget link color
    $wp_customize->add_setting('topbar_widget_link_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'topbar_widget_link_color', 
        array(
            'label'             =>  esc_html__('Link Color', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_topbar_widget_color_callback',
            'section'           =>  'olivewp_topbar_widget',
            'setting'           =>  'topbar_widget_link_color'
        )
    ));
    // setting for the topbar widget link color
    $wp_customize->add_setting('topbar_widget_link_hover_color', 
        array(
            'default'           => '##ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'topbar_widget_link_hover_color', 
        array(
            'label'             =>  esc_html__('Link Hover Color', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_topbar_widget_color_callback',
            'section'           =>  'olivewp_topbar_widget',
            'setting'           =>  'topbar_widget_link_hover_color'
        )
    ));



    /* ====================
    * Header Background Color 
    ==================== */
    $wp_customize->add_setting('enable_header_back_color',
        array(
            'default'           =>  false,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_checkbox'
        )
    );
    // Enable/Disable the Header Background color setting
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_header_back_color',
        array(
            'label'             =>  esc_html__( 'Enable to apply the settings', 'olivewp-plus'  ),
            'section'           =>  'olivewp_header_color',
            'setting'           =>  'enable_header_back_color',
            'priority'          =>  1,
            'type'              =>  'toggle'
        )
    ));
    // setting for the header background color
    $wp_customize->add_setting('header_back_color', 
        array(
            'default'           => 'rgba(0, 0, 0, 0.0)',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Customize_Alpha_Color_Control($wp_customize, 'header_back_color', 
        array(
            'label'             =>  esc_html__('Background Color', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_header_back_color_callback',
            'section'           =>  'olivewp_header_color',
            'setting'          =>  'header_back_color',
            'priority'          =>  2
        )
    ));



    /* ====================
    * After Menu Button
    ==================== */
    $wp_customize->add_section('olivewp_after_menu', 
        array(
            'title'     =>  esc_html__('After Menu Button', 'olivewp-plus' ),
            'panel'     =>  'colors_back_settings',
            'priority'  =>  6
        )
    );
    // Enable/Disable the After menu button setting
    $wp_customize->add_setting('enable_after_menu',
        array(
            'default'           =>  false,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_after_menu',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'olivewp-plus'  ),
            'section'   =>  'olivewp_after_menu',
            'setting'   =>  'enable_after_menu',
            'type'      =>  'toggle'
        )
    ));
    // setting for the after menu background color
    $wp_customize->add_setting('after_menu_back_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'after_menu_back_color', 
        array(
            'label'             =>  esc_html__('Background Color', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_after_menu_button_callback',
            'section'           =>  'olivewp_after_menu',
            'setting'           =>  'after_menu_back_color'
        )
    ));
    // setting for the after menu text color
    $wp_customize->add_setting('after_menu_text_color', 
        array(
            'default'           => '#fff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'after_menu_text_color', 
        array(
            'label'             =>  esc_html__('Text Color', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_after_menu_button_callback',
            'section'           =>  'olivewp_after_menu',
            'setting'           =>  'after_menu_text_color'
        )
    ));
    // setting for the after menu button hover color
    $wp_customize->add_setting('after_menu_button_hover_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'after_menu_button_hover_color', 
        array(
            'label'             =>  esc_html__('Button Hover Color', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_after_menu_button_callback',
            'section'           =>  'olivewp_after_menu',
            'setting'           =>  'after_menu_button_hover_color'
        )
    ));




    /* ====================
    * Banner
    ==================== */
    $wp_customize->add_section('olivewp_breadcrumb_banner', 
        array(
            'title'     =>  esc_html__('Banner', 'olivewp-plus' ),
            'panel'     =>  'colors_back_settings',
            'priority'  =>  7
        )
    );
    // Enable/Disable the breadcrumbs title setting
    $wp_customize->add_setting('enable_banner_color',
        array(
            'default'           =>  false,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_banner_color',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'olivewp-plus'  ),
            'section'   =>  'olivewp_breadcrumb_banner',
            'setting'   =>  'enable_banner_color',
            'type'      =>  'toggle'
        )
    ));
    // Heading for the banner title
    class Olivewp_Plus_Banner_Title_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Banner Title', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('breadcrumb_banner_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Banner_Title_Customize_Control($wp_customize, 'breadcrumb_banner_title', 
        array(
            'section'           =>  'olivewp_breadcrumb_banner',
            'setting'           =>  'breadcrumb_banner_title',
            'active_callback'   =>  'olivewp_plus_banner_bredcrumb_callback'
        )
    ));
    // setting for the banner title color
    $wp_customize->add_setting('breadcrumb_banner_title_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'breadcrumb_banner_title_color', 
        array(
            'label'             =>  esc_html__('Title Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_breadcrumb_banner',
            'setting'           =>  'breadcrumb_banner_title_color',
            'active_callback'   =>  'olivewp_plus_banner_bredcrumb_callback'
        )
    ));
    // Heading for the banner title
    class Olivewp_Plus_Breadcrumb_Title_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Breadcrumb Title', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('breadcrumb_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Breadcrumb_Title_Customize_Control($wp_customize, 'breadcrumb_title', 
        array(
            'section'           =>  'olivewp_breadcrumb_banner',
            'setting'           =>  'breadcrumb_title',
            'active_callback'   =>  'olivewp_plus_banner_bredcrumb_callback'
        )
    ));
    // setting for the breadcrumbs link color
    $wp_customize->add_setting('breadcrumb_breadcrumb_link_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'breadcrumb_breadcrumb_link_color', 
        array(
            'label'             =>  esc_html__('Text/Link Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_breadcrumb_banner',
            'setting'           =>  'breadcrumb_breadcrumb_link_color',
            'active_callback'   =>  'olivewp_plus_banner_bredcrumb_callback'
        )
    ));
    // setting for the breadcrumbs link hover color
    $wp_customize->add_setting('breadcrumb_breadcrumb_link_hover_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'breadcrumb_breadcrumb_link_hover_color', 
        array(
            'label'             =>  esc_html__('Link Hover Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_breadcrumb_banner',
            'setting'           =>  'breadcrumb_breadcrumb_link_hover_color',
            'active_callback'   =>  'olivewp_plus_banner_bredcrumb_callback'
        )
    ));




    /* ====================
    * Blog Page/Archive
    ==================== */
    $wp_customize->add_section('olivewp_blog_page_archive', 
        array(
            'title'     =>  esc_html__('Blog Page/Archive', 'olivewp-plus' ),
            'panel'     =>  'colors_back_settings',
            'priority'  =>  9
        )
    );
    // Enable/Disable the blog page/archive setting
    $wp_customize->add_setting('enable_blog_page_archive',
        array(
            'default'           =>  false,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_blog_page_archive',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'olivewp-plus'  ),
            'section'   =>  'olivewp_blog_page_archive',
            'setting'   =>  'enable_blog_page_archive',
            'type'      =>  'toggle'
        )
    ));
    // setting for the blog page/archive title color
    $wp_customize->add_setting('blog_page_title_color', 
        array(
            'default'           => '#000000',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'blog_page_title_color', 
        array(
            'label'             =>  esc_html__('Title Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_blog_page_archive',
            'setting'           =>  'blog_page_title_color',
            'active_callback'   =>  'olivewp_plus_blog_archive_page_callback'
        )
    ));
    // setting for the blog page/archive title hover color
    $wp_customize->add_setting('blog_page_title_hover_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'blog_page_title_hover_color', 
        array(
            'label'             =>  esc_html__('Title Hover Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_blog_page_archive',
            'setting'           =>  'blog_page_title_hover_color',
            'active_callback'   =>  'olivewp_plus_blog_archive_page_callback'
        )
    ));
    // setting for the blog page/archive meta link color
    $wp_customize->add_setting('blog_page_meta_link_color', 
        array(
            'default'           => '#858585',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'blog_page_meta_link_color', 
        array(
            'label'             =>  esc_html__('Meta Link Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_blog_page_archive',
            'setting'           =>  'blog_page_meta_link_color',
            'active_callback'   =>  'olivewp_plus_blog_archive_page_callback'
        )
    ));
    // setting for the blog page/archive meta link hover color
    $wp_customize->add_setting('blog_page_meta_link_hover_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'blog_page_meta_link_hover_color', 
        array(
            'label'             =>  esc_html__('Meta Link Hover Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_blog_page_archive',
            'setting'           =>  'blog_page_meta_link_hover_color',
            'active_callback'   =>  'olivewp_plus_blog_archive_page_callback'
        )
    ));





    /* ====================
    * Single Post
    ==================== */
    $wp_customize->add_section('olivewp_single_post', 
        array(
            'title'     =>  esc_html__('Single Post', 'olivewp-plus' ),
            'panel'     =>  'colors_back_settings',
            'priority'  =>  10
        )
    );
    // Enable/Disable the blog page/archive setting
    $wp_customize->add_setting('enable_single_post',
        array(
            'default'           =>  false,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_single_post',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'olivewp-plus'  ),
            'section'   =>  'olivewp_single_post',
            'setting'   =>  'enable_single_post',
            'type'      =>  'toggle'
        )
    ));
    // setting for the single post title color
    $wp_customize->add_setting('single_post_title_color', 
        array(
            'default'           => '#000000',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'single_post_title_color', 
        array(
            'label'             =>  esc_html__('Title Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_single_post',
            'setting'           =>  'single_post_title_color',
            'active_callback'   =>  'olivewp_plus_single_post_callback'
        )
    ));
    // setting for the single post meta link color
    $wp_customize->add_setting('single_post_meta_link_color', 
        array(
            'default'           => '#858585',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'single_post_meta_link_color', 
        array(
            'label'             =>  esc_html__('Meta Link Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_single_post',
            'setting'           =>  'single_post_meta_link_color',
            'active_callback'   =>  'olivewp_plus_single_post_callback'
        )
    ));
    // setting for the single post meta link hover color
    $wp_customize->add_setting('single_post_meta_link_hover_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'single_post_meta_link_hover_color', 
        array(
            'label'             =>  esc_html__('Meta Link Hover Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_single_post',
            'setting'           =>  'single_post_meta_link_hover_color',
            'active_callback'   =>  'olivewp_plus_single_post_callback'
        )
    ));




    /* ====================
    * Footer Bar
    ==================== */
    $wp_customize->add_section('olivewp_footer_bar', 
        array(
            'title'     =>  esc_html__('Footer Bar', 'olivewp-plus' ),
            'panel'     =>  'colors_back_settings',
            'priority'  =>  13
        )
    );
    // Enable/Disable the footer bar setting
    $wp_customize->add_setting('enable_footer_bar',
        array(
            'default'           =>  false,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_footer_bar',
        array(
            'label'     =>  esc_html__( 'Enable to apply the settings', 'olivewp-plus'  ),
            'section'   =>  'olivewp_footer_bar',
            'setting'   =>  'enable_footer_bar',
            'type'      =>  'toggle'
        )
    ));
    // setting for the footer bar background color
    $wp_customize->add_setting('footer_bar_back_color', 
        array(
            'default'           => '#000',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bar_back_color', 
        array(
            'label'             =>  esc_html__('Background Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_footer_bar',
            'setting'           =>  'footer_bar_back_color',
            'active_callback'   =>  'olivewp_plus_footer_bar_color_callback'
        )
    ));
    // setting for the footer bar title color
    $wp_customize->add_setting('footer_bar_title_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bar_title_color', 
        array(
            'label'             =>  esc_html__('Title Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_footer_bar',
            'setting'           =>  'footer_bar_title_color',
            'active_callback'   =>  'olivewp_plus_footer_bar_color_callback'
        )
    ));
    // setting for the footer bar text color
    $wp_customize->add_setting('footer_bar_text_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bar_text_color', 
        array(
            'label'             =>  esc_html__('Text Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_footer_bar',
            'setting'           =>  'footer_bar_text_color',
            'active_callback'   =>  'olivewp_plus_footer_bar_color_callback'
        )
    ));
    // setting for the footer bar link color
    $wp_customize->add_setting('footer_bar_link_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bar_link_color', 
        array(
            'label'             =>  esc_html__('Link Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_footer_bar',
            'setting'           =>  'footer_bar_link_color',
            'active_callback'   =>  'olivewp_plus_footer_bar_color_callback'
        )
    ));
    // setting for the footer bar link hover color
    $wp_customize->add_setting('footer_bar_link_hover_color', 
        array(
            'default'           => '#ff6f61',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'footer_bar_link_hover_color', 
        array(
            'label'             =>  esc_html__('Link Hover Color', 'olivewp-plus' ),
            'section'           =>  'olivewp_footer_bar',
            'setting'           =>  'footer_bar_link_hover_color',
            'active_callback'   =>  'olivewp_plus_footer_bar_color_callback'
        )
    ));

}

add_action('customize_register', 'olivewp_plus_color_back_customizer');