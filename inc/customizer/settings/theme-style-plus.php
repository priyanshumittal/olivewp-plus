<?php
/**
 * Theme Style Customizer
 *
 * @package OliveWP Plus
*/
function olivewp_plus_theme_style ( $wp_customize ) {

    // Theme Color Scheme
    $wp_customize->add_setting('theme_color', 
        array(
            'default'       => 'default.css',
            'capability'    => 'edit_theme_options'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Color_Customize_Control($wp_customize, 'theme_color',
        array(
            'label'                     =>  esc_html__('Predefined colors', 'olivewp-plus'),
            'section'                   =>  'theme_style',
            'type'                      =>  'radio',
            'priority'                  =>  1,
            'choices'                   =>  array(
                'default.css'           => 'bittersweet.png',
                'sky-blue.css'          => 'sky-blue.png',
                'orange.css'            => 'orange.png',
                'green.css'             => 'green.png',
                'red.css'               => 'red.png',
                'purple.css'            => 'purple.png',
                'moonstone.css'         => 'moonstone.png',
                'yellow.css'            => 'yellow.png',
                'indian-yellow.css'     => 'indian-yellow.png'
            )
        )
    ));


    // Note for predefined images, background image, and background color
    class Olivewp_Plus_Back_Image_Color_Note_Control extends WP_Customize_Control {

        public function render_content() {
            ?>
            <p><?php esc_html_e('Note: The below settings will work with the Boxed layout', 'olivewp-plus' ); ?></p>
            <?php
        }

    }
    $wp_customize->add_setting('pre_back_image_color_note',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Back_Image_Color_Note_Control($wp_customize, 'pre_back_image_color_note', 
        array(
            'section'   => 'theme_style',
            'priority'  =>  6
        )
    ));


    // Predefined Background images
    $wp_customize->add_setting('predefined_back_image', 
        array(
            'default'       => 'bg-img0.png',
            'capability'    => 'edit_theme_options'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Pre_Customize_Control($wp_customize, 'predefined_back_image',
        array(
            'label'                 =>  esc_html__('Predefined Background Images', 'olivewp-plus'),
            'section'               =>  'theme_style',
            'type'                  =>  'radio',
            'priority'              =>  7,
            'choices'               =>  array(
                'bg-img0.png'       => 'sm0.png',
                'bg-img1.png'       => 'sm1.png',
                'bg-img2.png'       => 'sm2.png',
                'bg-img3.png'       => 'sm3.png',
                'bg-img4.png'       => 'sm4.png',
                'bg-img5.png'       => 'sm5.png',
                'bg-img8.jpg'       => 'sm8.jpg',
                'bg-img9.jpg'       => 'sm9.jpg',
                'bg-img10.jpg'      => 'sm10.jpg'
            )
        )
    ));

}

add_action( 'customize_register', 'olivewp_plus_theme_style' );