<?php
/**
 * Typography Customizer
 *
 * @package OliveWP Plus
*/
function olivewp_plus_typography_customizer($wp_customize) {

    $wp_customize->add_panel('olivewp_plus_typography_setting', 
        array(
            'priority'      => 113,
            'capability'    => 'edit_theme_options',
            'title'         => esc_html__('Typography Settings', 'olivewp-plus' )
        )
    );

    $google_font_families = olivewp_plus_typo_fonts();
    if(class_exists('Spice_Adobe_Fonts')){
        $adobefonts_productid=get_option('saf_project_id');
        $adobe_font_families= spice_adobe_fonts_get_project_details($adobefonts_productid);
        if(!empty($adobe_font_families)){
        foreach($adobe_font_families as $adobe_font_family){
            $adobe_font_families_array[] = array($adobe_font_family['slug']=>strtolower($adobe_font_family['slug']));   
        }  

        $adobe_font_array =  call_user_func_array('array_merge', $adobe_font_families_array);
        $all_font_family_array=array_merge($google_font_families,$adobe_font_array);
    }else
    {
        $all_font_family_array = $google_font_families;
    }
        
        $font_family= $all_font_family_array;

    }else{
        $font_family=$google_font_families;
    } 
    $font_style = array('normal' => 'Normal', 'italic' => 'Italic');

    $text_transform = array('none' => 'None', 'capitalize' => 'Capitalize', 'lowercase' => 'Lowercase', 'uppercase' => 'Uppercase');


    /* ====================
    * Top Bar Widgets
    ==================== */
    $wp_customize->add_section('olivewp_top_bar_typography', 
        array(
            'title'     => esc_html__('Topbar Widgets', 'olivewp-plus' ),
            'panel'     => 'olivewp_plus_typography_setting',
            'priority'  => 1
        )
    );
    // Enable/Disable Topbar typography settings
    $wp_customize->add_setting('enable_top_bar_typography',
        array(
            'default'           =>  false,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_top_bar_typography',
        array(
            'label'         =>  esc_html__( 'Enable to apply the Typography', 'olivewp-plus'  ),
            'section'       =>  'olivewp_top_bar_typography',
            'type'          =>  'toggle'
        )
    ));
    // Heading for the topbar widget title
    class Olivewp_Plus_Topbar_Title_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Widget Title', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('topbar_widget_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Topbar_Title_Customize_Control($wp_customize, 'topbar_widget_title', 
        array(
            'section'           =>  'olivewp_top_bar_typography',
            'active_callback'   =>  'olivewp_plus_topbar_widget_typography_callback',
            'setting'           =>  'topbar_widget_title'
        )
    ));
    // Font family for the topbar widget title
    $wp_customize->add_setting('topbar_widget_title_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('topbar_widget_title_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_top_bar_typography',
            'setting'           => 'topbar_widget_title_fontfamily',
            'type'              => 'select',
            'active_callback'   => 'olivewp_plus_topbar_widget_typography_callback',
            'choices'           => $font_family
    ));
    // Font style for the topbar widget title
    $wp_customize->add_setting('topbar_widget_title_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('topbar_widget_title_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_top_bar_typography',
            'setting'           =>  'topbar_widget_title_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_topbar_widget_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the topbar widget title
    $wp_customize->add_setting('topbar_widget_title_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('topbar_widget_title_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_top_bar_typography',
            'setting'           =>  'topbar_widget_title_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_topbar_widget_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the topbar widget title
    $wp_customize->add_setting( 'topbar_widget_title_fontsize',
        array(
            'default'           => 30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'topbar_widget_title_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_top_bar_typography',
            'setting'           => 'topbar_widget_title_fontsize',
            'active_callback'   => 'olivewp_plus_topbar_widget_typography_callback',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the topbar widget title
    $wp_customize->add_setting( 'topbar_widget_title_line_height',
        array(
            'default'           => 45,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'topbar_widget_title_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_top_bar_typography',
            'setting'           => 'topbar_widget_title_line_height',
            'active_callback'   => 'olivewp_plus_topbar_widget_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the topbar widget title
    $wp_customize->add_setting( 'topbar_widget_title_font_weight',
        array(
            'default'           => 700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'topbar_widget_title_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_top_bar_typography',
            'setting'           => 'topbar_widget_title_font_weight',
            'active_callback'   => 'olivewp_plus_topbar_widget_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));
    

    // Heading for the topbar widget content
    class Olivewp_Plus_Topbar_Content_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Widget Content', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('topbar_widget_content',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Topbar_Content_Customize_Control($wp_customize, 'topbar_widget_content', 
        array(
            'section'           =>  'olivewp_top_bar_typography',
            'active_callback'   =>  'olivewp_plus_topbar_widget_typography_callback',
            'setting'           =>  'topbar_widget_content'
        )
    ));
    // Font family for the topbar widget content
    $wp_customize->add_setting('topbar_widget_content_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('topbar_widget_content_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_top_bar_typography',
            'setting'           => 'topbar_widget_content_fontfamily',
            'type'              => 'select',
            'active_callback'   => 'olivewp_plus_topbar_widget_typography_callback',
            'choices'           => $font_family
    ));
    // Font style for the topbar widget content
    $wp_customize->add_setting('topbar_widget_content_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('topbar_widget_content_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_top_bar_typography',
            'setting'           =>  'topbar_widget_content_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_topbar_widget_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the topbar widget content
    $wp_customize->add_setting('topbar_widget_content_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('topbar_widget_content_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_top_bar_typography',
            'setting'           =>  'topbar_widget_content_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_topbar_widget_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the topbar widget content
    $wp_customize->add_setting( 'topbar_widget_content_fontsize',
        array(
            'default'           => 16,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'topbar_widget_content_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_top_bar_typography',
            'setting'           => 'topbar_widget_content_fontsize',
            'active_callback'   => 'olivewp_plus_topbar_widget_typography_callback',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the topbar widget content
    $wp_customize->add_setting( 'topbar_widget_content_line_height',
        array(
            'default'           => 29,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'topbar_widget_content_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_top_bar_typography',
            'setting'           => 'topbar_widget_content_line_height',
            'active_callback'   => 'olivewp_plus_topbar_widget_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the topbar widget content
    $wp_customize->add_setting( 'topbar_widget_content_font_weight',
        array(
            'default'           => 400,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'topbar_widget_content_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_top_bar_typography',
            'setting'           => 'topbar_widget_content_font_weight',
            'active_callback'   => 'olivewp_plus_topbar_widget_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));




    /* ====================
    * Header
    ==================== */
    $wp_customize->add_section('olivewp_header_typography', 
        array(
            'title'     => esc_html__('Header', 'olivewp-plus' ),
            'panel'     => 'olivewp_plus_typography_setting',
            'priority'  => 2
        )
    );
    // Enable/Disable Header typography settings
    $wp_customize->add_setting('enable_header_typography',
        array(
            'default'           =>  false,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_header_typography',
        array(
            'label'         =>  esc_html__( 'Enable to apply the Typography', 'olivewp-plus'  ),
            'section'       =>  'olivewp_header_typography',
            'type'          =>  'toggle'
        )
    ));
    // Heading for the Site title
    class Olivewp_Plus_Sitetitle_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Site Title', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('site_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Sitetitle_Customize_Control($wp_customize, 'site_title', 
        array(
            'section'           =>  'olivewp_header_typography',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'setting'           =>  'site_title'
        )
    ));
    // Font family for the site title
    $wp_customize->add_setting('site_title_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('site_title_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_header_typography',
            'setting'           => 'site_title_fontfamily',
            'type'              => 'select',
            'active_callback'   => 'olivewp_plus_header_typography_callback',
            'choices'           => $font_family
    ));
    // Font style for the site title
    $wp_customize->add_setting('site_title_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('site_title_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'site_title_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the site title
    $wp_customize->add_setting('site_title_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('site_title_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'site_title_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the site title
    $wp_customize->add_setting( 'site_title_fontsize',
        array(
            'default'           => 30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'site_title_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_header_typography',
            'setting'           => 'site_title_fontsize',
            'active_callback'   => 'olivewp_plus_header_typography_callback',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the site title
    $wp_customize->add_setting( 'site_title_line_height',
        array(
            'default'           => 39,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'site_title_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_header_typography',
            'setting'           => 'site_title_line_height',
            'active_callback'   => 'olivewp_plus_header_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the site title
    $wp_customize->add_setting( 'site_title_font_weight',
        array(
            'default'           => 600,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'site_title_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_header_typography',
            'setting'           => 'site_title_font_weight',
            'active_callback'   => 'olivewp_plus_header_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the Tagline
    class Olivewp_Plus_Tagline_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Site Tagline', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('tagline',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Tagline_Customize_Control($wp_customize, 'tagline', 
        array(
                'section'           =>  'olivewp_header_typography',
                'active_callback'   =>  'olivewp_plus_header_typography_callback',
                'setting'           =>  'tagline'
            )
    ));
    // Font family for the tagline
    $wp_customize->add_setting('tagline_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('tagline_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_header_typography',
            'setting'           => 'tagline_fontfamily',
            'type'              => 'select',
            'active_callback'   => 'olivewp_plus_header_typography_callback',
            'choices'           => $font_family
    ));
    // Font style for the tagline
    $wp_customize->add_setting('tagline_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('tagline_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'tagline_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the tagline
    $wp_customize->add_setting('tagline_text_transform',
        array(
            'default'           =>  'none',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('tagline_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'tagline_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the tagline
    $wp_customize->add_setting( 'tagline_fontsize',
        array(
            'default'           =>  18,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'tagline_fontsize',
        array(
            'label'             =>  esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'tagline_fontsize',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the tagline
    $wp_customize->add_setting( 'tagline_line_height',
        array(
            'default'           =>  29,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'tagline_line_height',
        array(
            'label'             =>  esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'tagline_line_height',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the tagline
    $wp_customize->add_setting( 'tagline_font_weight',
        array(
            'default'           => 400,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'tagline_font_weight',
        array(
            'label'             =>  esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'tagline_font_weight',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the Menu
    class Olivewp_Plus_Menu_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Menus', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('menu',
        array(
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Menu_Customize_Control($wp_customize, 'menu', 
        array(
                'section'           =>  'olivewp_header_typography',
                'active_callback'   =>  'olivewp_plus_header_typography_callback',
                'setting'           =>  'menu'
            )
    ));
    // Font family for the menu
    $wp_customize->add_setting('menu_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('menu_fontfamily', 
        array(
            'label'             =>  esc_html__('Font family', 'olivewp-plus' ),
            'section'           =>  'olivewp_header_typography',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'setting'           =>  'menu_fontfamily',
            'type'              =>  'select',
            'choices'           =>  $font_family
    ));
    // Font style for the menu
    $wp_customize->add_setting('menu_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('menu_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'menu_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the menu
    $wp_customize->add_setting('menu_text_transform',
        array(
            'default'           =>  'none',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('menu_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'menu_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the menu
    $wp_customize->add_setting( 'menu_fontsize',
        array(
            'default'           =>  14,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'menu_fontsize',
        array(
            'label'             =>  esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'menu_fontsize',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the menu
    $wp_customize->add_setting( 'menu_line_height',
        array(
            'default'           =>  22,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'menu_line_height',
        array(
            'label'             =>  esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'menu_line_height',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the menu
    $wp_customize->add_setting( 'menu_font_weight',
        array(
            'default'           => 600,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'menu_font_weight',
        array(
            'label'             =>  esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'menu_font_weight',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the Submenu
    class Olivewp_Plus_Submenu_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Submenus', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('submenu',
        array(
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Submenu_Customize_Control($wp_customize, 'submenu', 
        array(
                'section'           =>  'olivewp_header_typography',
                'active_callback'   =>  'olivewp_plus_header_typography_callback',
                'setting'           =>  'submenu'
            )
    ));
    // Font family for the submenu
    $wp_customize->add_setting('submenu_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('submenu_fontfamily', 
        array(
            'label'             =>  esc_html__('Font family', 'olivewp-plus' ),
            'section'           =>  'olivewp_header_typography',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'setting'           =>  'submenu_fontfamily',
            'type'              =>  'select',
            'choices'           =>  $font_family
    ));
    // Font style for the submenu
    $wp_customize->add_setting('submenu_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('submenu_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'submenu_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the submenu
    $wp_customize->add_setting('submenu_text_transform',
        array(
            'default'           =>  'none',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('submenu_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'submenu_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the submenu
    $wp_customize->add_setting( 'submenu_fontsize',
        array(
            'default'           =>  14,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'submenu_fontsize',
        array(
            'label'             =>  esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'submenu_fontsize',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the submenu
    $wp_customize->add_setting( 'submenu_line_height',
        array(
            'default'           =>  22,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'submenu_line_height',
        array(
            'label'             =>  esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'submenu_line_height',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the submenu
    $wp_customize->add_setting( 'submenu_font_weight',
        array(
            'default'           => 600,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'submenu_font_weight',
        array(
            'label'             =>  esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_header_typography',
            'setting'           =>  'submenu_font_weight',
            'active_callback'   =>  'olivewp_plus_header_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));




    /* ====================
    * After Menu Button
    ==================== */
    $wp_customize->add_section('olivewp_after_btn_typography', 
        array(
            'title'     => esc_html__('After Menu Button', 'olivewp-plus' ),
            'panel'     => 'olivewp_plus_typography_setting',
            'priority'  => 3
        )
    );
    // Enable/Disable After Menu Button typography settings
    $wp_customize->add_setting('enable_after_btn_typography',
        array(
            'default'           =>  false,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_after_btn_typography',
        array(
            'label'         =>  esc_html__( 'Enable to apply the Typography', 'olivewp-plus'  ),
            'section'       =>  'olivewp_after_btn_typography',
            'type'          =>  'toggle'
        )
    ));
    // Font family for the after menu button
    $wp_customize->add_setting('after_btn_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('after_btn_fontfamily', 
        array(
            'label'             =>  esc_html__('Font family', 'olivewp-plus' ),
            'section'           =>  'olivewp_after_btn_typography',
            'active_callback'   =>  'olivewp_plus_after_menu_typography_callback',
            'setting'           =>  'after_btn_fontfamily',
            'type'              =>  'select',
            'choices'           =>  $font_family
    ));
    // Font style for the after menu button
    $wp_customize->add_setting('after_btn_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('after_btn_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_after_btn_typography',
            'setting'           =>  'after_btn_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_after_menu_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the after menu button
    $wp_customize->add_setting('after_btn_text_transform',
        array(
            'default'           =>  'uppercase',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('after_btn_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_after_btn_typography',
            'setting'           =>  'after_btn_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_after_menu_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the after menu button
    $wp_customize->add_setting( 'after_btn_fontsize',
        array(
            'default'           =>  14,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'after_btn_fontsize',
        array(
            'label'             =>  esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_after_btn_typography',
            'setting'           =>  'after_btn_fontsize',
            'active_callback'   =>  'olivewp_plus_after_menu_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the after menu button
    $wp_customize->add_setting( 'after_btn_line_height',
        array(
            'default'           =>  22,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'after_btn_line_height',
        array(
            'label'             =>  esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_after_btn_typography',
            'setting'           =>  'after_btn_line_height',
            'active_callback'   =>  'olivewp_plus_after_menu_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the after menu button
    $wp_customize->add_setting( 'after_btn_font_weight',
        array(
            'default'           => 600,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'after_btn_font_weight',
        array(
            'label'             =>  esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_after_btn_typography',
            'setting'           =>  'after_btn_font_weight',
            'active_callback'   =>  'olivewp_plus_after_menu_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));




    /* ====================
    * Breadcrumb Section
    ==================== */
    $wp_customize->add_section('olivewp_banner_typography', 
        array(
            'title'     => esc_html__('Banner', 'olivewp-plus' ),
            'panel'     => 'olivewp_plus_typography_setting',
            'priority'  => 4
        )
    );
    // Enable/Disable Banner typography settings
    $wp_customize->add_setting('enable_banner_typography',
        array(
            'default'           =>  false,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_banner_typography',
        array(
            'label'         =>  esc_html__( 'Enable to apply the Typography', 'olivewp-plus'  ),
            'section'       =>  'olivewp_banner_typography',
            'type'          =>  'toggle'
        )
    ));

    // Heading for the page title
    class Olivewp_Plus_Page_Title_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Page Title', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('banner_page_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Page_Title_Customize_Control($wp_customize, 'banner_page_title', 
        array(
            'section'           =>  'olivewp_banner_typography',
            'active_callback'   =>  'olivewp_plus_banner_typography_callback',
            'setting'           =>  'banner_page_title'
        )
    ));
    // Font family for the page title
    $wp_customize->add_setting('banner_page_title_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('banner_page_title_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_banner_typography',
            'setting'           => 'banner_page_title_fontfamily',
            'type'              => 'select',
            'active_callback'   => 'olivewp_plus_banner_typography_callback',
            'choices'           => $font_family
    ));
    // Font style for the page title
    $wp_customize->add_setting('banner_page_title_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('banner_page_title_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_banner_typography',
            'setting'           =>  'banner_page_title_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_banner_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the page title
    $wp_customize->add_setting('banner_page_title_text_transform',
        array(
            'default'           => 'capitalize',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('banner_page_title_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_banner_typography',
            'setting'           =>  'banner_page_title_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_banner_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the page title
    $wp_customize->add_setting( 'banner_page_title_fontsize',
        array(
            'default'           => 42,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'banner_page_title_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_banner_typography',
            'setting'           => 'banner_page_title_fontsize',
            'active_callback'   => 'olivewp_plus_banner_typography_callback',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the page title
    $wp_customize->add_setting( 'banner_page_title_line_height',
        array(
            'default'           => 63,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'banner_page_title_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_banner_typography',
            'setting'           => 'banner_page_title_line_height',
            'active_callback'   => 'olivewp_plus_banner_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the page title
    $wp_customize->add_setting( 'banner_page_title_font_weight',
        array(
            'default'           => 700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'banner_page_title_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_banner_typography',
            'setting'           => 'banner_page_title_font_weight',
            'active_callback'   => 'olivewp_plus_banner_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));
    // Heading for the breadcrumb title
    class Olivewp_Plus_Bread_Title_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Breadcrumb Title', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('breadcrumb_page_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Bread_Title_Customize_Control($wp_customize, 'breadcrumb_page_title', 
        array(
            'section'           =>  'olivewp_banner_typography',
            'active_callback'   =>  'olivewp_plus_banner_typography_callback',
            'setting'           =>  'breadcrumb_page_title'
        )
    ));
    // Font family for the breadcrumb title
    $wp_customize->add_setting('banner_breadcrumb_title_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('banner_breadcrumb_title_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_banner_typography',
            'setting'           => 'banner_breadcrumb_title_fontfamily',
            'type'              => 'select',
            'active_callback'   => 'olivewp_plus_banner_typography_callback',
            'choices'           => $font_family
    ));
    // Font style for the breadcrumb title
    $wp_customize->add_setting('banner_breadcrumb_title_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('banner_breadcrumb_title_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_banner_typography',
            'setting'           =>  'banner_breadcrumb_title_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_banner_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the breadcrumb title
    $wp_customize->add_setting('banner_breadcrumb_title_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('banner_breadcrumb_title_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_banner_typography',
            'setting'           =>  'banner_breadcrumb_title_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_banner_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the breadcrumb title
    $wp_customize->add_setting( 'banner_breadcrumb_title_fontsize',
        array(
            'default'           => 16,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'banner_breadcrumb_title_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_banner_typography',
            'setting'           => 'banner_breadcrumb_title_fontsize',
            'active_callback'   => 'olivewp_plus_banner_typography_callback',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the breadcrumb title
    $wp_customize->add_setting( 'banner_breadcrumb_title_line_height',
        array(
            'default'           => 20,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'banner_breadcrumb_title_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_banner_typography',
            'setting'           => 'banner_breadcrumb_title_line_height',
            'active_callback'   => 'olivewp_plus_banner_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the breadcrumb title
    $wp_customize->add_setting( 'banner_breadcrumb_title_font_weight',
        array(
            'default'           => 400,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'banner_breadcrumb_title_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_banner_typography',
            'setting'           => 'banner_breadcrumb_title_font_weight',
            'active_callback'   => 'olivewp_plus_banner_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));




    /* ====================
    * Content(H1----H6, Paragraph, Button) typography section 
    ==================== */
    $wp_customize->add_section('olivewp_content_typography', 
        array(
            'title'     => esc_html__('Content', 'olivewp-plus' ),
            'panel'     => 'olivewp_plus_typography_setting',
            'priority'  => 5
        )
    );
    // Enable/Disable Content typography section
    $wp_customize->add_setting('enable_content_typography',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_content_typography',
        array(
            'label'     => esc_html__( 'Enable to apply the Typography', 'olivewp-plus'  ),
            'section'   => 'olivewp_content_typography',
            'type'      => 'toggle'
        )
    ));
    // Heading for the H1
    class Olivewp_Plus_H1_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Heading 1 (H1)', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('content_h1',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_H1_Customize_Control($wp_customize, 'content_h1', 
        array(
                'active_callback'   => 'olivewp_plus_content_typography_callback',
                'section'           =>  'olivewp_content_typography',
                'setting'           =>  'content_h1'
            )
    ));
    //Font family for the H1
    $wp_customize->add_setting('content_h1_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h1_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h1_fontfamily',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the H1
    $wp_customize->add_setting('content_h1_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h1_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_h1_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the H1
    $wp_customize->add_setting('content_h1_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h1_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_h1_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the H1
    $wp_customize->add_setting( 'content_h1_fontsize',
        array(
            'default'           => 42,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h1_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h1_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the H1
    $wp_customize->add_setting( 'content_h1_line_height',
        array(
            'default'           => 63,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h1_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h1_line_height',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the H1
    $wp_customize->add_setting( 'content_h1_font_weight',
        array(
            'default'           => 700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h1_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h1_font_weight',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the H2
    class Olivewp_Plus_H2_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Heading 2 (H2)', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('content_h2',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_H2_Customize_Control($wp_customize, 'content_h2', 
        array(
                'active_callback'   =>  'olivewp_plus_content_typography_callback',
                'section'           =>  'olivewp_content_typography',
                'setting'           =>  'content_h2'
            )
    ));
    //Font family for the H2
    $wp_customize->add_setting('content_h2_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h2_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h2_fontfamily',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the H2
    $wp_customize->add_setting('content_h2_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h2_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_h2_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the H2
    $wp_customize->add_setting('content_h2_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h2_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_h2_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the H2
    $wp_customize->add_setting( 'content_h2_fontsize',
        array(
            'default'           => 30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h2_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h2_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the H2
    $wp_customize->add_setting( 'content_h2_line_height',
        array(
            'default'           => 45,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h2_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h2_line_height',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the H2
    $wp_customize->add_setting( 'content_h2_font_weight',
        array(
            'default'           => 700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h2_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h2_font_weight',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the H3
    class Olivewp_Plus_H3_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Heading 3 (H3)', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('content_h3',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_H3_Customize_Control($wp_customize, 'content_h3', 
        array(
                'active_callback'   =>  'olivewp_plus_content_typography_callback',
                'section'           =>  'olivewp_content_typography',
                'setting'           =>  'content_h3'
            )
    ));
    //Font family for the H3
    $wp_customize->add_setting('content_h3_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h3_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h3_fontfamily',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the H3
    $wp_customize->add_setting('content_h3_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h3_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_h3_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the H3
    $wp_customize->add_setting('content_h3_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h3_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_h3_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the H3
    $wp_customize->add_setting( 'content_h3_fontsize',
        array(
            'default'           => 24,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h3_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h3_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the H3
    $wp_customize->add_setting( 'content_h3_line_height',
        array(
            'default'           => 36,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h3_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h3_line_height',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the H3
    $wp_customize->add_setting( 'content_h3_font_weight',
        array(
            'default'           => 700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h3_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h3_font_weight',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the H4
    class Olivewp_Plus_H4_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Heading 4 (H4)', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('content_h4',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_H4_Customize_Control($wp_customize, 'content_h4', 
        array(
                'active_callback'   =>  'olivewp_plus_content_typography_callback',
                'section'           =>  'olivewp_content_typography',
                'setting'           =>  'content_h4'
            )
    ));
    //Font family for the H4
    $wp_customize->add_setting('content_h4_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h4_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h4_fontfamily',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the H4
    $wp_customize->add_setting('content_h4_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h4_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_h4_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the H4
    $wp_customize->add_setting('content_h4_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h4_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_h4_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the H4
    $wp_customize->add_setting( 'content_h4_fontsize',
        array(
            'default'           => 20,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h4_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h4_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the H4
    $wp_customize->add_setting( 'content_h4_line_height',
        array(
            'default'           => 30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h4_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h4_line_height',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the H4
    $wp_customize->add_setting( 'content_h4_font_weight',
        array(
            'default'           => 700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h4_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h4_font_weight',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the H5
    class Olivewp_Plus_H5_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Heading 5 (H5)', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('content_h5',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_H5_Customize_Control($wp_customize, 'content_h5', 
        array(
                'active_callback'   =>  'olivewp_plus_content_typography_callback',
                'section'           =>  'olivewp_content_typography',
                'setting'           =>  'content_h5'
            )
    ));
    //Font family for the H5
    $wp_customize->add_setting('content_h5_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h5_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h5_fontfamily',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the H5
    $wp_customize->add_setting('content_h5_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h5_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_h5_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the H5
    $wp_customize->add_setting('content_h5_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h5_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_h5_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the H5
    $wp_customize->add_setting( 'content_h5_fontsize',
        array(
            'default'           => 18,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h5_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h5_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the H5
    $wp_customize->add_setting( 'content_h5_line_height',
        array(
            'default'           => 27,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h5_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h5_line_height',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the H5
    $wp_customize->add_setting( 'content_h5_font_weight',
        array(
            'default'           => 700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h5_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h5_font_weight',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the H6
    class Olivewp_Plus_H6_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Heading 6 (H6)', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('content_h6',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_H6_Customize_Control($wp_customize, 'content_h6', 
        array(
                'active_callback'   =>  'olivewp_plus_content_typography_callback',
                'section'           =>  'olivewp_content_typography',
                'setting'           =>  'content_h6'
            )
    ));
    //Font family for the H6
    $wp_customize->add_setting('content_h6_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h6_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h6_fontfamily',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the H6
    $wp_customize->add_setting('content_h6_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h6_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_h6_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the H6
    $wp_customize->add_setting('content_h6_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_h6_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_h6_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the H6
    $wp_customize->add_setting( 'content_h6_fontsize',
        array(
            'default'           => 16,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h6_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h6_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the H6
    $wp_customize->add_setting( 'content_h6_line_height',
        array(
            'default'           => 24,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h6_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h6_line_height',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the H6
    $wp_customize->add_setting( 'content_h6_font_weight',
        array(
            'default'           => 700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_h6_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_h6_font_weight',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the Paragraph
    class Olivewp_Plus_Paragraph_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Paragraph', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('content_p',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Paragraph_Customize_Control($wp_customize, 'content_p', 
        array(
                'active_callback'   =>  'olivewp_plus_content_typography_callback',
                'section'           =>  'olivewp_content_typography',
                'setting'           =>  'content_p'
            )
    ));
    //Font family for the Paragraph
    $wp_customize->add_setting('content_p_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_p_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_p_fontfamily',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the Paragraph
    $wp_customize->add_setting('content_p_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_p_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_p_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the Paragraph
    $wp_customize->add_setting('content_p_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_p_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_p_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the Paragraph
    $wp_customize->add_setting( 'content_p_fontsize',
        array(
            'default'           => 18,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_p_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_p_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the Paragraph
    $wp_customize->add_setting( 'content_p_line_height',
        array(
            'default'           => 29,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_p_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_p_line_height',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the Paragraph
    $wp_customize->add_setting( 'content_p_font_weight',
        array(
            'default'           => 400,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_p_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_p_font_weight',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the Button text
    class Olivewp_Plus_Button_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Button Text', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('content_button',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Button_Customize_Control($wp_customize, 'content_button', 
        array(
                'active_callback'   =>  'olivewp_plus_content_typography_callback',
                'section'           =>  'olivewp_content_typography',
                'setting'           =>  'content_button'
            )
    ));
    //Font family for the Button
    $wp_customize->add_setting('content_button_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_button_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_button_fontfamily',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the Button
    $wp_customize->add_setting('content_button_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_button_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_button_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the Button
    $wp_customize->add_setting('content_button_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('content_button_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_content_typography',
            'setting'           =>  'content_button_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_content_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the Button
    $wp_customize->add_setting( 'content_button_fontsize',
        array(
            'default'           => 14,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_button_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_button_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the Button
    $wp_customize->add_setting( 'content_button_line_height',
        array(
            'default'           => 14,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_button_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_button_line_height',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the Button
    $wp_customize->add_setting( 'content_button_font_weight',
        array(
            'default'           => 600,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_button_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_button_font_weight',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));
    // Border Radius for the Button
    $wp_customize->add_setting( 'content_button_border_radius',
        array(
            'default'           => 4,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_button_border_radius',
        array(
            'label'             => esc_html__( 'Border Radius (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_button_border_radius',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  50,
                'step'  =>  1
            ),
        )
    ));
    // Padding top for the Button
    $wp_customize->add_setting( 'content_button_padtop',
        array(
            'default'           => 8,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_button_padtop',
        array(
            'label'             => esc_html__( 'Padding Top (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_button_padtop',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  50,
                'step'  =>  1
            ),
        )
    ));
    // Padding bottom for the Button
    $wp_customize->add_setting( 'content_button_padbottom',
        array(
            'default'           => 8,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_button_padbottom',
        array(
            'label'             => esc_html__( 'Padding Bottom (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_button_padbottom',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  50,
                'step'  =>  1
            ),
        )
    ));
    // Padding left for the Button
    $wp_customize->add_setting( 'content_button_padleft',
        array(
            'default'           => 15,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_button_padleft',
        array(
            'label'             => esc_html__( 'Padding Left (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_button_padleft',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  50,
                'step'  =>  1
            ),
        )
    ));
    // Padding right for the Button
    $wp_customize->add_setting( 'content_button_padright',
        array(
            'default'           => 15,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'content_button_padright',
        array(
            'label'             => esc_html__( 'Padding Right (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_content_typography',
            'setting'           => 'content_button_padright',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  50,
                'step'  =>  1
            ),
        )
    ));
    class Olivewp_Plus_Button_Typo_Note extends WP_Customize_Control {

        public function render_content() {
            ?>
            <p><?php esc_html_e('Note: Button border-radius and padding settings will work on the blog page, contact form 7, and single post', 'olivewp-plus' ); ?></p>
            <?php
        }

    }
    $wp_customize->add_setting('button_typo_note',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Button_Typo_Note($wp_customize, 'button_typo_note', 
        array(
            'section' => 'olivewp_content_typography',
            'active_callback'   => 'olivewp_plus_content_typography_callback',
        )
    ));





    /* ====================
    * Blog/Archive/Single Post typography 
    ==================== */
    $wp_customize->add_section('olivewp_post_typography', 
        array(
            'title'     => esc_html__(' Blog/Archive/Single Post', 'olivewp-plus' ),
            'panel'     => 'olivewp_plus_typography_setting',
            'priority'  => 6
        )
    );
    // Enable/Disable Post typography section
    $wp_customize->add_setting('enable_post_typography',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_post_typography',
        array(
            'label'     => esc_html__( 'Enable to apply the Typography', 'olivewp-plus'  ),
            'section'   => 'olivewp_post_typography',
            'type'      => 'toggle'
        )
    ));
    // Font family for the Post
    $wp_customize->add_setting('post_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('post_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'active_callback'   => 'olivewp_plus_post_typography_callback',
            'section'           => 'olivewp_post_typography',
            'setting'           => 'post_fontfamily',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the Post
    $wp_customize->add_setting('post_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('post_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_post_typography',
            'setting'           =>  'post_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_post_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the Post
    $wp_customize->add_setting('post_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('post_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_post_typography',
            'setting'           =>  'post_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_post_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the Post
    $wp_customize->add_setting( 'post_fontsize',
        array(
            'default'           => 24,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'post_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_post_typography_callback',
            'section'           => 'olivewp_post_typography',
            'setting'           => 'post_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the Post
    $wp_customize->add_setting( 'post_line_height',
        array(
            'default'           => 36,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'post_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_post_typography_callback',
            'section'           => 'olivewp_post_typography',
            'setting'           => 'post_line_height',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the Post
    $wp_customize->add_setting( 'post_font_weight',
        array(
            'default'           => 600,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'post_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_post_typography',
            'setting'           => 'post_font_weight',
            'active_callback'   => 'olivewp_plus_post_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));





    /* ====================
    * Post Meta
    ==================== */
    $wp_customize->add_section('olivewp_post_meta_typography', 
        array(
            'title'     => esc_html__('Post Meta', 'olivewp-plus' ),
            'panel'     => 'olivewp_plus_typography_setting',
            'priority'  => 7
        )
    );
    // Enable/Disable post meta typography settings
    $wp_customize->add_setting('enable_post_meta_typography',
        array(
            'default'           =>  false,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_post_meta_typography',
        array(
            'label'         =>  esc_html__( 'Enable to apply the Typography', 'olivewp-plus'  ),
            'section'       =>  'olivewp_post_meta_typography',
            'type'          =>  'toggle'
        )
    ));
    // Font family for the post meta
    $wp_customize->add_setting('post_meta_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('post_meta_fontfamily', 
        array(
            'label'             =>  esc_html__('Font family', 'olivewp-plus' ),
            'section'           =>  'olivewp_post_meta_typography',
            'active_callback'   =>  'olivewp_plus_post_meta_typography_callback',
            'setting'           =>  'post_meta_fontfamily',
            'type'              =>  'select',
            'choices'           =>  $font_family
    ));
    // Font style for the post meta
    $wp_customize->add_setting('post_meta_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('post_meta_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'section'           =>  'olivewp_post_meta_typography',
            'setting'           =>  'post_meta_fontstyle',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_post_meta_typography_callback',
            'choices'           =>  $font_style
    ));
    // Text transform for the post meta
    $wp_customize->add_setting('post_meta_text_transform',
        array(
            'default'           =>  'none',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('post_meta_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           =>  'olivewp_post_meta_typography',
            'setting'           =>  'post_meta_text_transform',
            'type'              =>  'select',
            'active_callback'   =>  'olivewp_plus_post_meta_typography_callback',
            'choices'           =>  $text_transform
    ));
    // Font size for the post meta
    $wp_customize->add_setting( 'post_meta_fontsize',
        array(
            'default'           =>  16,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'post_meta_fontsize',
        array(
            'label'             =>  esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_post_meta_typography',
            'setting'           =>  'post_meta_fontsize',
            'active_callback'   =>  'olivewp_plus_post_meta_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the post meta
    $wp_customize->add_setting( 'post_meta_line_height',
        array(
            'default'           =>  26,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'post_meta_line_height',
        array(
            'label'             =>  esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_post_meta_typography',
            'setting'           =>  'post_meta_line_height',
            'active_callback'   =>  'olivewp_plus_post_meta_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the post meta
    $wp_customize->add_setting( 'post_meta_font_weight',
        array(
            'default'           => 400,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'post_meta_font_weight',
        array(
            'label'             =>  esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_post_meta_typography',
            'setting'           =>  'post_meta_font_weight',
            'active_callback'   =>  'olivewp_plus_post_meta_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));





    /* ====================
    * Shop Page
    ==================== */
    $wp_customize->add_section('olivewp_shop_page_typography', 
        array(
            'title'     =>  esc_html__('Shop Page', 'olivewp-plus' ),
            'panel'     =>  'olivewp_plus_typography_setting',
            'priority'  =>  8
        )
    );
    // Enable/Disable shop page typography settings
    $wp_customize->add_setting('enable_shop_typography',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_shop_typography',
        array(
            'label'     => esc_html__( 'Enable to apply the Typography', 'olivewp-plus'  ),
            'section'   => 'olivewp_shop_page_typography',
            'type'      => 'toggle'
        )
    ));
    // Heading for the Shop H1
    class Olivewp_Plus_Shop_H1_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Heading 1 (H1)', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('shop_h1',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Shop_H1_Customize_Control($wp_customize, 'shop_h1', 
        array(
                'active_callback'   => 'olivewp_plus_shop_page_typography_callback',
                'section'           =>  'olivewp_shop_page_typography',
                'setting'           =>  'shop_h1'
            )
    ));
    // Font family for the Shop H1
    $wp_customize->add_setting('shop_h1_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('shop_h1_fontfamily', 
        array(
            'label'             =>  esc_html__('Font family', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h1_fontfamily',
            'type'              =>  'select',
            'choices'           =>  $font_family
    ));
    // Font style for the Shop H1
    $wp_customize->add_setting('shop_h1_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('shop_h1_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h1_fontstyle',
            'type'              =>  'select',
            'choices'           =>  $font_style
    ));
    // Text transform for the Shop H1
    $wp_customize->add_setting('shop_h1_text_transform',
        array(
            'default'           =>  'none',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('shop_h1_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h1_text_transform',
            'type'              =>  'select',
            'choices'           =>  $text_transform
    ));
    // Font size for the Shop H1
    $wp_customize->add_setting( 'shop_h1_fontsize',
        array(
            'default'           =>  42,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'shop_h1_fontsize',
        array(
            'label'             =>  esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h1_fontsize',
            'input_attrs'       =>  array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the Shop H1
    $wp_customize->add_setting( 'shop_h1_line_height',
        array(
            'default'           =>  63,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'shop_h1_line_height',
        array(
            'label'             =>  esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h1_line_height',
            'input_attrs'       =>  array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the Shop H1
    $wp_customize->add_setting( 'shop_h1_font_weight',
        array(
            'default'           => 700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'shop_h1_font_weight',
        array(
            'label'             =>  esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h1_font_weight',
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the Shop H2
    class Olivewp_Plus_Shop_H2_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Heading 2 (H2)', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('shop_h2',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Shop_H2_Customize_Control($wp_customize, 'shop_h2', 
        array(
                'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
                'section'           =>  'olivewp_shop_page_typography',
                'setting'           =>  'shop_h2'
            )
    ));
    // Font family for the Shop H2
    $wp_customize->add_setting('shop_h2_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('shop_h2_fontfamily', 
        array(
            'label'             =>  esc_html__('Font family', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h2_fontfamily',
            'type'              =>  'select',
            'choices'           =>  $font_family
    ));
    // Font style for the Shop H2
    $wp_customize->add_setting('shop_h2_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('shop_h2_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h2_fontstyle',
            'type'              =>  'select',
            'choices'           =>  $font_style
    ));
    // Text transform for the Shop H2
    $wp_customize->add_setting('shop_h2_text_transform',
        array(
            'default'           =>  'none',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('shop_h2_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h2_text_transform',
            'type'              =>  'select',
            'choices'           =>  $text_transform
    ));
    // Font size for the Shop H2
    $wp_customize->add_setting( 'shop_h2_fontsize',
        array(
            'default'           =>  18,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'shop_h2_fontsize',
        array(
            'label'             =>  esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h2_fontsize',
            'input_attrs'       =>  array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the Shop H2
    $wp_customize->add_setting( 'shop_h2_line_height',
        array(
            'default'           =>  27,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'shop_h2_line_height',
        array(
            'label'             =>  esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h2_line_height',
            'input_attrs'       =>  array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the Shop H2
    $wp_customize->add_setting( 'shop_h2_font_weight',
        array(
            'default'           => 700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'shop_h2_font_weight',
        array(
            'label'             =>  esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h2_font_weight',
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the Shop H3
    class Olivewp_Plus_Shop_H3_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Heading 3 (H3)', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('shop_h3',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Shop_H3_Customize_Control($wp_customize, 'shop_h3', 
        array(
                'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
                'section'           =>  'olivewp_shop_page_typography',
                'setting'           =>  'shop_h3'
            )
    ));
    // Font family for the Shop H3
    $wp_customize->add_setting('shop_h3_fontfamily',
        array(
            'default'           =>  'Poppins',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('shop_h3_fontfamily', 
        array(
            'label'             =>  esc_html__('Font family', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h3_fontfamily',
            'type'              =>  'select',
            'choices'           =>  $font_family
    ));
    // Font style for the Shop H3
    $wp_customize->add_setting('shop_h3_fontstyle',
        array(
            'default'           =>  'normal',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('shop_h3_fontstyle', 
        array(
            'label'             =>  esc_html__('Font style', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h3_fontstyle',
            'type'              =>  'select',
            'choices'           =>  $font_style
    ));
    // Text transform for the Shop H3
    $wp_customize->add_setting('shop_h3_text_transform',
        array(
            'default'           =>  'none',
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('shop_h3_text_transform', 
        array(
            'label'             =>  esc_html__('Text Transform', 'olivewp-plus' ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h3_text_transform',
            'type'              =>  'select',
            'choices'           =>  $text_transform
    ));
    // Font size for the Shop H3
    $wp_customize->add_setting( 'shop_h3_fontsize',
        array(
            'default'           =>  24,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'shop_h3_fontsize',
        array(
            'label'             =>  esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h3_fontsize',
            'input_attrs'       =>  array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the Shop H3
    $wp_customize->add_setting( 'shop_h3_line_height',
        array(
            'default'           =>  36,
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'shop_h3_line_height',
        array(
            'label'             =>  esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h3_line_height',
            'input_attrs'       =>  array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the Shop H3
    $wp_customize->add_setting( 'shop_h3_font_weight',
        array(
            'default'           => 700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'shop_h3_font_weight',
        array(
            'label'             =>  esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           =>  'olivewp_shop_page_typography',
            'setting'           =>  'shop_h3_font_weight',
            'active_callback'   =>  'olivewp_plus_shop_page_typography_callback',
            'input_attrs'       =>  array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));




    /* ====================
    * Sidebar Widgets
    ==================== */
    $wp_customize->add_section('olivewp_sidebar_typography', 
        array(
            'title'     => esc_html__('Sidebar Widgets', 'olivewp-plus' ),
            'panel'     => 'olivewp_plus_typography_setting',
            'priority'  => 9
        )
    );
    // Enable/Disable sidebar widgets typography settings
    $wp_customize->add_setting('enable_sidebar_typography',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_sidebar_typography',
        array(
            'label'     => esc_html__( 'Enable to apply the Typography', 'olivewp-plus'  ),
            'section'   => 'olivewp_sidebar_typography',
            'type'      => 'toggle'
        )
    ));
    // Heading for the sidebar widgets title
    class Olivewp_Plus_Sidebar_Title_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Widget Title', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('sidebar_widget_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Sidebar_Title_Customize_Control($wp_customize, 'sidebar_widget_title', 
        array(
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'section'           =>  'olivewp_sidebar_typography',
            'setting'           =>  'sidebar_widget_title'
        )
    ));
    // Font family for the sidebar widgets title
    $wp_customize->add_setting('sidebar_widget_title_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('sidebar_widget_title_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_sidebar_typography',
            'setting'           => 'sidebar_widget_title_fontfamily',
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the sidebar widgets title
    $wp_customize->add_setting('sidebar_widget_title_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('sidebar_widget_title_fontstyle', 
        array(
            'label'             => esc_html__('Font style', 'olivewp-plus' ),
            'section'           => 'olivewp_sidebar_typography',
            'setting'           => 'sidebar_widget_title_fontstyle',
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'type'              => 'select',
            'choices'           => $font_style
    ));
    // Text transform for the sidebar widgets title
    $wp_customize->add_setting('sidebar_widget_title_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('sidebar_widget_title_text_transform', 
        array(
            'label'             => esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           => 'olivewp_sidebar_typography',
            'setting'           => 'sidebar_widget_title_text_transform',
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'type'              => 'select',
            'choices'           => $text_transform
    ));
    // Font size for the sidebar widgets title
    $wp_customize->add_setting( 'sidebar_widget_title_fontsize',
        array(
            'default'           => 30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'sidebar_widget_title_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'section'           => 'olivewp_sidebar_typography',
            'setting'           => 'sidebar_widget_title_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the sidebar widgets title
    $wp_customize->add_setting( 'sidebar_widget_title_line_height',
        array(
            'default'           => 45,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'sidebar_widget_title_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_sidebar_typography',
            'setting'           => 'sidebar_widget_title_line_height',
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the sidebar widgets title
    $wp_customize->add_setting( 'sidebar_widget_title_font_weight',
        array(
            'default'           => 700,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'sidebar_widget_title_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_sidebar_typography',
            'setting'           => 'sidebar_widget_title_font_weight',
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the sidebar widgets content
    class Olivewp_Plus_Sidebar_Content_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Widget Content', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('sidebar_widget_content',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Sidebar_Content_Customize_Control($wp_customize, 'sidebar_widget_content', 
        array(
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'section'           =>  'olivewp_sidebar_typography',
            'setting'           =>  'sidebar_widget_content'
        )
    ));
    // Font family for the sidebar widgets content
    $wp_customize->add_setting('sidebar_widget_content_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('sidebar_widget_content_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_sidebar_typography',
            'setting'           => 'sidebar_widget_content_fontfamily',
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the sidebar widgets content
    $wp_customize->add_setting('sidebar_widget_content_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('sidebar_widget_content_fontstyle', 
        array(
            'label'             => esc_html__('Font style', 'olivewp-plus' ),
            'section'           => 'olivewp_sidebar_typography',
            'setting'           => 'sidebar_widget_content_fontstyle',
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'type'              => 'select',
            'choices'           => $font_style
    ));
    // Text transform for the sidebar widgets content
    $wp_customize->add_setting('sidebar_widget_content_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('sidebar_widget_content_text_transform', 
        array(
            'label'             => esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           => 'olivewp_sidebar_typography',
            'setting'           => 'sidebar_widget_content_text_transform',
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'type'              => 'select',
            'choices'           => $text_transform
    ));
    // Font size for the sidebar widgets content
    $wp_customize->add_setting( 'sidebar_widget_content_fontsize',
        array(
            'default'           => 18,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'sidebar_widget_content_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'section'           => 'olivewp_sidebar_typography',
            'setting'           => 'sidebar_widget_content_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    /* == Line height for the sidebar widgets content == */
    $wp_customize->add_setting( 'sidebar_widget_content_line_height',
        array(
            'default'           => 29,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'sidebar_widget_content_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'section'           => 'olivewp_sidebar_typography',
            'setting'           => 'sidebar_widget_content_line_height',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the sidebar widgets content
    $wp_customize->add_setting( 'sidebar_widget_content_font_weight',
        array(
            'default'           => 400,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'sidebar_widget_content_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_sidebar_typography',
            'setting'           => 'sidebar_widget_content_font_weight',
            'active_callback'   => 'olivewp_plus_sidebar_widget_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));




    /* ====================
    * Footer Widgets
    ==================== */
    $wp_customize->add_section('olivewp_footer_typography', 
        array(
            'title'     => esc_html__('Footer Widgets', 'olivewp-plus' ),
            'panel'     => 'olivewp_plus_typography_setting',
            'priority'  => 10
        )
    );
    // Enable/Disable footer widgets typography settings
    $wp_customize->add_setting('enable_footer_typography',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_footer_typography',
        array(
            'label'     => esc_html__( 'Enable to apply the Typography', 'olivewp-plus'  ),
            'section'   => 'olivewp_footer_typography',
            'type'      => 'toggle'
        )
    ));
    // Heading for the footer widgets title
    class Olivewp_Plus_Footer_Title_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Widget Title', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('footer_widget_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Footer_Title_Customize_Control($wp_customize, 'footer_widget_title', 
        array(
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'section'           =>  'olivewp_footer_typography',
            'setting'           =>  'footer_widget_title'
        )
    ));
    // Font family for the footer widgets title
    $wp_customize->add_setting('footer_widget_title_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('footer_widget_title_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_footer_typography',
            'setting'           => 'footer_widget_title_fontfamily',
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the footer widgets title
    $wp_customize->add_setting('footer_widget_title_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('footer_widget_title_fontstyle', 
        array(
            'label'             => esc_html__('Font style', 'olivewp-plus' ),
            'section'           => 'olivewp_footer_typography',
            'setting'           => 'footer_widget_title_fontstyle',
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'type'              => 'select',
            'choices'           => $font_style
    ));
    // Text transform for the footer widgets title
    $wp_customize->add_setting('footer_widget_title_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('footer_widget_title_text_transform', 
        array(
            'label'             => esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           => 'olivewp_footer_typography',
            'setting'           => 'footer_widget_title_text_transform',
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'type'              => 'select',
            'choices'           => $text_transform
    ));
    // Font size for the footer widgets title
    $wp_customize->add_setting( 'footer_widget_title_fontsize',
        array(
            'default'           => 30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'footer_widget_title_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'section'           => 'olivewp_footer_typography',
            'setting'           => 'footer_widget_title_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the footer widgets title
    $wp_customize->add_setting( 'footer_widget_title_line_height',
        array(
            'default'           => 30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'footer_widget_title_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_footer_typography',
            'setting'           => 'footer_widget_title_line_height',
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the footer widgets title
    $wp_customize->add_setting( 'footer_widget_title_font_weight',
        array(
            'default'           => 600,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'footer_widget_title_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_footer_typography',
            'setting'           => 'footer_widget_title_font_weight',
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

    // Heading for the footer widgets content
    class Olivewp_Plus_Footer_Content_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Widget Content', 'olivewp-plus' ); ?></h3>
            
        <?php }
    }
    $wp_customize->add_setting('footer_widget_content',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Footer_Content_Customize_Control($wp_customize, 'footer_widget_content', 
        array(
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'section'           =>  'olivewp_footer_typography',
            'setting'           =>  'footer_widget_content'
        )
    ));
    // Font family for the footer widgets content
    $wp_customize->add_setting('footer_widget_content_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('footer_widget_content_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_footer_typography',
            'setting'           => 'footer_widget_content_fontfamily',
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the footer widgets content
    $wp_customize->add_setting('footer_widget_content_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('footer_widget_content_fontstyle', 
        array(
            'label'             => esc_html__('Font style', 'olivewp-plus' ),
            'section'           => 'olivewp_footer_typography',
            'setting'           => 'footer_widget_content_fontstyle',
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'type'              => 'select',
            'choices'           => $font_style
    ));
    // Text transform for the footer widgets content
    $wp_customize->add_setting('footer_widget_content_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('footer_widget_content_text_transform', 
        array(
            'label'             => esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           => 'olivewp_footer_typography',
            'setting'           => 'footer_widget_content_text_transform',
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'type'              => 'select',
            'choices'           => $text_transform
    ));
    // Font size for the footer widgets content
    $wp_customize->add_setting( 'footer_widget_content_fontsize',
        array(
            'default'           => 18,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'footer_widget_content_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'section'           => 'olivewp_footer_typography',
            'setting'           => 'footer_widget_content_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    /* == Line height for the footer widgets content == */
    $wp_customize->add_setting( 'footer_widget_content_line_height',
        array(
            'default'           => 29,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'footer_widget_content_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'section'           => 'olivewp_footer_typography',
            'setting'           => 'footer_widget_content_line_height',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the footer widgets content
    $wp_customize->add_setting( 'footer_widget_content_font_weight',
        array(
            'default'           => 400,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'footer_widget_content_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_footer_typography',
            'setting'           => 'footer_widget_content_font_weight',
            'active_callback'   => 'olivewp_plus_footer_widget_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));




    /* ====================
    * Footer Bar
    ==================== */
    $wp_customize->add_section('olivewp_footer_bar_typography', 
        array(
            'title'     => esc_html__('Footer Bar', 'olivewp-plus' ),
            'panel'     => 'olivewp_plus_typography_setting',
            'priority'  => 11
        )
    );
    // Enable/Disable footer bar typography settings
    $wp_customize->add_setting('enable_footer_bar_typography',
        array(
            'default'           => false,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control( $wp_customize, 'enable_footer_bar_typography',
        array(
            'label'     => esc_html__( 'Enable to apply the Typography', 'olivewp-plus'  ),
            'section'   => 'olivewp_footer_bar_typography',
            'type'      => 'toggle'
        )
    ));
    // Font family for the footer bar
    $wp_customize->add_setting('footer_bar_fontfamily',
        array(
            'default'           => 'Poppins',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('footer_bar_fontfamily', 
        array(
            'label'             => esc_html__('Font family', 'olivewp-plus' ),
            'section'           => 'olivewp_footer_bar_typography',
            'setting'           => 'footer_bar_fontfamily',
            'active_callback'   => 'olivewp_plus_footer_bar_typography_callback',
            'type'              => 'select',
            'choices'           => $font_family
    ));
    // Font style for the footer bar
    $wp_customize->add_setting('footer_bar_fontstyle',
        array(
            'default'           => 'normal',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('footer_bar_fontstyle', 
        array(
            'label'             => esc_html__('Font style', 'olivewp-plus' ),
            'section'           => 'olivewp_footer_bar_typography',
            'setting'           => 'footer_bar_fontstyle',
            'active_callback'   => 'olivewp_plus_footer_bar_typography_callback',
            'type'              => 'select',
            'choices'           => $font_style
    ));
    // Text transform for the footer bar
    $wp_customize->add_setting('footer_bar_text_transform',
        array(
            'default'           => 'none',
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control('footer_bar_text_transform', 
        array(
            'label'             => esc_html__('Text Transform', 'olivewp-plus' ),
            'section'           => 'olivewp_footer_bar_typography',
            'setting'           => 'footer_bar_text_transform',
            'active_callback'   => 'olivewp_plus_footer_bar_typography_callback',
            'type'              => 'select',
            'choices'           => $text_transform
    ));
    // Font size for the footer bar
    $wp_customize->add_setting( 'footer_bar_fontsize',
        array(
            'default'           => 18,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'footer_bar_fontsize',
        array(
            'label'             => esc_html__( 'Font size (px)', 'olivewp-plus'  ),
            'active_callback'   => 'olivewp_plus_footer_bar_typography_callback',
            'section'           => 'olivewp_footer_bar_typography',
            'setting'           => 'footer_bar_fontsize',
            'input_attrs'       => array(
                'min'   =>  8,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Line height for the footer bar
    $wp_customize->add_setting( 'footer_bar_line_height',
        array(
            'default'           => 29,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'footer_bar_line_height',
        array(
            'label'             => esc_html__( 'Line height (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_footer_bar_typography',
            'setting'           => 'footer_bar_line_height',
            'active_callback'   => 'olivewp_plus_footer_bar_typography_callback',
            'input_attrs'       => array(
                'min'   =>  1,
                'max'   =>  100,
                'step'  =>  1
            ),
        )
    ));
    // Font weight for the footer bar
    $wp_customize->add_setting( 'footer_bar_font_weight',
        array(
            'default'           => 400,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'absint'
        )
    );
    $wp_customize->add_control( new Olivewp_Slider_Custom_Control( $wp_customize, 'footer_bar_font_weight',
        array(
            'label'             => esc_html__( 'Font weight (px)', 'olivewp-plus'  ),
            'section'           => 'olivewp_footer_bar_typography',
            'setting'           => 'footer_bar_font_weight',
            'active_callback'   => 'olivewp_plus_footer_bar_typography_callback',
            'input_attrs'       => array(
                'min'   =>  100,
                'max'   =>  900,
                'step'  =>  100
            ),
        )
    ));

}

add_action( 'customize_register', 'olivewp_plus_typography_customizer' );