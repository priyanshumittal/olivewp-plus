<?php
/**
 * Blog Options Customizer
 *
 * @package OliveWP Plus
*/

function olivewp_plus_blog_customizer ( $wp_customize ) {

    // Blog Layout
    $wp_customize->add_setting('olivewp_plus_blog_layout_feature',
        array(
            'default'           =>  esc_html__('default','olivewp-plus'),
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('olivewp_plus_blog_layout_feature', 
        array(
            'label'             => esc_html__('Blog Layout','olivewp-plus' ),
            //'active_callback'   =>  'olivewp_plus_related_post_callback',
            'section'           => 'olivewp_blog_section',
            'setting'           => 'olivewp_plus_blog_layout_feature',
            'type'              => 'select',
            'priority'          => 4,
            'choices'           =>  
            array(
                'default'    =>  esc_html__('Default', 'olivewp-plus' ),
                'grid'       =>  esc_html__('Grid', 'olivewp-plus' ),
                'list'       =>  esc_html__('List', 'olivewp-plus' )
            )
        )
    );

    //Grid Style
    $wp_customize->add_setting('olivewp_plus_grid_style_feature',
        array(
            'default'           =>  esc_html__('default','olivewp-plus'),
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('olivewp_plus_grid_style_feature', 
        array(
            'label'             => esc_html__('Grid Style','olivewp-plus' ),
            //'active_callback'   =>  'olivewp_plus_related_post_callback',
            'section'           => 'olivewp_blog_section',
            'setting'           => 'olivewp_plus_grid_style_feature',
            'type'              => 'select',
            'priority'          => 4,
            'choices'           =>  
            array(
                'default'    =>  esc_html__('Default', 'olivewp-plus' ),
                'masonry'   =>  esc_html__('Masonry ', 'olivewp-plus' )
            )
         )
    );


    //Thumbnail Size
    $wp_customize->add_setting('olivewp_plus_thumbnail_size_feature',
        array(
            'default'           =>  esc_html__('full','olivewp-plus'),
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('olivewp_plus_thumbnail_size_feature', 
        array(
            'label'             => esc_html__('Thumbnail Size','olivewp-plus' ),
            //'active_callback'   =>  'olivewp_plus_related_post_callback',
            'section'           => 'olivewp_blog_section',
            'setting'           => 'olivewp_plus_thumbnail_size_feature',
            'type'              => 'select',
            'priority'          => 4,
            'choices'           =>  
            array(
                'thumbnail'    =>  esc_html__('Cropped ', 'olivewp-plus' ),
                'full'         =>  esc_html__('Full Image ', 'olivewp-plus' )
            )
        )
    );

    //Blog Column
    if ( class_exists( 'Olivewp_Plus_Customize_Control_Radio_Image' ) ) {
        $wp_customize->add_setting('olivewp_plus_blog_col_feature', array(
                'default'   =>  2
            )
        );

        $wp_customize->add_control(new Olivewp_Plus_Customize_Control_Radio_Image($wp_customize, 'olivewp_plus_blog_col_feature', 
            array(
                'label'             =>  esc_html__('Column', 'olivewp-plus' ),
                //'active_callback'   =>  'olivewp_plus_footer_widget_callback',
                'setting'           =>  'olivewp_plus_blog_col_feature',
                'section'           =>  'olivewp_blog_section',
                'priority'          =>  4,
                'choices'           =>  array(
                    2 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-widgets/2.png',
                    ),
                    3 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-widgets/3.png',

                    ),
                    4 => array(
                        'url' => trailingslashit( OLIVEWP_PLUGIN_URL ) . 'inc/customizer/assets/images/footer-widgets/4.png',
                        
                    ),
                )
            )
        ));
    }


    //Thumbnail Position
    $wp_customize->add_setting('olivewp_plus_thumbnail_pos_feature',
        array(
            'default'           =>  esc_html__('left','olivewp-plus'),
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('olivewp_plus_thumbnail_pos_feature', 
        array(
            'label'             => esc_html__('Thumbnail Position','olivewp-plus' ),
            //'active_callback'   =>  'olivewp_plus_related_post_callback',
            'section'           => 'olivewp_blog_section',
            'setting'           => 'olivewp_plus_thumbnail_pos_feature',
            'type'              => 'select',
            'priority'          => 4,
            'choices'           =>  
            array(
                'left'     =>  esc_html__('Left ', 'olivewp-plus' ),
                'right'    =>  esc_html__('Right ', 'olivewp-plus' )
            )
        )
    );




    //Thumbnail Style
    $wp_customize->add_setting('olivewp_plus_thumbnail_style_feature',
        array(
            'default'           =>  esc_html__('default','olivewp-plus'),
            'capability'        =>  'edit_theme_options',
            'sanitize_callback' =>  'olivewp_sanitize_select'
        )
    );
    $wp_customize->add_control('olivewp_plus_thumbnail_style_feature', 
        array(
            'label'             => esc_html__('Thumbnail Style','olivewp-plus' ),
            //'active_callback'   =>  'olivewp_plus_related_post_callback',
            'section'           => 'olivewp_blog_section',
            'setting'           => 'olivewp_plus_thumbnail_style_feature',
            'type'              => 'select',
            'priority'          => 4,
            'choices'           =>  
            array(
                'default'    =>  esc_html__('Default ', 'olivewp-plus' ),
                'round'      =>  esc_html__('Round ', 'olivewp-plus' )
            )
        )
    );

    /* ====================
    * Blog Excerpt Read More Button
    ==================== */
    $wp_customize->add_setting('blog_button_title', 
        array(
            'default'           => esc_html__('Read More','olivewp-plus' ),
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control( 'blog_button_title',
        array(
            'label'     => esc_html__('Read More Button Text','olivewp-plus' ),
            'section'   => 'olivewp_blog_section',
            'type'      => 'text',
            'priority'  => 4
        )
    );

    /* ====================
    * Meta Hide Show 
    ==================== */
    // enable/disable setting for date
/*    $wp_customize->add_setting('olivewp_plus_enable_post_date',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control($wp_customize, 'olivewp_plus_enable_post_date',
        array(
            'label'     => esc_html__('Hide/Show Date', 'olivewp-plus' ),
            'type'      => 'toggle',
            'section'   => 'olivewp_blog_section',
            'priority'  => 5
        )
    ));*/
    // enable/disable setting for category
   /* $wp_customize->add_setting('olivewp_plus_enable_category',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control($wp_customize, 'olivewp_plus_enable_category',
        array(
            'label'     => esc_html__('Hide/Show Category', 'olivewp-plus' ),
            'type'      => 'toggle',
            'section'   => 'olivewp_blog_section',
            'priority'  => 6
        )
    ));*/
    // enable/disable setting for comment
 /*   $wp_customize->add_setting('olivewp_plus_enable_post_comment',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control($wp_customize, 'olivewp_plus_enable_post_comment',
        array(
            'label'     => esc_html__('Hide/Show Comments', 'olivewp-plus' ),
            'type'      => 'toggle',
            'section'   => 'olivewp_blog_section',
            'priority'  => 7
        )
    ));*/
    // enable/disable setting for author
    $wp_customize->add_setting('olivewp_plus_enable_post_admin',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control($wp_customize, 'olivewp_plus_enable_post_admin',
        array(
            'label'     => esc_html__('Hide/Show Author', 'olivewp-plus' ),
            'type'      => 'toggle',
            'section'   => 'olivewp_blog_section',
            'priority'  => 8
        )
    ));
    // Background Color
     $wp_customize->add_setting('olivewp_plus_blog_bgcolor', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'olivewp_plus_blog_bgcolor', 
        array(
            'label'             =>  esc_html__('Background Color', 'olivewp-plus' ),
            //'active_callback'   =>  'olivewp_plus_after_menu_button_callback',
            'section'           =>  'olivewp_blog_section',
            'setting'           =>  'olivewp_plus_blog_bgcolor',
            'priority'  => 15
        )
    ));
   


    // Border
    class Olivewp_Plus_Blog_Border_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Border Width', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('blog_border_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Blog_Border_Customize_Control($wp_customize, 'blog_border_title', 
        array(
            'section'           =>  'olivewp_blog_section',
            //'active_callback'   =>  'olivewp_plus_topbar_widget_typography_callback',
            'setting'           =>  'blog_border_title',
             'priority'  => 15
        )
    ));
    // Border Top
    $wp_customize->add_setting( 'olivewp_plus_blog_border_top',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_blog_border_top',
        array(
            'label'         => esc_html__( 'Top', 'olivewp-plus'  ),
            'section'       => 'olivewp_blog_section',
            'type'          => 'number',
            'priority'      => 16,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

     // Border Right
    $wp_customize->add_setting( 'olivewp_plus_blog_border_right',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_blog_border_right',
        array(
            'label'         => esc_html__( 'Right', 'olivewp-plus'  ),
            'section'       => 'olivewp_blog_section',
            'type'          => 'number',
            'priority'      => 17,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    // Border Bottom
    $wp_customize->add_setting( 'olivewp_plus_blog_border_bottom',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_blog_border_bottom',
        array(
            'label'         => esc_html__( 'Bottom', 'olivewp-plus'  ),
            'section'       => 'olivewp_blog_section',
            'type'          => 'number',
            'priority'      => 18,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

   // Border Left
    $wp_customize->add_setting( 'olivewp_plus_blog_border_left',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_blog_border_left',
        array(
            'label'         => esc_html__( 'Left', 'olivewp-plus'  ),
            'section'       => 'olivewp_blog_section',
            'type'          => 'number',
            'priority'      => 19,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

    // Blog Border Color
     $wp_customize->add_setting('olivewp_plus_blog_border_color', 
        array(
            'default'           => '#ffffff',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );
    $wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'olivewp_plus_blog_border_color', 
        array(
            'label'             =>  esc_html__('Border Color', 'olivewp-plus' ),
            //'active_callback'   =>  'olivewp_plus_after_menu_button_callback',
            'section'           =>  'olivewp_blog_section',
            'setting'           =>  'olivewp_plus_blog_border_color',
            'priority'  => 20
        )
    ));



    //Blog Padding & Margin Toggle
    $wp_customize->add_setting('olivewp_plus_enable_margin_padding',
        array(
            'default'           => false,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control($wp_customize, 'olivewp_plus_enable_margin_padding',
        array(
            'label'     => esc_html__('Enable/Disable Margin , Padding', 'olivewp' ),
            'type'      => 'toggle',
            'section'   => 'olivewp_blog_section',
            'priority'  => 20
        )
    ));

    // Padding
    class Olivewp_Plus_Blog_Padding_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Padding', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('blog_padding_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Blog_Padding_Customize_Control($wp_customize, 'blog_padding_title', 
        array(
            'section'           =>  'olivewp_blog_section',
            'active_callback' => 'olivewp_plus_blog_margin_padding_callback',
            'setting'           =>  'blog_padding_title',
             'priority'  => 20
        )
    ));
    // Padding Top
    $wp_customize->add_setting( 'olivewp_plus_blog_padding_top',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_blog_padding_top',
        array(
            'label'         => esc_html__( 'Top', 'olivewp-plus'  ),
            'section'       => 'olivewp_blog_section',
            'active_callback' => 'olivewp_plus_blog_margin_padding_callback',
            'type'          => 'number',
            'priority'      => 21,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

     // Padding Right
    $wp_customize->add_setting( 'olivewp_plus_blog_padding_right',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_blog_padding_right',
        array(
            'label'         => esc_html__( 'Right', 'olivewp-plus'  ),
            'section'       => 'olivewp_blog_section',
            'active_callback' => 'olivewp_plus_blog_margin_padding_callback',
            'type'          => 'number',
            'priority'      => 22,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    // Padding Bottom
    $wp_customize->add_setting( 'olivewp_plus_blog_padding_bottom',
        array(
            'default'           => 8,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_blog_padding_bottom',
        array(
            'label'         => esc_html__( 'Bottom', 'olivewp-plus'  ),
            'section'       => 'olivewp_blog_section',
            'active_callback' => 'olivewp_plus_blog_margin_padding_callback',
            'type'          => 'number',
            'priority'      => 23,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

   // Padding Left
    $wp_customize->add_setting( 'olivewp_plus_blog_padding_left',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_blog_padding_left',
        array(
            'label'         => esc_html__( 'Left', 'olivewp-plus'  ),
            'section'       => 'olivewp_blog_section',
            'active_callback' => 'olivewp_plus_blog_margin_padding_callback',
            'type'          => 'number',
            'priority'      => 24,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    // Margin
    class Olivewp_Plus_Blog_Margin_Customize_Control extends WP_Customize_Control {
        public function render_content() { ?>
            <h3><?php esc_html_e('Margin', 'olivewp-plus' ); ?></h3>
        <?php }
    }
    $wp_customize->add_setting('blog_margin_title',
        array(
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_text'
        )
    );
    $wp_customize->add_control(new Olivewp_Plus_Blog_Margin_Customize_Control($wp_customize, 'blog_margin_title', 
        array(
            'section'           =>  'olivewp_blog_section',
            'active_callback' => 'olivewp_plus_blog_margin_padding_callback',
            'setting'           =>  'blog_margin_title',
             'priority'  => 24
        )
    ));
    // Margin Top
    $wp_customize->add_setting( 'olivewp_plus_blog_margin_top',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_blog_margin_top',
        array(
            'label'         => esc_html__( 'Top', 'olivewp-plus'  ),
            'section'       => 'olivewp_blog_section',
            'active_callback' => 'olivewp_plus_blog_margin_padding_callback',
            'type'          => 'number',
            'priority'      => 25,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

     // Border Right
    $wp_customize->add_setting( 'olivewp_plus_blog_margin_right',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_blog_margin_right',
        array(
            'label'         => esc_html__( 'Right', 'olivewp-plus'  ),
            'section'       => 'olivewp_blog_section',
            'active_callback' => 'olivewp_plus_blog_margin_padding_callback',
            'type'          => 'number',
            'priority'      => 26,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    // Border Bottom
    $wp_customize->add_setting( 'olivewp_plus_blog_margin_bottom',
        array(
            'default'           => 30,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_blog_margin_bottom',
        array(
            'label'         => esc_html__( 'Bottom', 'olivewp-plus'  ),
            'section'       => 'olivewp_blog_section',
            'active_callback' => 'olivewp_plus_blog_margin_padding_callback',
            'type'          => 'number',
            'priority'      => 27,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );

   // Border Left
    $wp_customize->add_setting( 'olivewp_plus_blog_margin_left',
        array(
            'default'           => 0,
            'capability'        => 'edit_theme_options',
            'sanitize_callback' => 'olivewp_sanitize_number_range'
        )
    );
    $wp_customize->add_control( 'olivewp_plus_blog_margin_left',
        array(
            'label'         => esc_html__( 'Left', 'olivewp-plus'  ),
            'section'       => 'olivewp_blog_section',
            'active_callback' => 'olivewp_plus_blog_margin_padding_callback',
            'type'          => 'number',
            'priority'      => 28,
            'input_attrs'   => 
            array( 
                'min' => 0, 
                'max' => 200, 
                'step' => 1, 
                'style' => 'width: 60px;' 
            )
        )
    );


    // enable/disable setting for Read more button
    $wp_customize->add_setting('olivewp_plus_enable_post_read_more',
        array(
            'default'           => true,
            'sanitize_callback' => 'olivewp_sanitize_checkbox'
        )
    );
    $wp_customize->add_control(new Olivewp_Toggle_Control($wp_customize, 'olivewp_plus_enable_post_read_more',
        array(
            'label'     => esc_html__('Hide/Show Read More Button', 'olivewp-plus' ),
            'type'      => 'toggle',
            'section'   => 'olivewp_blog_section',
            'priority'  => 9
        )
    ));

}
add_action( 'customize_register', 'olivewp_plus_blog_customizer' );