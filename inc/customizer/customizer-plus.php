<?php 
/**
 * OliveWP Customizer Controls
 *
 * @package OliveWP Plus
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'Olivewp_Plus_Customizer' ) ) :

	/**
	 * The OliveWP Customizer class
	*/
	class Olivewp_Plus_Customizer {

		/**
		 * Setup class
		*/
		public function __construct() {

			add_action( 'customize_register', 						array( $this, 'custom_controls' ) );
			add_action( 'customize_register', 						array( $this, 'controls_helpers' ) );
			add_action( 'after_setup_theme',  					    array( $this, 'register_options' ) );
			add_action( 'customize_controls_enqueue_scripts', 		array( $this, 'custom_customize_enqueue' ) );

		}


		/**
		 * Adds custom controls
		*/
		public function custom_controls( $wp_customize ) {

			// Load customize control classes
			require_once ( OLIVEWP_PLUGIN_DIR . '/inc/customizer/controls/customizer-theme-style/customizer-theme-style.php' );
			require_once ( OLIVEWP_PLUGIN_DIR . '/inc/customizer/controls/customizer-image-radio/customizer-image-radio-plus.php' );
			require_once ( OLIVEWP_PLUGIN_DIR . '/inc/customizer/controls/customizer-alpha-color-picker/class-customize-alpha-color-control.php' );

		}


		/**
		 * Adds customizer helpers
		*/
		public function controls_helpers() {

			require_once ( OLIVEWP_PLUGIN_DIR . '/inc/customizer/active-callback-plus.php' );
		}


		/**
		 * Adds customizer options
		*/
		public function register_options() {

			require_once ( OLIVEWP_PLUGIN_DIR . '/inc/customizer/settings/blog-options-plus.php' );
			require_once ( OLIVEWP_PLUGIN_DIR . '/inc/customizer/settings/single-blog-options-plus.php' );
			require_once ( OLIVEWP_PLUGIN_DIR . '/inc/customizer/settings/meta-plus.php' );
			require_once ( OLIVEWP_PLUGIN_DIR . '/inc/customizer/settings/theme-style-plus.php' );
			require_once ( OLIVEWP_PLUGIN_DIR . '/inc/customizer/settings/general-settings-plus.php' );
			require_once ( OLIVEWP_PLUGIN_DIR . '/inc/customizer/settings/color-background-plus.php' );
			require_once ( OLIVEWP_PLUGIN_DIR . '/inc/customizer/settings/typography-plus.php' );

		}


		/**
		 * Load scripts for customizer
		*/
		public function custom_customize_enqueue() {

			/* Enqueue the CSS files */
			wp_enqueue_style( 'olivewp-plus-customize-css', OLIVEWP_PLUGIN_URL .'/inc/customizer/assets/css/customize-plus.css' );

			/* Enqueue the JS files */
			wp_enqueue_script( 'olivewp-plus-customize-js', OLIVEWP_PLUGIN_URL .'/inc/customizer/assets/js/customize-plus.js', array( 'jquery' ) );

		}

	}

endif;

return new Olivewp_Plus_Customizer();