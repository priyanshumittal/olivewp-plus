<?php
/**
 * Active Callback for customizer settings
 *
 * @package OliveWP Plus
*/

// callback function for the related post
function olivewp_plus_related_post_callback($control) {
    if (true == $control->manager->get_setting('olivewp_plus_enable_related_post')->value()) {
        return true;
    } else {
        return false;
    }
}

// callback function for the preloader
function olivewp_plus_preloader_callback($control) {
    if (false == $control->manager->get_setting('preloader_enable')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for the search effects
function olivewp_plus_search_effects_callback($control) {
    if (false == $control->manager->get_setting('search_btn_enable')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for the breadcrumbs section
function olivewp_plus_breadcrumb_section_callback($control) {
    if (true == $control->manager->get_setting('breadcrumb_banner_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

// callback function for the breadcrumbs overlay color
function olivewp_plus_breadcrumb_overlay_callback($control) {
    if (true == $control->manager->get_setting('breadcrumb_overlay_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

// callback function for the scroll to top
function olivewp_plus_scroll_to_top_callback($control) {
    if (true == $control->manager->get_setting('scrolltotop_setting_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

// callback function for the scroll to top color
function olivewp_plus_scroll_to_top_color_callback($control) {
    if (true == $control->manager->get_setting('scroll_to_top_color_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

// callback function for the footer widget
function olivewp_plus_footer_widget_callback($control) {
    if (true == $control->manager->get_setting('footer_widget_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

// callback function for the footer widget overlay color
function olivewp_plus_footer_widget_overlay_color_callback($control) {
    if (true == $control->manager->get_setting('footer_widget_image_overlay_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

// callback function for the footer bar
function olivewp_plus_footer_bar_callback($control) {
    if (true == $control->manager->get_setting('footer_bar_enable')->value()) {
        return true;
    } else {
        return false;
    }
}

// callback function for the footer bar sections
function olivewp_plus_footer_bar_section_callback($control) {
    if ($control->manager->get_setting('footer_bar_layout')->value() == '1') {
        return false;
    }
    return true;
}

// callback function for the footer bar enable
function olivewp_plus_footer_bar_border_callback($control) {
    if (false == $control->manager->get_setting('footer_bar_border_enable')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for the topbar widget color
function olivewp_plus_topbar_widget_color_callback($control) {
    if (false == $control->manager->get_setting('enable_topbar_color')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for the header background color
function olivewp_plus_header_back_color_callback($control) {
    if (false == $control->manager->get_setting('enable_header_back_color')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for the after menu button
function olivewp_plus_after_menu_button_callback($control) {
    if (false == $control->manager->get_setting('enable_after_menu')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for the breadcrumb title color
function olivewp_plus_banner_bredcrumb_callback($control) {
    if (false == $control->manager->get_setting('enable_banner_color')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for the blog page/archive color
function olivewp_plus_blog_archive_page_callback($control) {
    if (false == $control->manager->get_setting('enable_blog_page_archive')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for the single post color
function olivewp_plus_single_post_callback($control) {
    if (false == $control->manager->get_setting('enable_single_post')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for footer bar color
function olivewp_plus_footer_bar_color_callback($control) {
    if (false == $control->manager->get_setting('enable_footer_bar')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for topbar widgets typography callback
function olivewp_plus_topbar_widget_typography_callback($control) {
    if (false == $control->manager->get_setting('enable_top_bar_typography')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for header typography callback
function olivewp_plus_header_typography_callback($control) {
    if (false == $control->manager->get_setting('enable_header_typography')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for after menu button typography callback
function olivewp_plus_after_menu_typography_callback($control) {
    if (false == $control->manager->get_setting('enable_after_btn_typography')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for banner typography callback
function olivewp_plus_banner_typography_callback($control) {
    if (false == $control->manager->get_setting('enable_banner_typography')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for Content(H1...H6) typography callback
function olivewp_plus_content_typography_callback($control) {
    if (false == $control->manager->get_setting('enable_content_typography')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for Blog/Archive/Single post typography callback
function olivewp_plus_post_typography_callback($control) {
    if (false == $control->manager->get_setting('enable_post_typography')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for post meta typography callback
function olivewp_plus_post_meta_typography_callback($control) {
    if (false == $control->manager->get_setting('enable_post_meta_typography')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for shop page typography callback
function olivewp_plus_shop_page_typography_callback($control) {
    if (false == $control->manager->get_setting('enable_shop_typography')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for sidebar typography callback
function olivewp_plus_sidebar_widget_typography_callback($control) {
    if (false == $control->manager->get_setting('enable_sidebar_typography')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for footer typography callback
function olivewp_plus_footer_widget_typography_callback($control) {
    if (false == $control->manager->get_setting('enable_footer_typography')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for footer bar callback
function olivewp_plus_footer_bar_typography_callback($control) {
    if (false == $control->manager->get_setting('enable_footer_bar_typography')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for breadcrumb image
function olivewp_plus_breadcrumb_callback($control) {
    if (false == $control->manager->get_setting('breadcrumb_image_enable')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for blog margin & padding
function olivewp_plus_blog_margin_padding_callback($control) {
    if (false == $control->manager->get_setting('olivewp_plus_enable_margin_padding')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for meta padding
function olivewp_plus_meta_padding_callback($control) {
    if (false == $control->manager->get_setting('olivewp_enable_meta_padding')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for meta margin
function olivewp_plus_meta_margin_callback($control) {
    if (false == $control->manager->get_setting('olivewp_enable_meta_margin')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for meta border
function olivewp_plus_meta_border_callback($control) {
    if (false == $control->manager->get_setting('olivewp_enable_meta_border')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for single post padding
function olivewp_plus_padding_single_post_callback($control) {
    if (false == $control->manager->get_setting('olivewp_plus_enable_padding_single_post')->value()) {
        return false;
    } else {
        return true;
    }
}

// callback function for single post margin
function olivewp_plus_margin_single_post_callback($control) {
    if (false == $control->manager->get_setting('olivewp_plus_enable_margin_single_post')->value()) {
        return false;
    } else {
        return true;
    }
}