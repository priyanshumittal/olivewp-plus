jQuery( document ).ready(function($) {
    // Change scroll to top button border radius
    wp.customize('scroll_to_top_button_radious', function(control) {

        control.bind(function( borderRadius ) {
            $('.scroll-up a').css('border-radius', borderRadius + 'px');  
        });

    });

    // Change height of Breadcrumb Header
    wp.customize('breadcrumb_height', function(control) {

        control.bind(function( headerHeight ) {
            $('section.page-title-section').css('height', headerHeight + 'px');  
        });

    });
});