(function($) {
    $( function() {
        wp.customize('after_menu_multiple_option', function(control) {
            control.bind(function( after_menu ) {
                if(after_menu=='menu_btn')
                {
                    $("#customize-control-after_menu_btn_txt").show();
                    $("#customize-control-after_menu_btn_link").show();
                    $("#customize-control-after_menu_btn_new_tabl").show();
                    $("#customize-control-after_menu_btn_border").show();
                    $("#customize-control-after_menu_html").hide();
                    $("#customize-control-after_menu_widget_area_section").hide();
                }
                else if(after_menu=='html')
                {
                    $("#customize-control-after_menu_btn_txt").hide();
                    $("#customize-control-after_menu_btn_link").hide();
                    $("#customize-control-after_menu_btn_new_tabl").hide();
                    $("#customize-control-after_menu_btn_border").hide();
                    $("#customize-control-after_menu_widget_area_section").hide();
                    $("#customize-control-after_menu_html").show(); 
                }
                else if(after_menu=='top_menu_widget')
                {
                    $("#customize-control-after_menu_btn_txt").hide();
                    $("#customize-control-after_menu_btn_link").hide();
                    $("#customize-control-after_menu_btn_new_tabl").hide();
                    $("#customize-control-after_menu_btn_border").hide();
                    $("#customize-control-after_menu_html").hide(); 
                    $("#customize-control-after_menu_widget_area_section").show();
                }
                else
                {
                    $("#customize-control-after_menu_btn_txt").hide();
                    $("#customize-control-after_menu_btn_link").hide();
                    $("#customize-control-after_menu_btn_new_tabl").hide();
                    $("#customize-control-after_menu_btn_border").hide();
                    $("#customize-control-after_menu_html").hide(); 
                    $("#customize-control-after_menu_widget_area_section").hide();
                }
            });
        });

        wp.customize('banner_height_option', function(control) {
            control.bind(function( banner_height ) {
                if(banner_height=='desktop')
                {
                    $("#customize-control-banner_height_desktop").show();
                    $("#customize-control-banner_height_ipad").hide();
                    $("#customize-control-banner_height_mobile").hide();

                }
                else if(banner_height=='ipad')
                {
                    $("#customize-control-banner_height_desktop").hide();
                    $("#customize-control-banner_height_ipad").show();
                    $("#customize-control-banner_height_mobile").hide();
                }
                else if(banner_height=='mobile')
                {
                    $("#customize-control-banner_height_desktop").hide();
                    $("#customize-control-banner_height_ipad").hide();
                    $("#customize-control-banner_height_mobile").show();
                }
                else
                {
                    $("#customize-control-banner_height_desktop").show();
                    $("#customize-control-banner_height_ipad").hide();
                    $("#customize-control-banner_height_mobile").hide();
                }
            });
        });

        if (($('#_customize-input-footer_bar_section1').val() == "custom_text")) {

            $('#customize-control-footer_bar_section1_copyright').show();
        } 
        else {

            $('#customize-control-footer_bar_section1_copyright').hide();
        }
        if (($('#_customize-input-footer_bar_section2').val() == "custom_text")) {

            $('#customize-control-footer_bar_section2_copyright').show();
        } 
        else {
            $('#customize-control-footer_bar_section2_copyright').hide();
        }

        wp.customize('footer_bar_section1', function (value) {
            value.bind(function (newval) {
                if (newval == "custom_text") {
                    $('#customize-control-footer_bar_section1_copyright').show();
                } else {
                    $('#customize-control-footer_bar_section1_copyright').hide();
                }
            });
        });
        wp.customize('footer_bar_section2', function (value) {
            value.bind(function (newval) {
                if (newval == "custom_text") {
                    $('#customize-control-footer_bar_section2_copyright').show();
                } 
                else {
                    $('#customize-control-footer_bar_section2_copyright').hide();
                }
            });
        });

        wp.customize('olivewp_plus_blog_layout_feature', function (value) {  
            value.bind(function (blg_feature) {
               if (blg_feature == "default") {
                    $("#customize-control-olivewp_blog_post_order").show();
                    $('#customize-control-olivewp_plus_grid_style_feature').hide();
                    $('#customize-control-olivewp_plus_thumbnail_size_feature').hide();
                    $('#customize-control-olivewp_plus_blog_col_feature').hide();
                    $('#customize-control-olivewp_plus_thumbnail_pos_feature').hide();
                    $('#customize-control-olivewp_plus_thumbnail_style_feature').hide(); 
                }
                else if(blg_feature=='grid')
                {
                    $('#customize-control-olivewp_plus_grid_style_feature').show();
                    $("#customize-control-olivewp_blog_post_order").show();
                    $('#customize-control-olivewp_plus_thumbnail_size_feature').show();
                    $('#customize-control-olivewp_plus_blog_col_feature').show();
                    $('#customize-control-olivewp_plus_thumbnail_pos_feature').hide();
                    $('#customize-control-olivewp_plus_thumbnail_style_feature').hide();                     
                }
                else
                {
                    $('#customize-control-olivewp_plus_grid_style_feature').hide();
                    $("#customize-control-olivewp_blog_post_order").hide();
                    $('#customize-control-olivewp_plus_thumbnail_size_feature').hide();
                    $('#customize-control-olivewp_plus_blog_col_feature').hide();
                    $('#customize-control-olivewp_plus_thumbnail_pos_feature').show();
                    $('#customize-control-olivewp_plus_thumbnail_style_feature').show();     

                }
            });
        });
    });
})(jQuery)