<?php
if ( class_exists( 'WP_Customize_Control' ) ) {
    
    class Olivewp_Plus_Customize_Control_Radio_Image extends WP_Customize_Control {

        /**
         * The type of customize control being rendered.
         *
         * @since 1.1.24
         * @var   string
         */
        public $type = 'radio-image';

        /**
         * Displays the control content.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function render_content() {
            /* If no choices are provided, bail. */
            if (empty($this->choices)) {
                return;
            }
            ?>
            <?php if (!empty($this->label)) : ?>
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
            <?php endif; ?>
            <?php if (!empty($this->description)) : ?>
                <span class="description customize-control-description"><?php echo $this->description; ?></span>
            <?php endif; ?>
            <div id="<?php echo esc_attr("input_{$this->id}"); ?>">
                <?php foreach ($this->choices as $value => $args) : ?>
                    <input type="radio" value="<?php echo esc_attr($value); ?>" name="<?php echo esc_attr("_customize-radio-{$this->id}"); ?>" id="<?php echo esc_attr("{$this->id}-{$value}"); ?>" <?php $this->link(); ?> <?php checked($this->value(), $value); ?> />
                    <label for="<?php echo esc_attr("{$this->id}-{$value}"); ?>" class="<?php echo esc_attr("{$this->id}-{$value}"); ?>">
                        <?php if (!empty($args['label'])) : ?>
                            <span class="screen-reader-text"><?php echo esc_html($args['label']); ?></span>
                        <?php endif; ?>
                        <img class="wp-ui-highlight" src="<?php echo esc_url(sprintf($args['url'], OLIVEWP_PLUGIN_URL, get_stylesheet_directory_uri())); ?>" <?php if (!empty($args['label'])) : echo 'alt="' . esc_attr($args['label']) . '"'; endif; ?> />
                    </label>
                <?php endforeach; ?>
            </div><!-- .image -->
            <script type="text/javascript">
                jQuery(document).ready(function () {    
                    jQuery('#<?php echo esc_attr("input_{$this->id}"); ?>').buttonset();
                    if(jQuery("#_customize-input-after_menu_multiple_option").val()=='menu_btn')
                    {
                        jQuery("#customize-control-after_menu_btn_txt").show();
                        jQuery("#customize-control-after_menu_btn_link").show();
                        jQuery("#customize-control-after_menu_btn_new_tabl").show();
                        jQuery("#customize-control-after_menu_btn_border").show();
                        jQuery("#customize-control-after_menu_html").hide();  
                        jQuery("#customize-control-after_menu_widget_area_section").hide();
                    }
                    else if(jQuery("#_customize-input-after_menu_multiple_option").val()=='html')
                    {
                        jQuery("#customize-control-after_menu_btn_txt").hide();
                        jQuery("#customize-control-after_menu_btn_link").hide();
                        jQuery("#customize-control-after_menu_btn_new_tabl").hide();
                        jQuery("#customize-control-after_menu_btn_border").hide();
                        jQuery("#customize-control-after_menu_widget_area_section").hide();
                        jQuery("#customize-control-after_menu_html").show(); 
                    }
                    else if(jQuery("#_customize-input-after_menu_multiple_option").val()=='top_menu_widget')
                    {

                        jQuery("#customize-control-after_menu_btn_txt").hide();
                        jQuery("#customize-control-after_menu_btn_link").hide();
                        jQuery("#customize-control-after_menu_btn_new_tabl").hide();
                        jQuery("#customize-control-after_menu_btn_border").hide();
                        jQuery("#customize-control-after_menu_html").hide();
                        jQuery("#customize-control-after_menu_widget_area_section").show();
                    }
                    else
                    {
                        jQuery("#customize-control-after_menu_btn_txt").hide();
                        jQuery("#customize-control-after_menu_btn_link").hide();
                        jQuery("#customize-control-after_menu_btn_new_tabl").hide();
                        jQuery("#customize-control-after_menu_btn_border").hide();
                        jQuery("#customize-control-after_menu_html").hide();
                        jQuery("#customize-control-after_menu_widget_area_section").hide();
                    }



                    if(jQuery("#_customize-input-olivewp_plus_blog_layout_feature").val()=='default')
                    {   
                        jQuery("#customize-control-olivewp_blog_post_order").show();
                        jQuery('#customize-control-olivewp_plus_grid_style_feature').hide();
                        jQuery('#customize-control-olivewp_plus_thumbnail_size_feature').hide();
                        jQuery('#customize-control-olivewp_plus_blog_col_feature').hide();
                        jQuery('#customize-control-olivewp_plus_thumbnail_pos_feature').hide();
                        jQuery('#customize-control-olivewp_plus_thumbnail_style_feature').hide(); 
                    }
                    else if(jQuery("#_customize-input-olivewp_plus_blog_layout_feature").val()=='grid')
                    {
                       jQuery('#customize-control-olivewp_plus_grid_style_feature').show();
                       jQuery("#customize-control-olivewp_blog_post_order").show();
                       jQuery('#customize-control-olivewp_plus_thumbnail_size_feature').show();
                       jQuery('#customize-control-olivewp_plus_blog_col_feature').show();
                       jQuery('#customize-control-olivewp_plus_thumbnail_pos_feature').hide();
                       jQuery('#customize-control-olivewp_plus_thumbnail_style_feature').hide();
                    }
                    else
                    {
                        jQuery('#customize-control-olivewp_plus_grid_style_feature').hide();
                        jQuery("#customize-control-olivewp_blog_post_order").hide();
                        jQuery('#customize-control-olivewp_plus_thumbnail_size_feature').hide();
                        jQuery('#customize-control-olivewp_plus_blog_col_feature').hide();
                        jQuery('#customize-control-olivewp_plus_thumbnail_pos_feature').show();
                        jQuery('#customize-control-olivewp_plus_thumbnail_style_feature').show();                        
                    }    
                });
            </script>
        <?php
        }
           
        /**
         * Loads the $ UI Button script and hooks our custom styles in.
         *
         * @since  1.1.24
         * @access public
         * @return void
         */
        public function enqueue() {
            wp_enqueue_script('jquery-ui-button');
            wp_enqueue_style( 'olivewp-radio-image-controls-css', OLIVEWP_PLUGIN_URL . '/inc/customizer/controls/customizer-image-radio/css/customizer-image-radio.css', array(), '1.0', 'all' );
        }

    }
}