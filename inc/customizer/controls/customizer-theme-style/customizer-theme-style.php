<?php
/**
 * Customizer Control: Theme Style Layout.
*/
// Predefined Default Background
class Olivewp_Plus_Pre_Customize_Control extends WP_Customize_Control {

    public $type = 'new_menu';

    function render_content() {
        echo '<h3>' . __('Predefined Background Images', 'olivewp-plus') . '</h3>';
        $name = '_customize-image-radio-' . $this->id;
        $i = 1;
        foreach ($this->choices as $key => $value) {
            ?>
            <label>
                <input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr($name); ?>" data-customize-setting-link="<?php echo esc_attr($this->id); ?>" <?php if ($this->value() == $key) { echo 'checked'; } ?>>
                    <img <?php if ($this->value() == $key) { echo 'class="color_scheme_active"'; } ?> src="<?php echo OLIVEWP_PLUGIN_URL.'/inc/customizer/assets/images/bg-pattern/'.$value; ?>" alt="<?php echo esc_attr($value); ?>" />
            </label>
            <?php
            if ($i == 4) {
                echo '<p></p>';
                $i = 0;
            }
            $i++;
        }
        ?>
        <h3><?php esc_html_e('Background Image', 'olivewp-plus'); ?></h3>
        <p><?php esc_html_e('Go to', 'olivewp-plus'); ?> => <?php esc_html_e('Appearance', 'olivewp-plus'); ?> => <?php esc_html_e('Customize', 'olivewp-plus'); ?> => <?php esc_html_e('Colors & Background', 'olivewp-plus'); ?> => <?php esc_html_e('Background Image', 'olivewp-plus'); ?></p><br/>
        <h3><?php esc_html_e('Background Color', 'olivewp-plus'); ?></h3>
        <p> <?php esc_html_e('Go to', 'olivewp-plus'); ?> => <?php esc_html_e('Appearance', 'olivewp-plus'); ?> => <?php esc_html_e('Customize', 'olivewp-plus'); ?> => <?php esc_html_e('Colors & Background', 'olivewp-plus'); ?> => <?php esc_html_e('Background Color', 'olivewp-plus'); ?> </p>
        <script>
            jQuery(document).ready(function ($) {
                $("#customize-control-predefined_back_image label img").click(function () {
                    $("#customize-control-predefined_back_image label img").removeClass("color_scheme_active");
                    $(this).addClass("color_scheme_active");
                });
            });
        </script>
        <?php
    }

}



// Layout Style
class Olivewp_Plus_Style_Layout_Customize_Control extends WP_Customize_Control {

    public $type = 'new_menu';

    function render_content() {
        echo '<h3>', __('Theme Layout', 'olivewp-plus') . '</h3>';
        $name = '_customize-layout-radio-' . $this->id;
        foreach ($this->choices as $key => $value) { ?>
            <label>
                <input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr($name); ?>" data-customize-setting-link="<?php echo esc_attr($this->id); ?>" <?php if ($this->value() == $key) { echo 'checked'; } ?>>
                    <img <?php if ($this->value() == $key) { echo 'class="color_scheme_active"'; } ?> src="<?php echo OLIVEWP_PLUGIN_URL.'/inc/customizer/assets/images/bg-pattern/'.$value;?>" alt="<?php echo esc_attr($value); ?>" />
            </label>
        <?php }
        ?>
        <script>
            jQuery(document).ready(function ($) {
                $("#customize-control-olivewp_layout_style label img").click(function () {
                    $("#customize-control-olivewp_layout_style label img").removeClass("color_scheme_active");
                    $(this).addClass("color_scheme_active");
                });
            });
        </script>
        <?php
    }

}



// Theme color
class Olivewp_Plus_Color_Customize_Control extends WP_Customize_Control {

    public $type = 'new_menu';

    function render_content() {
        echo '<h3>' . __('Predefined Colors', 'olivewp-plus') . '</h3>';
        $name = '_customize-color-radio-' . $this->id;
        foreach ($this->choices as $key => $value) { ?>
            <label>
                <input type="radio" value="<?php echo $key; ?>" name="<?php echo esc_attr($name); ?>" data-customize-setting-link="<?php echo esc_attr($this->id); ?>" <?php if ($this->value() == $key) { echo 'checked="checked"'; } ?>>
                <img <?php if ($this->value() == $key) {  echo 'class="color_scheme_active"'; } ?> src="<?php echo OLIVEWP_PLUGIN_URL.'/inc/customizer/assets/images/bg-pattern/'.$value; ?>" alt="<?php echo esc_attr($value); ?>" />
            </label>
        <?php }
        ?>
        <script>
            jQuery(document).ready(function ($) {
                $("#customize-control-theme_color label img").click(function () {
                    $("#customize-control-theme_color label img").removeClass("color_scheme_active");
                    $(this).addClass("color_scheme_active");
                });
            });
        </script>
        <?php
    }

}