<?php
/**
 * Getting started template
 */
$olivewp_plus_name = wp_get_theme();
?>
<div id="getting_started" class="olivewp-plus-tab-pane active">
	<div class="spice-container">
		<div class="spice-row">
			<div class="spice-col-1">
				<h1 class="olivewp-plus-info-title text-center"><?php 
				/* translators: %s: theme name */
				printf( esc_html__('%s Plus Configuration','olivewp-plus'), $olivewp_plus_name ); ?><?php if( !empty($olivewp_plus_name['Version']) ): ?> <sup id="olivewp-plus-theme-version"><?php echo esc_html( $olivewp_plus_name['Version'] ); ?> </sup><?php endif; ?></h1>
			</div>
		</div>
		<div class="spice-row" style="margin-top: 20px;">

			<div class="spice-col-1">
			    <div class="olivewp-plus-page" style="border: none;box-shadow: none;">
					<div class="mockup">
			    		<img src="<?php echo OLIVEWP_PLUGIN_URL .'inc/admin/assets/img/mockup-lite.png';?>"  width="100%">
			    	</div>
				</div>	
			</div>	

		</div>
		
		<div class="spice-row" style="margin-top: 20px;">

			<div class="spice-col-3"> 
				
				<div class="olivewp-plus-page">
					<div class="olivewp-plus-page-top"><?php esc_html_e( 'Links to Customizer Settings', 'olivewp-plus' ); ?></div>
					<div class="olivewp-plus-page-content">
						<ul class="olivewp-plus-page-list-flex">
							<li>
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('customize.php?autofocus[section]=title_tagline'); ?>" target="_blank"><?php echo esc_html__('Site Logo','olivewp-plus'); ?></a>
							</li>
							<li>
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('customize.php?autofocus[panel]=olivewp_theme_panel'); ?>" target="_blank"><?php echo esc_html__('Blog options','olivewp-plus'); ?></a>
							</li>

							<li>
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('customize.php?autofocus[panel]=nav_menus'); ?>" target="_blank"><?php echo esc_html__('Menus','olivewp-plus'); ?></a>
							</li>
														
						    <li> 
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('customize.php?autofocus[section]=theme_style'); ?>" target="_blank"><?php echo esc_html__('Layout & Color scheme','olivewp-plus'); ?></a>
							</li>

							<li>
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('customize.php?autofocus[panel]=widgets'); ?>" target="_blank"><?php echo esc_html__('Widgets','olivewp-plus'); ?></a>
							</li>
							
							<li>
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('customize.php?autofocus[panel]=olivewp_general_settings'); ?>" target="_blank"><?php echo esc_html__('General Settings','olivewp-plus'); ?></a><?php esc_html_e(" ( Preloader, After Menu, Header Presets, Sticky Header, Post Navigation Styles)","olivewp-plus"); ?>
							</li>
							
						    <li> 
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('customize.php?autofocus[panel]=olivewp_plus_typography_setting'); ?>" target="_blank"><?php echo esc_html__('Typography','olivewp-plus'); ?></a>
							</li> 

							<li> 
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('customize.php?autofocus[panel]=colors_back_settings'); ?>" target="_blank"><?php echo esc_html__('Colors & Background','olivewp-plus'); ?></a>
							</li>  
						</ul>
					</div>
				</div>

			</div>

			<div class="spice-col-3"> 
				
				<div class="olivewp-plus-page">
					<div class="olivewp-plus-page-top"><?php esc_html_e( 'Useful Links', 'olivewp-plus' ); ?></div>
					<div class="olivewp-plus-page-content">
						<ul class="olivewp-plus-page-list-flex">
							<li>
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('https://olivewp.org/starter-sites/'); ?>" target="_blank"><?php echo esc_html__('Starter Sites','olivewp-plus'); ?></a>
							</li>
							<li>
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('https://olivewp.org/'); ?>" target="_blank"><?php 
									/* translators: %s: theme name */
									printf( esc_html__('%s Plus Details','olivewp-plus'), $olivewp_plus_name ); ?></a>
							</li>

							<li>
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('https://users.freemius.com/login'); ?>" target="_blank"><?php 
									/* translators: %s: theme name */
									printf( esc_html__('%s Plus Support','olivewp-plus'), $olivewp_plus_name ); ?></a>
							</li>
														
						    <li> 
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('https://wordpress.org/support/theme/olivewp/reviews/#new-post'); ?>" target="_blank"><?php echo esc_html__('Your feedback is valuable to us','olivewp-plus'); ?></a>
							</li>
							
							<li>
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('https://olivewp.org/docs/'); ?>" target="_blank"><?php 
									/* translators: %s: theme name */
									printf( esc_html__('%s Plus Documentation','olivewp-plus'), $olivewp_plus_name ); ?></a>
							</li>

							<li> 
								<a class="olivewp-plus-page-quick-setting-link" href="<?php echo esc_url('https://olivewp.org/olivewp-plus-changelog/'); ?>" target="_blank"><?php echo esc_html__('Changelog','olivewp-plus'); ?></a>
							</li> 
						</ul>
					</div>
				</div>

			</div>		
		</div>
	</div>
</div>