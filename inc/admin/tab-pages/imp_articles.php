<?php
/**
 * Important Articles
 */
?>
<div id="imp_articles" class="olivewp-plus-tab-pane panel-close">
	<div class="spice-container">

		<div class="spice-row">
			<div class="spice-col-1">
				<h1 class="olivewp-plus-info-title text-center">
					<?php echo esc_html__('Important Articles','olivewp-plus'); ?>
				</h1>
			</div>
		</div>

		<div class="spice-row" style="margin-top: 20px;">
			<div class="spice-col-1">
			    <div class="olivewp-plus-page" style="border: none;box-shadow: none;">
					<div class="olivewp-plus-imp-articles">1.  
						<a target="_blank" href="https://olivewp.org/docs/getting-started/how-to-download-and-install-the-plugin/"><?php echo esc_html__('How to install the Plus Plugin','olivewp-plus'); ?></a>
					</div>

					<div class="olivewp-plus-imp-articles">2. 
						<a target="_blank" href="https://olivewp.org/docs/getting-started/how-to-import-demo-data/"><?php echo esc_html__('How to import Demo Data','olivewp-plus'); ?>  </a>
					</div>

					<div class="olivewp-plus-imp-articles">3. 
						<a target="_blank" href="https://olivewp.org/docs/customization/"><?php echo esc_html__('How to customize the theme','olivewp-plus'); ?></a>
					</div>
				</div>	
			</div>	

			<a href="https://olivewp.org/docs/" target="_blank" class="button button-primary button-hero"><?php echo esc_html__('More Help Articles','olivewp-plus'); ?></a>

		</div>

	</div>
</div>