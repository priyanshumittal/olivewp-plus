<?php
/**
 * Use typography settings in the theme package
 *
 * @package OliveWP Plus
*/
function olivewp_plus_typography_custom_css() {
    // Get values from the customizer settings
    $olivewp_plus_enable_top_bar_typography        =   get_theme_mod('enable_top_bar_typography', false);
    $olivewp_plus_enable_header_typo               =   get_theme_mod('enable_header_typography', false);
    $olivewp_plus_enable_after_btn_typography      =   get_theme_mod('enable_after_btn_typography', false);
    $olivewp_plus_enable_banner_typography         =   get_theme_mod('enable_banner_typography', false);
    $olivewp_plus_enable_content_typography        =   get_theme_mod('enable_content_typography', false);
    $olivewp_plus_enable_post_typography           =   get_theme_mod('enable_post_typography', false);
    $olivewp_plus_enable_post_meta_typography      =   get_theme_mod('enable_post_meta_typography', false);
    $olivewp_plus_enable_shop_typography           =   get_theme_mod('enable_shop_typography', false);
    $olivewp_plus_enable_sidebar_typography        =   get_theme_mod('enable_sidebar_typography', false);
    $olivewp_enable_footer_typography              =   get_theme_mod('enable_footer_typography', false);
    $olivewp_enable_footer_bar_typography          =   get_theme_mod('enable_footer_bar_typography', false);

    /* ====================
        * Topbar 
    ==================== */
    if($olivewp_plus_enable_top_bar_typography == true) { ?>
        <style>
            body .spice-topbar .wp-block-search .wp-block-search__label, body .spice-topbar .widget.widget_block h1, body .spice-topbar .widget.widget_block h2, body .spice-topbar .widget.widget_block h3, body .spice-topbar .widget.widget_block h4, body .spice-topbar .widget.widget_block h5, body .spice-topbar .widget.widget_block h6 {
                font-family: '<?php echo esc_attr( get_theme_mod('topbar_widget_title_fontfamily','Poppins') );?>';
                font-style: <?php echo esc_attr( get_theme_mod('topbar_widget_title_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('topbar_widget_title_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('topbar_widget_title_fontsize', '30') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('topbar_widget_title_line_height','45') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('topbar_widget_title_font_weight','700') ) ?> ;
            }
            body .spice-topbar .widget p,  body .spice-topbar .widget a, body .spice-topbar .widget p a, body .spice-topbar .widget ul li {
                font-family: '<?php echo esc_attr( get_theme_mod('topbar_widget_content_fontfamily','Poppins') );?>';
                font-style: <?php echo esc_attr( get_theme_mod('topbar_widget_content_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('topbar_widget_content_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('topbar_widget_content_fontsize', '16') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('topbar_widget_content_line_height','29') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('topbar_widget_content_font_weight','400') ); ?> ;
            }
        </style>
    <?php }


    /* ====================
        * Header section (Site title, Tagline, Menu, Submenu) 
    ==================== */
    if($olivewp_plus_enable_header_typo == true) { ?>
        <style>
            body .site-title {
                font-family: '<?php echo esc_attr( get_theme_mod('site_title_fontfamily','Poppins') );?>';
                font-style: <?php echo esc_attr( get_theme_mod('site_title_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('site_title_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('site_title_fontsize', '30') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('site_title_line_height','39') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('site_title_font_weight','600') ); ?> ;
            }
            body .site-description {
                font-family: '<?php echo esc_attr( get_theme_mod('tagline_fontfamily','Poppins') );?>';
                font-style: <?php echo esc_attr( get_theme_mod('tagline_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('tagline_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('tagline_fontsize', '18') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('tagline_line_height','29') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('tagline_font_weight','400') ); ?> ;
            }
            body .spice-nav > li.parent-menu a {
                font-family: '<?php echo esc_attr( get_theme_mod('menu_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('menu_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('menu_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('menu_fontsize', '14') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('menu_line_height','22') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('menu_font_weight','600') ); ?> ;
            }
            body .spice-nav .dropdown-menu > li a {
                font-family: '<?php echo esc_attr( get_theme_mod('submenu_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('submenu_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('submenu_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('submenu_fontsize', '14') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('submenu_line_height','20') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('submenu_font_weight','600') ); ?> ;
            }
        </style>
    <?php }


    /* ====================
        * After Menu 
    ==================== */
    if($olivewp_plus_enable_after_btn_typography == true) { ?>
        <style>
            body .spice.spice-custom .header-button a {
                font-family: '<?php echo esc_attr( get_theme_mod('after_btn_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('after_btn_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('after_btn_text_transform','uppercase') );?> ;
                font-size: <?php echo intval( get_theme_mod('after_btn_fontsize', '14') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('after_btn_line_height','22') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('after_btn_font_weight','600') ) ?> ;
            }
        </style>
    <?php }


    /* ====================
        * Breadcrumb Banner 
    ==================== */
    if($olivewp_plus_enable_banner_typography == true) { ?>
        <style>
            body .page-title h1, body .page-title.content-area-title h1 {
                font-family: '<?php echo esc_attr( get_theme_mod('banner_page_title_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('banner_page_title_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('banner_page_title_text_transform','capitalize') );?> ;
                font-size: <?php echo intval( get_theme_mod('banner_page_title_fontsize', '42') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('banner_page_title_line_height','63') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('banner_page_title_font_weight','700') ) ?> ;
            }
            body .page-breadcrumb span, .rank-math-breadcrumb span, .rank-math-breadcrumb p, .navxt-breadcrumb a , .navxt-breadcrumb span,.navxt-breadcrumb  {
                font-family: '<?php echo esc_attr( get_theme_mod('banner_breadcrumb_title_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('banner_breadcrumb_title_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('banner_breadcrumb_title_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('banner_breadcrumb_title_fontsize', '16') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('banner_breadcrumb_title_line_height','20') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('banner_breadcrumb_title_font_weight','400') ) ?> ;
            }
        </style>
    <?php }


    /* ====================
        * Content(H1----H6, Paragraph, Button) 
    ==================== */
    if($olivewp_plus_enable_content_typography == true) { ?>
        <style>
            body .entry-content h1, body .page-section-full h1 {
                font-family: '<?php echo esc_attr( get_theme_mod('content_h1_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('content_h1_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('content_h1_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('content_h1_fontsize', '42') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('content_h1_line_height','63') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('content_h1_font_weight','700') ) ?> ;
            }
            body .entry-content h2:not(.woocommerce-page h2), body .page-section-full h2 {
                font-family: '<?php echo esc_attr( get_theme_mod('content_h2_fontfamily','Poppins') );?>';
                font-style: <?php echo esc_attr( get_theme_mod('content_h2_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('content_h2_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('content_h2_fontsize', '30') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('content_h2_line_height','45') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('content_h2_font_weight','700') ) ?> ;
            }
            body .entry-content h3:not(.woocommerce-page h3), body .page-section-full h3 {
                font-family: '<?php echo esc_attr( get_theme_mod('content_h3_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('content_h3_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('content_h3_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('content_h3_fontsize', '24') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('content_h3_line_height','36') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('content_h3_font_weight','700') ) ?> ;
            }
            body .entry-content h4, body .page-section-full h4 {
                font-family: '<?php echo esc_attr( get_theme_mod('content_h4_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('content_h4_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('content_h4_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('content_h4_fontsize', '20') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('content_h4_line_height','30') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('content_h4_font_weight','700') ) ?> ;
            }
            body .entry-content h5, body .page-section-full h5 {
                font-family: '<?php echo esc_attr( get_theme_mod('content_h5_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('content_h5_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('content_h5_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('content_h5_fontsize', '18') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('content_h5_line_height','27') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('content_h5_font_weight','700') ) ?> ;
            }
            body .entry-content h6, body .page-section-full h6 {
                font-family: '<?php echo esc_attr( get_theme_mod('content_h6_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('content_h6_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('content_h6_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('content_h6_fontsize', '16') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('content_h6_line_height','24') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('content_h6_font_weight','700') ) ?> ;
            }
            body .entry-content p, .blog-author p, body .comment-section p, body .comment-form p, body .woocommerce-product-details__short-description p, body .page-section-full p, body input, body textarea, body select, body.woocommerce-page p:not(body.woocommerce-page .site-footer p) {
                font-family: '<?php echo esc_attr( get_theme_mod('content_p_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('content_p_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('content_p_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('content_p_fontsize', '18') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('content_p_line_height','29') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('content_p_font_weight','400') ) ?> ;
            }
            body form.search-form input.search-submit, body input[type="submit"], body button[type="submit"], body .wp-block-button__link, body .woocommerce div.product form.cart .button, body.woocommerce ul.products li.product .button, body .woocommerce .cart .button, body .cart_totals  .wc-proceed-to-checkout a.checkout-button, body .woocommerce #payment #place_order, body .post .entry-content .more-link, body .page-section-full .wp-block-button__link, body.woocommerce-page ul.products li.product .button {
                font-family: '<?php echo esc_attr( get_theme_mod('content_button_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('content_button_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('content_button_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('content_button_fontsize', '14') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('content_button_line_height','14') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('content_button_font_weight','600') ) ?> ;
            }
            body .post .entry-content .more-link, body .wpcf7 input[type="submit"], body .comment-form input[type="submit"], body .wp-block-button__link {
                border-radius: <?php echo intval( get_theme_mod('content_button_border_radius','4') ).'px'; ?> ;
                padding-top: <?php echo intval( get_theme_mod('content_button_padtop','8') ).'px'; ?> ;
                padding-bottom: <?php echo intval( get_theme_mod('content_button_padbottom','8') ).'px'; ?> ;
                padding-left: <?php echo intval( get_theme_mod('content_button_padleft','15') ).'px'; ?> ;
                padding-right: <?php echo intval( get_theme_mod('content_button_padright','15') ).'px'; ?> ;
            }
        </style>
    <?php }


    /* ====================
        * Blog/Archive/Single Post
    ==================== */
    if($olivewp_plus_enable_post_typography == true) { ?>
        <style>
            body .post-content .entry-header h3.entry-title,
            body .entry-header h3.entry-title {
                font-family: '<?php echo esc_attr( get_theme_mod('post_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('post_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('post_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('post_fontsize', '24') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('post_line_height','36') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('post_font_weight','600') ) ?> ;
            }
        </style>
    <?php }


    /* ====================
        * Post Meta
    ==================== */
    if($olivewp_plus_enable_post_meta_typography == true) { ?>
        <style>
            body .entry-meta span {
                font-family: '<?php echo esc_attr( get_theme_mod('post_meta_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('post_meta_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('post_meta_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('post_meta_fontsize', '16') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('post_meta_line_height','26') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('post_meta_font_weight','400') ) ?> ;
            }
        </style>
    <?php }


    /* ====================
        * Shop Page
    ==================== */
    if($olivewp_plus_enable_shop_typography == true) { ?>
        <style>
            body.woocommerce h1.product_title {
                font-family: '<?php echo esc_attr( get_theme_mod('shop_h1_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('shop_h1_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('shop_h1_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('shop_h1_fontsize', '42') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('shop_h1_line_height','63') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('shop_h1_font_weight','700') ) ?> ;
            }
            body.woocommerce ul.products li.product .woocommerce-loop-product__title, body.woocommerce h2:not(.sidebar h2, body.woocommerce-page .site-footer h2, body.woocommerce-page .custom-logo-link-url h2.site-title), body.woocommerce-page .cart_totals h2, body.woocommerce-page h2.woocommerce-loop-product__title, body.woocommerce-page .cross-sells h2, body .woocommerce ul.products li.product .woocommerce-loop-product__title  {
                font-family: '<?php echo esc_attr( get_theme_mod('shop_h2_fontfamily','Poppins') );?>';
                font-style: <?php echo esc_attr( get_theme_mod('shop_h2_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('shop_h2_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('shop_h2_fontsize', '18') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('shop_h2_line_height','27') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('shop_h2_font_weight','700') ) ?> ;
            }
            body.woocommerce-page h3 {
                font-family: '<?php echo esc_attr( get_theme_mod('shop_h3_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('shop_h3_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('shop_h3_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('shop_h3_fontsize', '24') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('shop_h3_line_height','36') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('shop_h3_font_weight','700') ) ?> ;
            }
        </style>
    <?php }



    /* ====================
        * Sidebar
    ==================== */
    if($olivewp_plus_enable_sidebar_typography == true) { ?>
        <style>
            body .sidebar .wp-block-search .wp-block-search__label, body .sidebar .widget.widget_block h1, body .sidebar .widget.widget_block h2, body .sidebar .widget.widget_block h3, body .sidebar .widget.widget_block h4, body .sidebar .widget.widget_block h5, body .sidebar .widget.widget_block h6, body .sidebar .widget .widget-title, body .sidebar .wc-block-product-search__label {
                font-family: '<?php echo esc_attr( get_theme_mod('sidebar_widget_title_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('sidebar_widget_title_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('sidebar_widget_title_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('sidebar_widget_title_fontsize', '30') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('sidebar_widget_title_line_height','45') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('sidebar_widget_title_font_weight','700') ) ?> ;
            }
            body .sidebar ul li a:not(.sidebar .wp-block-social-links .wp-social-link a), body .sidebar ol li a, body .sidebar .wp-block-latest-comments, body .sidebar .wp-block-latest-posts__post-excerpt, body .sidebar p:not(.sidebar .widget p.wp-block-tag-cloud)  {
                font-family: '<?php echo esc_attr( get_theme_mod('sidebar_widget_content_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('sidebar_widget_content_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('sidebar_widget_content_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('sidebar_widget_content_fontsize', '18') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('sidebar_widget_content_line_height','29') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('sidebar_widget_content_font_weight','400') ) ?> ;
            }
            body .sidebar .widget .wp-block-tag-cloud a {
                font-family: '<?php echo esc_attr( get_theme_mod('sidebar_widget_content_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('sidebar_widget_content_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('sidebar_widget_content_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('sidebar_widget_content_fontsize', '18') ) . 'px'; ?> !important;
                line-height: <?php echo intval( get_theme_mod('sidebar_widget_content_line_height','29') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('sidebar_widget_content_font_weight','400') ) ?> ;
            }
        </style>
    <?php }


    /* ====================
        * Footer
    ==================== */
    if($olivewp_enable_footer_typography == true) { ?>
        <style>
            body .footer-sidebar .wp-block-search .wp-block-search__label, body .footer-sidebar .widget.widget_block h1, body .footer-sidebar .widget.widget_block h2, body .footer-sidebar .widget.widget_block h3, body .footer-sidebar .widget.widget_block h4, body .footer-sidebar .widget.widget_block h5, body .footer-sidebar .widget.widget_block h6, body .footer-sidebar .widget .widget-title {
                font-family: '<?php echo esc_attr( get_theme_mod('footer_widget_title_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('footer_widget_title_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('footer_widget_title_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('footer_widget_title_fontsize', '30') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('footer_widget_title_line_height','30') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('footer_widget_title_font_weight','600') ) ?> ;
            }
            body .footer-sidebar a:not(.footer-sidebar .wp-block-social-links .wp-social-link a), body .footer-sidebar p:not(.footer-sidebar .widget p.wp-block-tag-cloud), body .footer-sidebar .wp-block-latest-posts__post-excerpt {
                font-family: '<?php echo esc_attr( get_theme_mod('footer_widget_content_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('footer_widget_content_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('footer_widget_content_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('footer_widget_content_fontsize', '18') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('footer_widget_content_line_height','29') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('footer_widget_content_font_weight','400') ) ?> ;
            }
            body .footer-sidebar .widget .wp-block-tag-cloud a  {
                font-family: '<?php echo esc_attr( get_theme_mod('footer_widget_content_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('footer_widget_content_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('footer_widget_content_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('footer_widget_content_fontsize', '18') ) . 'px'; ?> !important;
                line-height: <?php echo intval( get_theme_mod('footer_widget_content_line_height','29') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('footer_widget_content_font_weight','400') ) ?> ;
            }
        </style>
    <?php }

    /* ====================
        * Footer Bar
    ==================== */
    if($olivewp_enable_footer_bar_typography == true) { ?>
        <style>
            body .site-info p, body .site-info a:not(.site-info .wp-block-social-links .wp-social-link a), body .site-info h1, body .site-info h2, body .site-info h3, body .site-info h4, body .site-info h5, body .site-info h6, body .site-info {
                font-family: '<?php echo esc_attr( get_theme_mod('footer_bar_fontfamily','Poppins') );?>' ;
                font-style: <?php echo esc_attr( get_theme_mod('footer_bar_fontstyle','normal') );?> ;
                text-transform: <?php echo esc_attr( get_theme_mod('footer_bar_text_transform','none') );?> ;
                font-size: <?php echo intval( get_theme_mod('footer_bar_fontsize', '18') ) . 'px'; ?> ;
                line-height: <?php echo intval( get_theme_mod('footer_bar_line_height','29') ).'px'; ?> ;
                font-weight: <?php echo intval( get_theme_mod('footer_bar_font_weight','400') ) ?> ;
            }
        </style>
    <?php }

}
add_action('wp_head', 'olivewp_plus_typography_custom_css');