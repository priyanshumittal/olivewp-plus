<?php
/**
 * Olivewp functions and definitions
 *
 * @package OliveWP Plus
 */

/**
 * Load all core theme function files
*/



/*
-------------------------------------------------------------------------------
 Short Code for the Load More Post Navigation
-------------------------------------------------------------------------------*/
if (!function_exists('olivewp_plus_script_load_more')) :

    function olivewp_plus_script_load_more($args = array()) {
        //initial posts load
        global $template;
        //$row = '';
        $olivewp_plus_blog_masonry='';
        //$row_template = array("template-blog-grid-view-sidebar.php", "template-blog-grid-view.php");
        $masonry_template = array("template-blog-masonry-two-column.php","template-blog-masonry-three-column.php","template-blog-masonry-four-column.php");
        
        if (in_array(basename($template), $masonry_template))   
        {
            $olivewp_plus_blog_masonry =  'waterfall spice-row';
        }

        /*if (in_array(basename($template), $row_template))   
        {
            $row =  'spice-row';
        }*/

        if(((get_theme_mod('olivewp_plus_blog_layout_feature','default')=='grid') && (get_theme_mod('olivewp_plus_grid_style_feature','default')=='masonry') && (basename($template)=='index.php')) || ((get_theme_mod('olivewp_plus_blog_layout_feature','default')=='grid') && (get_theme_mod('olivewp_plus_grid_style_feature','default')=='default')  && (basename($template)=='index.php') )) {
            olivewp_plus_ajax_script_load_more($args);
        }
        else
        {
            
            //echo '<div id="ajax-content" class="'.$olivewp_plus_blog_masonry.' '.$row.' content-area">';
            echo '<div id="ajax-content" class="'.$olivewp_plus_blog_masonry.' content-area">';
            olivewp_plus_ajax_script_load_more($args);
            echo '</div>';
        }
        echo '<div class="spice-clr"></div>';
        echo '<span id="ajax-content2" >';
        echo '</span>';
        echo '<a href="#" id="loadMore" class="'.get_theme_mod('post_navigation_style','pagination').'='.basename($template).'='.get_theme_mod('olivewp_plus_blog_layout_feature','default').'='.get_theme_mod('olivewp_plus_grid_style_feature','default').'" data-page="1" data-url="'.admin_url("admin-ajax.php").'" >Load More</a>';
    }
    add_shortcode('ajax_posts', 'olivewp_plus_script_load_more');

endif;

function olivewp_plus_check_breadcrumb_enable(){
    $olivewp_breadcrumbs_choice=get_post_meta(get_the_ID(),'olivewp_show_breadcrumb', true );
    if ( function_exists('yoast_breadcrumb') && ($olivewp_breadcrumbs_choice != 'olivewp_breadcrumbs_disable')) {
        $seo_bread_title = get_option('wpseo_titles');
        if($seo_bread_title['breadcrumbs-enable'] == true) {?>
            <style type="text/css">
                .page-title-section .spice-col-3,.page-title-section h1,.page-title-section h2,.page-title-section h3,.page-title-section h4,.page-title-section h5,.page-title-section h1,.page-title-section h6,.page-title-section span,.page-title-section p{
                    margin:unset;
                }
            </style>
            <?php
        }
    }
    elseif ( function_exists('yoast_breadcrumb')) {
        $seo_bread_title = get_option('wpseo_titles');
        if($seo_bread_title['breadcrumbs-enable'] == true) {
            if ($olivewp_breadcrumbs_choice == 'olivewp_breadcrumbs_disable') {?>
            <style type="text/css">
                .page-title-section .spice-col-3,.page-title-section h1,.page-title-section h2,.page-title-section h3,.page-title-section h4,.page-title-section h5,.page-title-section h1,.page-title-section h6,.page-title-section span,.page-title-section p{
                    margin:unset;
                }
            </style>
            <?php
            }
        }
    }
    if(get_theme_mod('breadcrumb_enable',true) == false) {?>
        <style type="text/css">
            .page-title-section .spice-col-3,.page-title-section h1,.page-title-section h2,.page-title-section h3,.page-title-section h4,.page-title-section h5,.page-title-section h1,.page-title-section h6,.page-title-section span,.page-title-section p{
                margin:unset;
            }
        </style>
        <?php
    }
}
add_action('wp_head','olivewp_plus_check_breadcrumb_enable',11);