<?php
/**
 * Template part for displaying posts
 *
 * @package OliveWP Plus
 */
$img_class='img-fluid';
if((get_theme_mod('olivewp_plus_thumbnail_style_feature','default')=='round') && (get_theme_mod('olivewp_plus_blog_layout_feature','default')=='list')) 
	{ 
		$img_class='img-round';
	}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('post'); ?> >
	<!-- Post Featured Image -->
	<?php if(has_post_thumbnail()): 
		  if(get_theme_mod('olivewp_plus_thumbnail_pos_feature','left')=='left'):?>
		<figure class="post-thumbnail <?php echo esc_attr($img_class);?>">
			<a href="<?php the_permalink(); ?>" >
				<?php the_post_thumbnail('full', array( 'loading' => false )); ?>
			</a>				
		</figure>
	<?php endif; endif;?>
	
	<div class="post-content">

        <!-- Entry Meta -->
        <?php /////////////////////if(get_theme_mod('olivewp_plus_enable_post_date',true) || get_theme_mod('olivewp_plus_enable_category',true) || get_theme_mod('olivewp_plus_enable_post_comment',true)): ?> 
			<div class="entry-meta">

				<?php 
				/**
	 			* Meta Drag & Drop Feature
	 			*/ 
	 			$olivewp_plus_meta_sort = get_theme_mod( 'olivewp_blog_meta_sort', 
										array('blog_date','blog_category','blog_comment')
									);
	 			if ( ! empty( $olivewp_plus_meta_sort ) && is_array( $olivewp_plus_meta_sort ) ) :
	 				foreach ( $olivewp_plus_meta_sort as $olivewp_plus_meta_sort_key => $olivewp_plus_meta_sort_val ) : ?>
	 					
	 					<!-- Post Date -->
	 					<?php /////////////// if(get_theme_mod('olivewp_plus_enable_post_date',true)==true):
		 					if ( 'blog_date' === $olivewp_plus_meta_sort_val ) : ?>	
								<span class="date">	
										<?php if(get_theme_mod('olivewp_enable_meta_icon',true)==true):?>
											<i class="far fa-clock"></i>
										<?php 
										else: 
												echo '<span class="meta-links">Posted on:</span>';
										endif;?>
										<a href="<?php echo esc_url(home_url()); ?>/<?php echo esc_html(date('Y/m', strtotime(get_the_date()))); ?>" alt="<?php esc_attr_e('date-time','olivewp'); ?>">
										   <time class="entry-date"><?php echo esc_html(get_the_date()); ?></time>
										</a>
								</span>
							<?php ///////////////endif; 
						endif;?>

						<!-- Post Category -->
						<?php ///////////if(get_theme_mod('olivewp_plus_enable_category',true)==true): 
							if ( 'blog_category' === $olivewp_plus_meta_sort_val ) :
								if ( has_category() ) :
										echo '<span class="cat-links">';
										if(get_theme_mod('olivewp_enable_meta_icon',true)==true):?>
											<i class="far fa-folder-open"></i>
										<?php 
										else: 
												echo '<span class="meta-links">Posted in:</span>';
										endif; 
										the_category( ', ' );
										echo '</span>';
									endif;  
							/////////////////////endif;
						endif;?>

						<!-- Post Comment -->
						<?php /////////////////////if(get_theme_mod('olivewp_plus_enable_post_comment',true)==true):
							if ( 'blog_comment' === $olivewp_plus_meta_sort_val ) : ?>
								<span class="comments-link">
										<?php if(get_theme_mod('olivewp_enable_meta_icon',true)==true):?>
											<i class="far fa-comment-alt"></i>
										<?php 
										else: 
											echo '<span class="meta-links">Comment:</span>';
										endif;?>
							     		<a href="<?php the_permalink(); ?>#respond" alt="<?php esc_attr_e('Comments','olivewp'); ?>">
							     			<?php echo esc_html(get_comments_number()); ?>&nbsp;<?php echo esc_html__('Comments','olivewp'); ?>
								     	</a>
							     	</span>
							<?php endif;
						///////////////////////endif; 
					endforeach;
				endif; ?>
			</div>
		<?php //////////////////////////endif; ?>
		
		<!-- Post Title -->
		<header class="entry-header">
			<h3 class="entry-title">
				<a href="<?php the_permalink();?>"><?php the_title();?></a>
			</h3>                                                  
		</header>

		<!-- Post Content -->
		<div class="entry-content">
			<?php do_action( 'olivewp_post_content_data' );
			if((get_theme_mod('olivewp_plus_enable_post_admin',true)==true) || (get_theme_mod('olivewp_plus_enable_post_read_more',true) == true)): ?>
				<div class="spice-seprator"></div>
			<?php endif; ?>
			<!-- Post Author -->
			<?php if(get_theme_mod('olivewp_plus_enable_post_admin',true)==true): ?>
				<div class="footer-meta entry-meta">
					<i class="far fa-user"></i>
					<a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><span class="author"><?php echo esc_html(get_the_author());?></span></a>
				</div> 
			<?php endif;
			$olivewp_plus_button_show_hide=get_theme_mod('olivewp_blog_content','excerpt');
			if($olivewp_plus_button_show_hide=="excerpt")
			{
				if(get_theme_mod('olivewp_plus_enable_post_read_more',true) == true):
 					olivewp_plus_readmore_button();
 				endif;
			} ?>
		</div>
	</div>
		<!-- Post Featured Image -->
	<?php if(has_post_thumbnail()): 
		  if(get_theme_mod('olivewp_plus_thumbnail_pos_feature','left')=='right'):?>
		<figure class="post-thumbnail <?php echo esc_attr($img_class);?>">
			<a href="<?php the_permalink(); ?>" >
				<?php the_post_thumbnail('full', array( 'loading' => false )); ?>
			</a>				
		</figure>
	<?php endif; endif;?>
</article>