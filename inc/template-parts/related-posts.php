<?php
/**
 * Template part for displaying related posts
 *
 * @package OliveWP Plus
 */
$isRTL = (is_rtl()) ? (bool) true : (bool) false;
wp_register_script('olivewp-plus-related-posts', OLIVEWP_PLUGIN_URL . 'inc/assets/js/related-posts.js', array('jquery'));
wp_localize_script('olivewp-plus-related-posts', 'related_posts_settings', array('rtl' => $isRTL));
wp_enqueue_script('olivewp-plus-related-posts');

$olivewp_plus_related_post         =   olivewp_plus_related_posts(); 
$olivewp_plus_related_title        =   get_theme_mod('olivewp_plus_related_post_title',__('Related Posts','olivewp-plus'));
if($olivewp_plus_related_post->have_posts() ): ?>
<!--Blog related post-->
<article class="related-posts">
     <?php
    if(!empty($olivewp_plus_related_title)):?>
        <div class="comment-title">
            <h3><?php echo esc_html($olivewp_plus_related_title); ?></h3>
        </div>
    <?php endif; ?>
    <div class="spice-row">
        <div id="related-posts-carousel" class="owl-carousel owl-theme"> 
            <?php while ($olivewp_plus_related_post->have_posts()) : $olivewp_plus_related_post->the_post();?>
                <div class="item">
                    <article class="post">
                        <figure class="post-thumbnail">                     
                            <a href="<?php the_permalink(); ?>" >
                                <?php the_post_thumbnail('full', array('class'=>'img-fluid', 'loading' => false )); ?>
                            </a>
                         </figure>
                        <div class="post-content"> 
                            <div class="entry-meta">
                                <!-- Post Date -->
                                <span class="date"> 
                                    <i class="far fa-clock"></i>
                                    <a href="<?php echo esc_url(home_url()); ?>/<?php echo esc_html(date('Y/m', strtotime(get_the_date()))); ?>" alt="<?php esc_attr_e('date-time','olivewp-plus'); ?>">
                                       <time class="entry-date"><?php echo esc_html(get_the_date()); ?></time>
                                    </a>
                                </span>
                                
                                <!-- Post Category -->
                                <?php if ( has_category() ) :
                                    echo '<span class="cat-links"><i class="far fa-folder-open"></i>'; 
                                    the_category( ', ' );
                                    echo '</span>';
                                endif; ?> 

                                <!-- Post Comment -->
                                <span class="comments-link">
                                    <i class="far fa-comment-alt"></i>
                                    <a href="<?php the_permalink(); ?>#respond" alt="<?php esc_attr_e('Comments','olivewp-plus'); ?>">
                                        <?php echo esc_html(get_comments_number()); ?>&nbsp;<?php echo esc_html__('Comments','olivewp-plus'); ?>
                                    </a>
                                </span>  
                            </div>
                            <header class="entry-header">
                                <h3 class="entry-title">
                                    <a href="<?php the_permalink();?>"><?php the_title();?></a>
                                 </h3> 
                             </header>
                            <div class="entry-content">
                                <?php do_action( 'olivewp_post_content_data' ); 
                                if((get_theme_mod('olivewp_plus_enable_post_admin',true)==true) || (get_theme_mod('olivewp_plus_enable_post_read_more',true) == true)): ?>
                                    <div class="spice-seprator"></div>
                                <?php endif; ?>
                                <!-- Post Author -->
                                <?php if(get_theme_mod('olivewp_plus_enable_post_admin',true)==true): ?>
                                    <div class="footer-meta entry-meta">
                                        <i class="far fa-user"></i>
                                        <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))); ?>"><span class="author"><?php echo esc_html(get_the_author());?></span></a>
                                    </div> 
                                    <?php endif;
                                    $olivewp_plus_button_show_hide=get_theme_mod('olivewp_blog_content','excerpt');
                                    if($olivewp_plus_button_show_hide=="excerpt")
                                    {
                                        if(get_theme_mod('olivewp_plus_enable_post_read_more',true) == true):
                                            olivewp_plus_readmore_button();
                                        endif;
                                    } ?>
                            </div>
                        </div>
                     </article>
                </div>
            <?php endwhile; wp_reset_query(); ?>
        </div>
    </div>
</article>
<?php endif; ?>