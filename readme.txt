=== OliveWP Plus ===

Contributors: spicethemes
Requires at least: 4.5
Tested up to: 6.3
Stable tag: 1.3.1
Requires PHP: 5.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Enhance OliveWP WordPress Themes functionality.

== Description ==

OliveWP is a page builder theme. It is a  lightweight, fully responsive, and a perfect theme for your project. It allows you to create stunning blogs and any type of websites like portfolio, WooCommerce and business with all the available beautiful designs. The theme is RTL & translation ready and compatible with popular plugins like WooCommerce, contact form 7, etc etc. It provides number of starter sites for various businesses which enables you to easily create your website with all beautiful designs in a very short time. Currently theme is 100% compatible with Elementor. Check our demos to know more about them: https://olivewp.org/starter-sites/

== Changelog ==

@Version 1.3.1
* Added Rank Math, Seo Yoast and NavXT Breadcrumbs Feature.

@Version 1.3
* Updated freemius directory.

@Version 1.2.4
* Added Add-ons tab in the OliveWP Plus option page.
* Moved the starter site option into the OliveWP Plus menu from the Appearance menu.

@Version 1.2.3
* Updated freemius code.

@Version 1.2.2
* Updated Freemius Code.

@Version 1.2.1
* Added Adobe Fonts featured in typography with Spice Adobe Fonts Plugin.
* Fixed Blog Image Blur & Blog Template issues.

@Version 1.2
* Added blog layout , margin , padding and background color feature for blog archive page.
* Added padding and background color feature for single post.
* Added Blog Masonry 2 Column, 3 Column, 4Column Template

@Version 1.1
* Added customizer setting for managing breadcrumb section.
* Added meta setting for managing breadcrumb in individual posts / pages.
* Updated option page mockup image.

@Version 1.0
* Fixed Content Width & Sidebar Width settings issue.

@Version 0.2
* Added padding setting for the banner section.
* Recommended the Spice Social Share plugin on the options page.
* Fixed some issues.

@Version 0.1
* Initial release

== External resources ==

Owl Carousel: 
Copyright: (c) David Deutsch
License: MIT License
Source: https://cdnjs.com/libraries/OwlCarousel2/2.2.1

Alpha Color Picker Control:
Copyright: (c) 2016 Codeinwp cristian-ungureanu
License: MIT License
Source: https://github.com/Codeinwp/customizer-controls/tree/master/customizer-alpha-color-picker

Custom control - Image Radio Button Custom Control:
Copyright: Anthony Hortin
License: GNU General Public License v2 or later
Source: https://github.com/maddisondesigns/customizer-custom-controls

* Images on /images folder
Copyright (C) 2022, spicethemes and available as [GPLv2](https://www.gnu.org/licenses/gpl-2.0.html)